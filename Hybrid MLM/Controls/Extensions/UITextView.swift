//
//  UITextView.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 24/06/21.
//

import Foundation
import UIKit
extension UITextView:UITextViewDelegate {
    func leftSpace() {
        self.textContainerInset = UIEdgeInsets(top: 4, left: 6, bottom: 4, right: 4)
    }
  
}
