//
//  File.swift
//  Loopz
//
//  Created by Nabeel Gulzar on 11/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation


extension String {
    var doubleValue: Double? {
        if self.length == 0 {
            return 0
        }
        return Double(self) ?? 0
    }
    var floatValue: Float? {
        if self.length == 0 {
            return 0
        }
        return Float(self) ?? 0
    }
    var integerValue: Int? {
        if self.length == 0 {
            return 0
        }
        return Int(self) ?? 0
    }
    
    var length: Int {
        return self.sorted().count
    }
    
    var localised: String {
        return self //OneSkyOTAPlugin.localizedString(forKey: self, value: "", table: nil)
    }
}


extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
