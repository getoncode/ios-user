//
//  UIImage.swift
//  iDeliver
//
//  Created by Nabeel Gulzar on 09/03/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    /// Resize the Image
    ///
    /// - Parameter size: New Size
    /// - Returns: UIImage
    func resizeImage(size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        self.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
