//
//  UITextField.swift
//  Qoot Inventory
//
//  Created by Nabeel Gulzar on 29/12/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit


extension UITextField {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
       /* if self.textAlignment != .center {
            if Utility.getLanguage().Code == "ar" {
                self.textAlignment = .right
            }else{
                self.textAlignment = .left
            }
        }*/
    }
    
    func setPlaceHolder( _color: UIColor,_ text:String) {
        self.attributedPlaceholder = NSAttributedString(string: text,
                                                        attributes: [NSAttributedString.Key.foregroundColor: _color.withAlphaComponent(0.8)])
    }
}
