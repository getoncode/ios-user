//
//  UIView.swift
//  Qoot Inventory
//
//  Created by Nabeel Gulzar on 02/09/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit
extension UIView {
    
    //Add Shadow
    func setShadow(opacity: Float) {
        self.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(0))
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = opacity
    }
}
