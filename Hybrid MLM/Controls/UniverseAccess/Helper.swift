//
//  Helper.swift
//  event
//
//  Created by Shahna Nabeel on 25/02/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import RxCocoa
import RxSwift
import RxAlamofire

class Helper : NSObject {
    
    static let loginVM = LoginVM()
    
    static var alertPopup:UIAlertController? = nil
    static let HelperResponseType = PublishSubject<ResponseTypes>()
    enum ResponseTypes:Int {
        case Default                =   0
        case LocationAvalibility    =   1
        case confirmLogoiut         =   2
        case Dismiss                =   3
    }
    
    //push viewcontroller
    class func PushViewController(_ storyboard: String, _ vc: UIViewController){
        let storyboard: UIStoryboard = UIStoryboard(name: storyboard, bundle: nil)
        let pushVc = storyboard.instantiateViewController(withIdentifier: String.init(describing: vc.classForCoder))
        vc.show(pushVc, sender: self)
    }
    
    //logout function
    class func logout(){
        Utility.setLogin(value: false)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: String(describing: LoginViewController.self)) as! LoginViewController
        let nav = UINavigationController.init(rootViewController: controller)
        nav.modalPresentationStyle = .fullScreen
        Helper.finalController().present(nav, animated: true, completion: nil)
        
        //clear userdefaults
        if let appDomain = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: appDomain)
        }
    }
    
    //extend expiry time
    class func extendExpiryTime(secs : Double) -> Date {
        let expiry = Calendar.current.date(byAdding: .second, value: Int(secs), to: Date())
        return expiry!
    }
    
    //check if token expired
    class func callRefreshToken() -> Bool{
        guard let expiryDate = UserDefaults.standard.value(forKey: StringConstants.Expiry()) as? Date else { return false }
        if expiryDate < Date(){
            loginVM.refreshToken(UserDefaults.standard.value(forKey: StringConstants.refreshToken()) as? String ?? "")
            if expiryDate > UserDefaults.standard.value(forKey: StringConstants.Expiry()) as! Date {
               return true
            }
            return false
        }
        return true
    }
    
    //find if textfield is empty
    class func checkTextFieldCount(TF: [UITextField]) -> Bool{
        var check = true
        for item in TF {
            if item.text?.count == 0{
                check = false
            }
        }
        return check
    }
    
    //convert milliseconds to minute
    class func converter(milliSeconds: Double) -> Double {
        let minutes = (milliSeconds * 0.001)
        return minutes
    }
    
    
    //set shadow for uiview
    class func setShadow(view: UIView,color: UIColor, radius: CGFloat) {
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowRadius = radius
        view.layer.masksToBounds = false
    }
    
    //get previous view controller
    class func previousController(vc:UIViewController) -> UIViewController{
        let count = vc.navigationController?.viewControllers.count ?? 0
        let vcToreturn = vc.navigationController?.viewControllers[count - 2]
        return vcToreturn ?? vc
    }
    
    //gradient combinations
    class func gradient(view: UIView) {
        
        let colorOne = UIColor(red: 118/255, green: 214/255, blue: 255/255, alpha: 1.0).cgColor
        //let colorTwo = UIColor(red: 92/255, green: 171/255, blue: 255/255, alpha: 1.0).cgColor
        let colorThree = UIColor(red: 1/255, green: 25/255, blue: 147/255, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.frame
        gradientLayer.colors = [colorOne, colorThree]
        let gradientOffset = view.bounds.height / view.bounds.width / 2
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.3 + gradientOffset)
        gradientLayer.endPoint =  CGPoint(x: 0.8, y: 0.8 - gradientOffset)
        
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        view.backgroundColor = UIColor(patternImage: image!)
        
    }
    
    //gradient combinations
    class func gradientLight(view: UIView) {
        
        let colorOne = UIColor(red: 28/255, green: 48/255, blue: 160/255, alpha: 1.0).cgColor
        let colorTwo = UIColor(red: 61/255, green: 122/255, blue: 202/255, alpha: 1.0).cgColor
        let colorThree = UIColor(red: 88/255, green: 166/255, blue: 228/255, alpha: 1.0).cgColor
        
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.frame
        gradientLayer.colors = [colorThree, colorTwo, colorOne]
        let gradientOffset = view.bounds.height / view.bounds.width / 2
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.7 + gradientOffset)
        gradientLayer.endPoint =  CGPoint(x: 0.8, y: 0.8 - gradientOffset)
        
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        view.backgroundColor = UIColor(patternImage: image!)
        
    }
    
    
    
    //set gradient
    class func setGradient(view: UIButton,color1: UIColor, color2: UIColor,corner: CGFloat) {
        // Apply Gradient Color
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size = view.frame.size
        gradientLayer.cornerRadius = corner
        gradientLayer.colors =
            [color2.withAlphaComponent(5).cgColor,color1]
        //Use diffrent colors
        view.layer.addSublayer(gradientLayer)
    }
    //set root view
    class func setRootViewController()
    {
        
    }
    
    //Set Button
    class func setButton(button:UIButton,view:UIView,primaryColour:UIColor,seconderyColor:UIColor,shadow:Bool,corner:Int,border:UIColor,type: UIButton.ButtonType) {
        //Set Background
        view.backgroundColor = UIColor.clear
        button.setBackgroundImage(self.GetImageFrom(color: primaryColour), for: .normal)
        button.setBackgroundImage(self.GetImageFrom(color: seconderyColor), for: .highlighted)
        button.setBackgroundImage(self.GetImageFrom(color: seconderyColor), for: .selected)
        
        //Set Border and Corner
        view.layer.cornerRadius = CGFloat(corner)
        view.layer.borderWidth = CGFloat(1)
        view.layer.borderColor = border.cgColor
        view.clipsToBounds = true
        
        
        if view.isKind(of: UIView.classForCoder()) {
            if shadow {
                view.setShadow(opacity: 0.5)
            }
            view.backgroundColor = UIColor .clear
        }
        
        //Set Title
        var pC = primaryColour
        var sC = seconderyColor
        if seconderyColor == UIColor.clear {
            sC = UIColor.lightGray
        }
        if primaryColour == UIColor.clear {
            pC = UIColor.lightGray
        }
        button.setTitleColor(sC, for: .normal)
        button.setTitleColor(pC, for: .highlighted)
        button.setTitleColor(pC, for: .selected)
    }
    
    //setbutton2
    class func setButton2(button:UIButton,view:UIView,primaryColour:UIColor,seconderyColor:UIColor,shadow:Bool,corner:Int,border:UIColor,primaryColourSelect:UIColor,seconderyColorSelect:UIColor) {
        //Set Background
        view.backgroundColor = UIColor.clear
        button.setBackgroundImage(self.GetImageFrom(color: primaryColour), for: .normal)
        button.setBackgroundImage(self.GetImageFrom(color: primaryColourSelect), for: .highlighted)
        button.setBackgroundImage(self.GetImageFrom(color: primaryColourSelect), for: .selected)
        
        //Set Border and Corner
        view.layer.cornerRadius = CGFloat(corner)
        view.layer.borderWidth = CGFloat(1)
        view.layer.borderColor = border.cgColor
        view.clipsToBounds = true
        
        
        if view.isKind(of: UIView.classForCoder()) {
            if shadow {
                view.setShadow(opacity: 1)
            }
            view.backgroundColor = UIColor .clear
        }
        
        //Set Title
        var pC = seconderyColorSelect
        var sC = seconderyColor
        if seconderyColor == UIColor.clear {
            sC = Colors.PrimaryText
        }
        if seconderyColorSelect == UIColor.clear {
            pC = Colors.PrimaryText
        }
        button.setTitleColor(sC, for: .normal)
        button.setTitleColor(pC, for: .highlighted)
        button.setTitleColor(pC, for: .selected)
        var origImage = button.image(for: .normal);
        var tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        button.setImage(tintedImage, for: .normal)
        origImage = button.image(for: .highlighted);
        tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        button.setImage(tintedImage, for: .highlighted)
        origImage = button.image(for: .selected);
        tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        button.setImage(tintedImage, for: .selected)
    }
    
    
    class func roundBorder(view: UIView, radius: CGFloat, borderWidth: CGFloat, color: CGColor)
    {
        //Set Border and Corner
        view.layer.cornerRadius = radius
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = color
        view.clipsToBounds = true
    }
    class func roundBorderButton(view: UIButton, radius: CGFloat, borderWidth: CGFloat, color: CGColor)
    {
        //Set Border and Corner
        view.layer.cornerRadius = radius
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = color
        view.clipsToBounds = true
    }
    
    //password validation check
    class func validateUppercase(password: String) -> Bool {
        let capitalLetterRegEx  = ".*[A-Z ]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        return texttest.evaluate(with: password)
    }
    class func validateAlphabets(password: String) -> Bool {
        let justLettersRegex = "[^A-Za-zÀ-ÖØ-öø-ÿ]"
        // let trimmedString = string.replacingOccurrences(of: " ", with: "")
        return password.isEmpty == false && password.range(of: justLettersRegex, options: .regularExpression) == nil
    }
    class func validateNumeric(password: String) -> Bool {
        let numberRegEx  = ".*[0-9]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        return texttest.evaluate(with: password)
    }
    class func validateSpecialCharacters(password: String) -> Bool {
        let specialCharacterRegEx  = ".*[!&^%$#@()/_*+-]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        return texttest.evaluate(with: password)
    }
    
    
    class func bottomLine(text: UITextField)
    {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: text.frame.height - 1, width: text.frame.width, height: 1.0)
        bottomLine.backgroundColor = Colors.AppBorderColor.cgColor
        text.borderStyle = UITextField.BorderStyle.none
        text.layer.addSublayer(bottomLine)
    }
    
    //tab button background
    class func setTabColor(button: UIButton)  {
        if button.isSelected
        {
            button.backgroundColor = Colors.AppBaseColor
        }
        else
        {
            button.backgroundColor = UIColor.clear
        }
    }
    
    //Set Button Title
    class func setButtonTitle(normal:String,highlighted:String,selected:String,button:UIButton) {
        button.setTitle(normal, for: .normal)
        button.setTitle(highlighted, for: .highlighted)
        button.setTitle(selected, for: .selected)
    }
    
    //Get Image From Color
    static func GetImageFrom(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    //Get UIColor from HexColorCode
    class func getUIColor(color:String) -> UIColor {
        var cString:String = color.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.sorted().count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    ///Set ui element Border and corner
    class func UiElementBorderWithCorner(element:UIView, radius:Float, borderWidth:Float , backgroundColor:UIColor, border:UIColor, tint:UIColor) {
        element.layer.cornerRadius = CGFloat(radius)
        element.layer.borderWidth = CGFloat(borderWidth)
        element.backgroundColor = backgroundColor
        element.layer.borderColor = border.cgColor
        element.tintColor = tint
        element.clipsToBounds = true
    }
    
    
    //Transparent navigation
    class func transparentNavigation(controller:UINavigationController) {
        
        controller.navigationBar.backgroundColor = UIColor.clear
        controller.navigationBar.isTranslucent = true
        Helper.removeNavigationSeparator(controller: controller, image: false)
    }
    
    //NonTransparent navigation
    class func nonTransparentNavigation(controller:UINavigationController) {
        controller.navigationBar.backgroundColor = Colors.AppBaseColor
        //        controller.navigationBar.isTranslucent = false
        controller.navigationBar.tintColor = Colors.AppBackgroundColor
        controller.navigationBar.barTintColor = Colors.AppBaseColor
        controller.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.AppBackgroundColor]
        controller.navigationBar.setBackgroundImage(Helper.GetImageFrom(color: Colors.AppBaseColor), for: UIBarMetrics.default)
    }
    
    //Remove separation from navigation
    class func removeNavigationSeparator(controller:UINavigationController,image:Bool) {
        if image {
            controller.navigationBar.setBackgroundImage(nil, for:.default)
            controller.navigationBar.shadowImage = nil
            controller.navigationBar.layoutIfNeeded()
        }else{
            controller.navigationBar.setBackgroundImage(UIImage(), for:.default)
            controller.navigationBar.shadowImage = UIImage()
            controller.navigationBar.layoutIfNeeded()
        }
    }
    
    
    /// Remove null from Objects
    ///
    /// - Returns: Object without Null
    class func nullKeyRemoval(data:[String:Any]) -> [String:Any] {
        var dict = data
        let keysToRemove = Array(dict.keys).filter { dict[$0] is NSNull }
        for key in keysToRemove {
            dict.removeValue(forKey: key)
        }
        var keysToUpdate = Array(dict.keys).filter { dict[$0] is [String:Any] }
        for key in keysToUpdate {
            dict.updateValue(nullKeyRemoval(data: dict[key] as! [String : Any]), forKey: key)
        }
        keysToUpdate = Array(dict.keys).filter { dict[$0] is [Any] }
        for key in keysToUpdate {
            let dataArray = dict[key] as! [Any]
            var temp:[Any] = []
            for item in dataArray {
                if item is NSNull {
                    
                }else{
                    if ((item as? [String:Any]) != nil){
                        temp.append(Helper.nullKeyRemoval(data: item as! [String:Any]))
                    }else{
                        temp.append(item)
                    }
                }
            }
            dict.updateValue(temp, forKey: key)
        }
        return dict
    }
    
    /// Validate Email address
    class func isValidEmail(text:String) -> Bool {
        
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}", options: .caseInsensitive)
            return regex.firstMatch(in: text, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, text.length)) != nil
        }
        catch {
            return false
        }
    }
    
    //validate PhoneNumber
    class func isValidPhoneNumber(text:String) -> Bool {
        
        let charcter  = NSCharacterSet(charactersIn: "+0123456789").inverted
        var filtered: String!
        let inputString: NSArray = text.components(separatedBy: charcter) as NSArray
        filtered = inputString.componentsJoined(by: "") as String?
        if text == ""{
            return false
        }
        //        if text == filtered {
        //            let phoneUtil = NBPhoneNumberUtil.sharedInstance()
        //            phoneUtil.cou
        //            do {
        //                let number:NBPhoneNumber = try phoneUtil!.parse(text, defaultRegion:phoneUtil.getcou)
        //
        //                let isValidNumber: Bool = phoneUtil!.isValidNumber(forRegion: number, regionCode: code)
        //
        //                return isValidNumber
        //            }catch  let error as NSError {
        //                print(error.localizedDescription)
        //                return false
        //            }
        //        }else {
        return text == filtered
        //        }
    }
    
    ///Adding Done Button On Keyboard
    class func addDoneButtonOnTextField(tf:UITextField, vc: UIView){
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem.init(title: StringConstants.Done(), style: .plain, target: vc, action: #selector(UIView.endEditing(_:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        tf.inputAccessoryView = keyboardToolbar
    }
    
    //Handling Alert Messages
    class func showAlert(message:String, head:String) {
        alertPopup?.dismiss(animated: false, completion: nil)
        alertPopup = UIAlertController(title:head, message: message, preferredStyle: UIAlertController.Style.alert)
        alertPopup?.addAction(UIAlertAction(title: StringConstants.OK(), style: UIAlertAction.Style.cancel, handler: nil))
        if UIApplication.shared.connectedScenes.first != nil {
            let vc = Helper.finalController()
            vc.present(alertPopup!, animated: true, completion: nil)
        }
    }
    
    class func finalController() -> UIViewController {
        if let wd = UIApplication.shared.windows.first(where: { $0.isKeyWindow }){
            var vc = wd.rootViewController
            if(vc is UINavigationController){
                vc = (vc as! UINavigationController).viewControllers.last
            }
            if(vc is UITabBarController){
                vc = (vc as! UITabBarController).viewControllers?[(vc as! UITabBarController).selectedIndex]
            }
            if(vc is UINavigationController){
                vc = (vc as! UINavigationController).viewControllers.last
            }
            while(vc?.presentedViewController != nil && (vc?.presentedViewController?.isKind(of: UIViewController.classForCoder()))!) {
                vc = vc?.presentedViewController
                if(vc is UINavigationController){
                    vc = (vc as! UINavigationController).viewControllers.last
                }
            }
            if(vc is UINavigationController){
                vc = (vc as! UINavigationController).viewControllers.last
            }
            if(vc is UITabBarController){
                vc = (vc as! UITabBarController).viewControllers?[(vc as! UITabBarController).selectedIndex]
            }
            if(vc is UINavigationController){
                vc = (vc as! UINavigationController).viewControllers.last
            }
            while(vc?.presentedViewController != nil && (vc?.presentedViewController?.isKind(of: UIViewController.classForCoder()))!) {
                vc = vc?.presentedViewController
                if(vc is UINavigationController){
                    vc = (vc as! UINavigationController).viewControllers.last
                }
            }
            return vc!
        }
        return UIViewController()
    }
    
    
    class func dateToString(date:Date,format:String) ->String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = format
        let str = formatter.string(from: date)
        return str
    }
    
    class func stringToDate(date:String) -> String{
        //        let formatter = DateFormatter()
        //        // initially set the format based on your datepicker date / server String
        //        formatter.dateFormat = "MMM d, yyyy"
        //        let str = formatter.date(from: date)
        //        return str
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd" //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        //according to date format your date string
        guard let date = dateFormatter.date(from: "2021-01-06") else {
            fatalError()
        }
        print(date)
        dateFormatter.dateFormat = "MMM d, yyyy" //Your New Date format as per requirement change it own
        let newDate = dateFormatter.string(from: date) //pass Date here
        return newDate
    }
    
    class func stringToDate2(date:String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd" //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        //according to date format your date string
        guard let date = dateFormatter.date(from: "2021-01-06") else {
            fatalError()
        }
        return date
    }
    
    class func stringToDate(_ date:String, _ format:String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let newDate = dateFormatter.date(from: date) ?? Date()
        return newDate
    }
    
    class func convertDate(date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd hh:mm:ss"
        // Convert String to Date
        guard let returnDate = dateFormatter.date(from:date ) else { return "" }
        return self.dateToString(date: returnDate, format: "dd MMM, yyyy")
    }
    
    
    //Handling Alert Messages
    class func showAlertReturn(message:String, head:String, type:String, Hide:Bool, completion: @escaping(_ success: Bool) -> Void) {
        alertPopup?.dismiss(animated: true, completion: nil)
        alertPopup = UIAlertController(title:head, message: message, preferredStyle: UIAlertController.Style.alert)
        if Hide == false {
            alertPopup?.addAction(UIAlertAction(title: StringConstants.Cancel(), style: UIAlertAction.Style.destructive){ action -> Void in
                completion(false)            // Put your code here
            })
        }
        alertPopup?.addAction(UIAlertAction(title: type, style: UIAlertAction.Style.default){ action -> Void in
            completion(true)            // Put your code here
        })
        
        if UIApplication.shared.connectedScenes.first != nil {
            let vc = Helper.finalController()
            vc.present(alertPopup!, animated: true, completion: nil)
       }
    }
    
    //Handling Progress Indicator
    class func showPI(string:String)
    {
        let progress: LoadingProgress = LoadingProgress.shared
        progress.showPI(message: string)
    }
    
    class func hidePI()
    {
        LoadingProgress.shared.hide()
    }
    
    class func setImage(image:String, on:UIImageView) {
        let imageIn = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if imageIn?.length == 0 {
            on.image = #imageLiteral(resourceName: "user")
            return
        }
        on.kf.setImage(with: URL(string: imageIn!), placeholder: #imageLiteral(resourceName: "user"), options: nil, progressBlock: nil) { data in
            
        }
    }
    
    class func setImage(image:String, on:UIButton) {
        let imageIn = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if imageIn?.length == 0 {
            on.setImage(#imageLiteral(resourceName: "logo"), for: .normal)
            return
        }
        on.kf.setImage(with: URL(string: imageIn!), for: .normal, placeholder: #imageLiteral(resourceName: "logo"), options: nil, progressBlock: nil) { data in
            
        }
    }
    
    
    //Handling Alert Messages
    class func showAlertReturn(message:String, head:String, type:String, closeHide:Bool,responce:Helper.ResponseTypes) {
        Helper.hidePI()
        alertPopup?.dismiss(animated: true, completion: nil)
        alertPopup = UIAlertController(title:head, message: message, preferredStyle: UIAlertController.Style.alert)
        if closeHide == false {
            alertPopup?.addAction(UIAlertAction(title: StringConstants.Cancel(), style: UIAlertAction.Style.destructive, handler: nil))
        }
        alertPopup?.addAction(UIAlertAction(title: type, style: UIAlertAction.Style.default){ action -> Void in
            Helper.HelperResponseType.onNext(responce)
            // Put your code here
        })
        
        if UIApplication.shared.delegate?.window != nil {
            let vc = Helper.finalController()
            vc.present(alertPopup!, animated: true, completion: nil)
        }
    }
    
    class func addBottomBorderWithColor(TF : UIView, color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: TF.frame.size.height - width, width: TF.frame.size.width, height: width)
        TF.layer.addSublayer(border)
    }
    
    class func isLoggin() -> Bool {
        if !Utility.loggedIn {
            
            Helper.showAlertReturn(message: "Please Login to Continue", head: "", type: "Login", Hide: false, completion: {data in
                if data {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: String(describing: LoginViewController.self)) as! LoginViewController
                    //            controller.event = event
                    let nav = UINavigationController.init(rootViewController: controller)
                    nav.setNavigationBarHidden(true, animated: true)
                    Helper.finalController().present(nav, animated: true, completion: nil)
                }
            })
        }
        return Utility.loggedIn
    }
    
}

extension NSAttributedString
{
    convenience init(text: String, aligment: NSTextAlignment, color:UIColor) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = aligment
        self.init(string: text, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.foregroundColor:color])
    }
}
extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
             let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
             let mask = CAShapeLayer()
             mask.path = path.cgPath
             self.layer.mask = mask
        }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


