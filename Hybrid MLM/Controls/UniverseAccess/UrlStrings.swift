//
//  UrlStrings.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/01/21.
//

import Foundation

class UrlStrings: NSObject {
    
    class func accessToken() -> String {
        return "http://localhost/blueprint/app/public/oauth/token"
    }
    class func googleClientID() -> String {
        return "1005686356333-3j0utfmvommesov0edpeuu6c3f4vsd20.apps.googleusercontent.com"
    }
    class func register() -> String {
        return "http://localhost/blueprint/app/public/user/register"
    }
    
}
