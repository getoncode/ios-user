//
//  Colors.swift
//  event
//
//  Created by Shahna Nabeel on 25/02/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class Colors: NSObject {

    static let AppBaseColor:UIColor = UIColor.init(named: "AppBaseColor")!
    static let AppBackgroundColor:UIColor = UIColor.init(named: "AppBackgroundColor")!
    static let AppBorderColor:UIColor = UIColor.init(named: "AppBorderColor")!
    static let TextColor:UIColor = UIColor.init(named: "TextColor")!
    static let AppSubTitleColor:UIColor = UIColor.init(named: "AppSubTitleColor")!
    static let AppWhiteBackground:UIColor = UIColor.init(named: "AppWhiteBackground")!
    static let ShadowColor:UIColor = UIColor.init(named: "ShadowColor")!
    static let Seperator:UIColor = UIColor.init(named: "Seperator")!
    

    //unused
    static let AppBaseColor3:UIColor = UIColor.init(named: "AppBaseColor3")!
    static let PrimaryText:UIColor = UIColor.init(named: "PrimaryText")!
    static let PrimaryText2:UIColor = UIColor.init(named: "PrimaryText2")!
    static let PrimaryText3:UIColor = UIColor.init(named: "PrimaryText3")!
     static let Gray:UIColor = UIColor.init(named: "Gray")!
    
}
