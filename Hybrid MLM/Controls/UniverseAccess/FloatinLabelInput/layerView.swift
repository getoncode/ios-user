//
//  layerView.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 25/11/20.
//

import UIKit

class layerView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
        
    }
    
    func setup() {
        
//        // Create a CAShapeLayer
//        let shapeLayer = CAShapeLayer()
//
//        // The Bezier path that we made needs to be converted to
//        // a CGPath before it can be used on a layer.
//        shapeLayer.path = createBezierPath().cgPath
//
//        // apply other properties related to the path
//        shapeLayer.strokeColor = UIColor.blue.cgColor
//        shapeLayer.fillColor = UIColor.white.cgColor
//        shapeLayer.lineWidth = 1.0
//        shapeLayer.position = CGPoint(x: 10, y: 10)
//
//        // add the new layer to our custom view
//        self.layer.addSublayer(shapeLayer)
        
        let squarePath = UIBezierPath()
        squarePath.move(to: CGPoint(x: 0, y: 0))
        squarePath.addLine(to:CGPoint(x: self.frame.size.height, y:self.frame.size.width))
        squarePath.addLine(to: CGPoint(x: self.frame.origin.x, y: self.frame.size.height - 50))
        squarePath.lineWidth = self.frame.size.width
        squarePath.close()
        let square = CAShapeLayer()
        square.path = squarePath.cgPath
        square.fillColor = UIColor.red.cgColor
        self.layer.addSublayer(square)
    }
    func createBezierPath() -> UIBezierPath {
        
        let squarePath = UIBezierPath()
        squarePath.move(to: CGPoint(x: 0, y: 0))
        squarePath.addLine(to: self.frame.origin)
        squarePath.addLine(to: CGPoint(x: self.frame.origin.x, y: self.frame.size.height - 50))
        squarePath.lineWidth = self.frame.size.width
        squarePath.close()
        return squarePath
    }
    
}
