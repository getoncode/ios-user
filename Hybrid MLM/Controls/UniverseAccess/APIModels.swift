//
//  APIModels.swift
//  event
//
//  Created by Shahna Nabeel on 02/03/19.
//  Copyright © 2019 Shahna Nabeel. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire

class APIModel:NSObject {
    
    static let disposeBag = DisposeBag()
    static let APIkeys = APIKeys()
    
    enum ErrorCode: Int {
        case success                = 200
        case badRequest             = 400
        case Unprocessable          = 422
        case unauthorized           = 401
        case forbidden              = 403
        case notFound               = 404
        case internalServerError    = 500
        case internalServerErrorTwo = 502
    }
    
    class func apiTokenCall(method : Alamofire.HTTPMethod, api:String, params:[String:Any],header:[String:String]) -> Observable<(Bool,Any)> {
        
        Helper.showPI(string: "")
        return Observable.create { observer in
            RxAlamofire.requestJSON(method, APIModel.APIkeys.baseTokenURL + api, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                
                Helper.hidePI()
                let errNum = APIModel.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    if let bodyInTemp = body as? [String:Any] {
                        let bodyIn = Helper.nullKeyRemoval(data: bodyInTemp)
                        if let datagot = bodyIn[APIModel.APIkeys.status2] as? Int {
                            if datagot == 1 {
                                if let dataGot = bodyIn[APIModel.APIkeys.data] as? [String : Any] {
                                    observer.onNext((true, dataGot))
                                    observer.onCompleted()
                                }else if let dataGot = bodyIn[APIModel.APIkeys.data] as? [Any] {
                                    observer.onNext((true, dataGot))
                                    observer.onCompleted()
                                }else {
                                    observer.onNext((true, bodyIn))
                                    observer.onCompleted()
                                }
                            }else{
                                guard let dataGot2 = bodyIn[APIModel.APIkeys.message] as? String else {
                                    Helper.showAlert(message: StringConstants.Error(), head: "")
                                    return
                                }
                                Helper.showAlert(message: dataGot2, head: "")
                            }
                            
                            
                        }else if let datagot = bodyIn[APIModel.APIkeys.status] as? Int {
                            if datagot == 1 {
                                if let dataGot = bodyIn[APIModel.APIkeys.data] as? [String : Any] {
                                    observer.onNext((true, dataGot))
                                    observer.onCompleted()
                                }else if let dataGot = bodyIn[APIModel.APIkeys.data] as? [Any] {
                                    observer.onNext((true, dataGot))
                                    observer.onCompleted()
                                }else {
                                    observer.onNext((true, bodyIn))
                                    observer.onCompleted()
                                }
                            }else{
                                guard let dataGot2 = bodyIn[APIModel.APIkeys.message] as? String else {
                                    Helper.showAlert(message: StringConstants.Error(), head: "")
                                    return
                                }
                                Helper.showAlert(message: dataGot2, head: "")
                            }
                            
                        }else{
                            if let  dataGot = bodyIn as? [String : Any]{
                                observer.onNext((true, dataGot))
                                observer.onCompleted()
                            }
                            else
                            {
                                Helper.logout()
                                Helper.showAlert(message: "", head: StringConstants.Error())
                            }
                        }
                    }else{
                        observer.onNext((true, body))
                        observer.onCompleted()
                    }
                }else{
                    observer.onNext((false, [:]))
                    observer.onCompleted()
                    if let bodyIn = body as? [String:Any] {
                        if let dataGot = bodyIn[APIModel.APIkeys.message] as? String {
                            Helper.showAlert(message: dataGot,head: StringConstants.Error())
                        }else if let got = bodyIn["message"] as? String{
                            Helper.logout()
                            Helper.showAlert(message: got,head: StringConstants.Error())
                        }
                        else
                        {
                            Helper.showAlert(message: "",head: StringConstants.Error())
                        }
                    }else{
                        Helper.showAlert(message: "",head: StringConstants.Error())
                    }
                }
                
            }, onError: { (Error) in
                Helper.showAlert(message: Error.localizedDescription,head: StringConstants.Error())
                Helper.hidePI()
            }).disposed(by: APIModel.disposeBag)
            return Disposables.create()
        }.share(replay: 1)
    }
    
    class func apiCall(method : Alamofire.HTTPMethod, api:String, params:[String:Any],header:[String:String]) -> Observable<(Bool,Any)> {
        
        Helper.showPI(string: "")
        return Observable.create { observer in
            RxAlamofire.requestJSON(method, APIModel.APIkeys.baseURL + api, parameters: params, headers: header).debug().subscribe(onNext: { (head, body) in
                
                Helper.hidePI()
                let errNum = APIModel.ErrorCode(rawValue: head.statusCode)!
                if errNum == .success {
                    if let bodyInTemp = body as? [String:Any] {
                        let bodyIn = Helper.nullKeyRemoval(data: bodyInTemp)
                        if let datagot = bodyIn[APIModel.APIkeys.status2] as? Int {
                            if datagot == 1 {
                                if let dataGot = bodyIn[APIModel.APIkeys.data] as? [String : Any] {
                                    observer.onNext((true, dataGot))
                                    observer.onCompleted()
                                }else if let dataGot = bodyIn[APIModel.APIkeys.data] as? [Any] {
                                    observer.onNext((true, dataGot))
                                    observer.onCompleted()
                                }else {
                                    observer.onNext((true, bodyIn))
                                    observer.onCompleted()
                                }
                            }else{
                                guard let dataGot2 = bodyIn[APIModel.APIkeys.message] as? String else {
                                    Helper.showAlert(message: StringConstants.Error(), head: "")
                                    return
                                }
                                Helper.showAlert(message: dataGot2, head: "")
                            }
                            
                            
                        }else if let datagot = bodyIn[APIModel.APIkeys.status] as? Int {
                            if datagot == 1 {
                                if let dataGot = bodyIn[APIModel.APIkeys.data] as? [String : Any] {
                                    observer.onNext((true, dataGot))
                                    observer.onCompleted()
                                }else if let dataGot = bodyIn[APIModel.APIkeys.data] as? [Any] {
                                    observer.onNext((true, dataGot))
                                    observer.onCompleted()
                                }
                                else {
                                    observer.onNext((true, bodyIn))
                                    observer.onCompleted()
                                }
                            }else{
                                guard let dataGot2 = bodyIn[APIModel.APIkeys.message] as? String else {
                                    Helper.showAlert(message: StringConstants.Error(), head: "")
                                    return
                                }
                                Helper.showAlert(message: dataGot2, head: "")
                            }
                            
                            
                        }else if let datagot = bodyIn[APIModel.APIkeys.status] as? String {
                            if datagot == "success" {
                                if let dataGot = bodyIn[APIModel.APIkeys.data] as? [String : Any] {
                                    observer.onNext((true, dataGot))
                                    observer.onCompleted()
                                }else if let dataGot = bodyIn[APIModel.APIkeys.data] as? [Any] {
                                    observer.onNext((true, dataGot))
                                    observer.onCompleted()
                                }
                                else {
                                    observer.onNext((true, bodyIn))
                                    observer.onCompleted()
                                }
                            }else{
                                guard let dataGot2 = bodyIn[APIModel.APIkeys.message] as? String else {
                                    Helper.showAlert(message: StringConstants.Error(), head: "")
                                    return
                                }
                                Helper.showAlert(message: dataGot2, head: "")
                            }
                            
                            
                        }
                        else{
                            Helper.showAlert(message: "", head: StringConstants.Error())
                        }
                    }else{
                        observer.onNext((true, body))
                        observer.onCompleted()
                    }
                }else{
                    observer.onNext((false, [:]))
                    observer.onCompleted()
                    if let bodyIn = body as? [String:Any] {
                        if let dataGot = bodyIn[APIModel.APIkeys.message] as? String {
                            Helper.showAlert(message: dataGot,head: StringConstants.Error())
                        }else if let dataGot = bodyIn[APIModel.APIkeys.status2] as? String {
                            Helper.showAlert(message: dataGot, head: "")
                        }else if let dataGot = bodyIn[APIModel.APIkeys.status2] as? [String:Any] {
                            let msg = dataGot.first?.value as! [String]
                            Helper.showAlert(message: msg[0], head: "")
                        }else{
                            let errs = bodyIn.values.first as? [String]
                            if errs == nil{
                                Helper.showAlert(message: "" ,head: StringConstants.Error())
                            }else{
                                Helper.showAlert(message: errs?[0] ?? "" ,head: StringConstants.Error())
                            }
                        }
                    }else{
                        Helper.showAlert(message: "",head: StringConstants.Error())
                    }
                }
                
            }, onError: { (Error) in
                Helper.showAlert(message: Error.localizedDescription,head: StringConstants.Error())
                Helper.hidePI()
            }).disposed(by: APIModel.disposeBag)
            return Disposables.create()
        }.share(replay: 1)
    }
}

class APIKeys {
    static let baseTokenURLFirst = ""
    let baseTokenURL = baseTokenURLFirst + ""
    static let baseURLFirst = "http://localhost/blueprint/app/public/"
    let baseURL = baseURLFirst + "rest/api"
    lazy var dataF : String = {
        return ""
    }()
    
    lazy var data : String = {
        return "data"
    }()
    
    lazy var message : String = {
        return "msg"
    }()
    lazy var status : String = {
        return "status"
    }()
    lazy var status2 : String = {
        return "message"
    }()
}

