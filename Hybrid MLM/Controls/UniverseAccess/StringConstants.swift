//
//  StringConstants.swift
//  event
//
//  Created by Shahna Nabeel on 25/02/19.
//  Copyright © 2019 Shahna Nabeel. All rights reserved.
//

import UIKit

class StringConstants: NSObject {
    
    //refresh token
    class func tokenExpired() -> String {
        return "Refresh token expired, Please login again!".localised
    }
    class func Sorry() -> String {
        return "Sorry".localised
    }
    class func Request() -> String {
        return "Request".localised
    }
    class func Mail() -> String {
        return "Mail".localised
    }
    //signup
    class func UserName() -> String {
        return "User name".localised
    }
    class func Email() -> String
    {
        return "Email".localised
    }
    class func Phone() -> String
    {
        return "Phone".localised
    }
    class func PhoneNumber() -> String
    {
        return "Phone Number".localised
    }
    class func Password() -> String
    {
        return "Password".localised
    }
    class func SuccessfullyRegistered() -> String
    {
        return "Successfully Registered".localised
    }
    
    //login
    class func refreshToken() -> String{
        return "refreshToken".localised
    }
    class func accessToken() -> String{
        return "accessToken".localised
    }
    class func Error() -> String {
        return "Error".localised
    }
    class func Done() -> String {
        return "Done".localised
    }
    class func Cancel() -> String {
        return "Cancel".localised
    }
    class func Female() -> String {
        return "Female".localised
    }
    class func Male() -> String
    {
        return "Male".localised
    }
    class func Expiry() -> String
    {
        return "Expiry".localised
    }
    class func EnterYourPassword() -> String
    {
        return "Enter your password".localised
    }
    class func Note() -> String {
        return "Note".localised
    }
    class func EnteraValidEmail() -> String
    {
        return "Enter a valid email".localised
    }
    class func Your() -> String
    {
        return "Your ".localised
    }
    
    
    //forgot password
    class func EnterPhone() -> String
    {
        return "Enter your phone number".localised
    }
    class func MyBooking() -> String
    {
        return "MyBooking".localised
    }
    class func Offers() -> String
    {
        return "Offers".localised
    }
    class func InviteFriends() -> String
    {
        return "InviteFriends".localised
    }
    class func Logout() -> String
    {
        return "Logout".localised
    }
    class func MyFavourites() -> String
    {
        return "My Favourites".localised
    }
    class func InvalidEmail() -> String
    {
        return "Invalid Email".localised
    }
    class func EnterValidName() -> String
    {
        return "Enter Valid Name".localised
    }
    class func EnterValidPhone() -> String {
        return "Enter Valid Phone Number".localised
    }
    class func EnterValidPassword() -> String {
        return "Enter Valid Password of minimum 6 characters".localised
    }
    class func Warning() -> String {
        return "Warning".localised
    }
    class func AreYouSureToLogout() -> String {
        return "Are You Sure To Logout".localised
    }
    
    class func LoggedOut() -> String {
        return "Logged Out Successfully".localised
    }
    class func ChoosePhoto() -> String {
        return "Choose Photo".localised
    }
    class func TakePhoto() -> String {
        return "Take Photo".localised
    }
    class func PickFromLibrary() -> String {
        return "Pick From Library".localised
    }
    class func Remove() -> String {
        return "Remove".localised
    }
    class func NoCamera() -> String {
        return "No Camera".localised
    }
    class func OK() -> String {
        return "OK".localised
    }
    class func NoCameraMsg() -> String {
        return "Camera is not available in your device.".localised
    }
    class func ProfileUpdated() -> String {
        return "Profile updated successfully.".localised
    }
    class func Success() -> String {
        return "Success.".localised
    }
    class func IncorrectOtp() -> String {
        return "Incorrect OTP.".localised
    }
    class func EnterOtp() -> String {
        return "Enter OTP.".localised
    }
    class func ConfirmPassword() -> String {
        return "Confirm Password".localised
    }
    class func Confirm() -> String {
        return "Confirm".localised
    }
    class func EnterNewPassword() -> String
    {
        return "Enter New Password".localised
    }
    class func CancelBooking() -> String
    {
        return "Cancel Booking".localised
    }
    class func AddReview() -> String
    {
        return "Add Review".localised
    }
    class func SponsorName() -> String
    {
        return "Sponser Name".localised
    }
    
    //Home
    class func All() -> String
    {
        return "All".localised
    }
    class func lastWeek() -> String
    {
        return "last Week".localised
    }
    class func lastMonth() -> String
    {
        return "last Month".localised
    }
    class func lastYear() -> String
    {
        return "last Year".localised
    }
    class func all() -> String
    {
        return "all".localised
    }
    class func week() -> String
    {
        return "week".localised
    }
    class func month() -> String
    {
        return "month".localised
    }
    class func year() -> String
    {
        return "year".localised
    }
    class func Main() -> String
    {
        return "Main".localised
    }
    class func column() -> String
    {
        return "column".localised
    }
    class func pie() -> String
    {
        return "pie".localised
    }
    class func Amount() -> String
    {
        return "Amount".localised
    }
    class func area() -> String
    {
        return "area".localised
    }
    class func circle() -> String
    {
        return "circle".localised
    }
    class func timesAgo() -> String
    {
        return "timesAgo".localised
    }
    
    //payout history
    class func PayoutRequest() -> String
    {
        return "Payout Request".localised
    }
    class func PayoutRelease() -> String
    {
        return "Payout Release".localised
    }
    
    //side menu
    class func Payout() -> String
    {
        return "Payout".localised
    }
    class func Reports() -> String
    {
        return "Reports".localised
    }
    class func Support() -> String
    {
        return "Support".localised
    }
    class func PrivacyPolicy() -> String
    {
        return "Privacy & Policy".localised
    }
    class func TermsConditions() -> String
    {
        return "Terms & Conditions".localised
    }
    
    
    //profile
    class func AccountInfo() -> String
    {
        return "Account Info".localised
    }
    class func Personal() -> String
    {
        return "Personal".localised
    }
    class func Social() -> String
    {
        return "Social".localised
    }
    class func Security() -> String
    {
        return "Security".localised
    }
    class func Localization() -> String
    {
        return "Localization".localised
    }
    class func Currency() -> String
    {
        return "Currency".localised
    }
    class func Language() -> String
    {
        return "Language".localised
    }
    
    //localization
    class func iPhoneX() -> String
    {
        return "iPhone X".localised
    }
    class func iPhoneXS() -> String
    {
        return "iPhone XS".localised
    }
    class func iPhoneXSMax() -> String
    {
        return "iPhone XS Max".localised
    }
    class func iPhoneXR() -> String
    {
        return "iPhone XR".localised
    }
    class func iPhone11() -> String
    {
        return "iPhone 11".localised
    }
    class func iPhone11Pro() -> String
    {
        return "iPhone 11 Pro".localised
    }
    class func iPhone11ProMax() -> String
    {
        return "iPhone 11 Pro Max".localised
    }
    class func iPhone12mini() -> String
    {
        return "iPhone 12 mini".localised
    }
    class func iPhone12() -> String
    {
        return "iPhone 12".localised
    }
    class func iPhone12Pro() -> String
    {
        return "iPhone 12 Pro".localised
    }
    class func iPhone12ProMax() -> String
    {
        return "iPhone 12 Pro Max".localised
    }
    class func iPhone13mini() -> String
    {
        return "iPhone 13 mini".localised
    }
    class func iPhone13() -> String
    {
        return "iPhone 13".localised
    }
    class func iPhone13Pro() -> String
    {
        return "iPhone 13 Pro".localised
    }
    class func iPhone13ProMax() -> String
    {
        return "iPhone 13 Pro Max".localised
    }
    
    //edit profile
    class func Edit() -> String
    {
        return "Edit".localised
    }
    class func Save() -> String
    {
        return "Save".localised
    }
    class func State() -> String {
        return "State".localised
    }
    class func Country() -> String {
        return "Country".localised
    }
    class func SelectedCountry() -> String {
        return "SelectedCountry".localised
    }
    class func ProfileUpdatedSuccessfully() -> String {
        return "Profile updated successfully".localised
    }
    class func FirstNameMissing() -> String
    {
        return "First name missing".localised
    }
    class func LastNameMissing() -> String {
        return "Last name missing".localised
    }
    class func EnterYourDOB() -> String {
        return "Enter your date of birth".localised
    }
    class func EnterYourGender() -> String {
        return "Enter your gender".localised
    }
    class func  EnterYourAddress() -> String {
        return "Enter your address".localised
    }
    class func EnterYourState() -> String
    {
        return "Enter your state".localised
    }
    class func EnterYourCountry() -> String {
        return "Enter your country".localised
    }
    class func EnterYourCity() -> String {
        return "Enter your city".localised
    }
    class func EnterYourPincode() -> String {
        return "Enter your Pincode".localised
    }
    class func  EnteraValidPincode() -> String {
        return "Enter a valid Pincode".localised
    }
    class func EnteraValidPhoneNumber() -> String {
        return "Enter a valid phone number".localised
    }
    class func LastnameOnlyAlphabets() -> String {
        return "Lastname should contain only alphabets".localised
    }
    class func  FirstnameOnlyAlphabets() -> String {
        return "Firstname should contain only alphabets".localised
    }
    //social
    class func  SocialInfoUpdated() -> String {
        return "Social info updated successfully".localised
    }
    
    //transactions
    class func NoCharges() -> String {
        return "No Charges".localised
    }
    class func  TXN() -> String {
        return "TXN  #".localised
    }
   
    
    //Networkvc
    class func Placement() -> String {
        return "Placement".localised
    }
    class func Sponsor() -> String {
        return "Sponsor".localised
    }
    
    //MyActivity
    class func en() -> String {
        return "en".localised
    }
    
    //Mail
    class func Inbox() -> String
    {
        return "Inbox".localised
    }
    class func SentItems() -> String
    {
        return "Sent items".localised
    }
    class func Draft() -> String
    {
        return "Draft".localised
    }
    class func StarredMail() -> String
    {
        return "Starred Mail".localised
    }
    class func Trash() -> String
    {
        return "Trash".localised
    }
    
    
    //inbox
    class func SuccessfullyRemoved() -> String
    {
        return "Successfully removed".localised
    }
    class func UnstarredSuccessfully() -> String
    {
        return "Unstarred Successfully".localised
    }
    
    //compose
    class func EmptyContent() -> String
    {
        return "Empty Content".localised
    }
    class func ChooseRecipient() -> String
    {
        return "Choose Recipient".localised
    }
    class func Notee() -> String
    {
        return "Note:".localised
    }
    class func ComposeMail() -> String
    {
        return "Compose Mail...".localised
    }
    class func SuccessfullySend() -> String
    {
        return "Successfully Send".localised
    }
    class func FailedtoSendMail() -> String
    {
        return "Failed to send Mail".localised
    }
    
    //business report
    class func MyWallet() -> String
    {
        return "My Wallet".localised
    }
    
    //network
    class func applicationurlencoded() -> String
    {
        return "application/x-www-form-urlencoded".localised
    }
    class func ContentType() -> String
    {
        return "Content-Type".localised
    }
    class func POST() -> String
    {
        return "POST".localised
    }
    class func Authorization() -> String
    {
        return "Authorization".localised
    }
    class func getGenealogyViewparams() -> String
    {
        return "purpose=getGenealogyView&params[type]=".localised
    }
    class func Bearer() -> String
    {
        return "Bearer ".localised
    }
    
    //send Money
    class func ChooseAmount() -> String
    {
        return "Choose Amount".localised
    }
    class func Alert() -> String
    {
        return "Alert".localised
    }
    class func AddNote() -> String
    {
        return "Add Note".localised
    }
    
    //settings vc
    class func Settingsaresaved() -> String
    {
        return "Settings are saved".localised
    }
    
    //transaction password
    class func Enteryourtransactionpassword() -> String
    {
        return "Enter your transaction password".localised
    }
    
    //success transfer
    class func loading() -> String
    {
        return "loading".localised
    }
    class func TransferredSuccessfully() -> String
    {
        return "Transferred Successfully!".localised
    }
    
}


