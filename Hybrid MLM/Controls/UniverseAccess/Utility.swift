//
//  Utility.swift
//  event
//
//  Created by Shahna Nabeel on 25/02/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

struct DAteFormats {
    let datetodisplay = "EEE dd MMM yyyy"
    let timetodisplay = "hh:mm a"
    let dateAndTimeToDisplay = "EEE dd MMM yyyy, hh:mm a"
    let dateFromServer = "yyyy-MM-dd"
    let timeFromServer = "HH:mm:ss"
    let dob = "yyyy/MM/dd"
    let monthYear = "MMMM yyyy"
    let monthYearServer = "MM-yyyy"
}


class Utility: NSObject {
    static var loggedIn:Bool {
        var value = false
        if UserDefaults.standard.bool(forKey: UserDefaultConstants.LoggedIn) {
            value = UserDefaults.standard.bool(forKey: UserDefaultConstants.LoggedIn)
        }
        return value
    }
    
    class func setLogin(value:Bool) {
        UserDefaults.standard.set(value, forKey: UserDefaultConstants.LoggedIn)
        UserDefaults.standard.synchronize()
    }
    
    static var PushToken:String {
        var value = ""
        if (UserDefaults.standard.string(forKey: UserDefaultConstants.PushTocken) != nil) {
            value = UserDefaults.standard.string(forKey: UserDefaultConstants.PushTocken)!
        }
        return value
    }
    
    class func savePushToken(value:String) {
        UserDefaults.standard.set(value, forKey: UserDefaultConstants.PushTocken)
        UserDefaults.standard.synchronize()
    }
    
    static var UserData:User {
        var value = User.init(data: [:])
        if let user = UserDefaults.standard.object(forKey: UserDefaultConstants.User) as? [String:Any] {
            value = User.init(data: user)
        }
        return value
    }
    class func saveuserImage(valueIn:String) {
        var value:[String:Any] = [:]
        if let user = UserDefaults.standard.object(forKey: UserDefaultConstants.User) as? [String:Any] {
            value = user
        }
        value.updateValue(valueIn, forKey: "image")
        UserDefaults.standard.set(value, forKey: UserDefaultConstants.User)
        UserDefaults.standard.synchronize()
    }
    class func saveuserData(value:[String:Any]) {
        UserDefaults.standard.set(value, forKey: UserDefaultConstants.User)
        UserDefaults.standard.synchronize()
    }
    class func recipientsData(value:[Int],names:[String]) {
        if UserDefaults.standard.value(forKey: UserDefaultConstants.Recipients) == nil{
            UserDefaults.standard.set(value, forKey: UserDefaultConstants.Recipientsid)
            UserDefaults.standard.set(names, forKey: UserDefaultConstants.Recipients)
        }
        else{
            var param = UserDefaults.standard.value(forKey: UserDefaultConstants.Recipientsid) as? [Int]
            param?.append(contentsOf: value)
            UserDefaults.standard.set(param, forKey: UserDefaultConstants.Recipientsid)
            //names
            var params = UserDefaults.standard.value(forKey: UserDefaultConstants.Recipients) as? [String]
            params?.append(contentsOf: names)
            UserDefaults.standard.set(params, forKey: UserDefaultConstants.Recipients)
        }
        UserDefaults.standard.synchronize()
    }
    class func clearRecipientData(_ key: String){
        if key != "all"{
            let index = key.integerValue!
            var data = UserDefaults.standard.value(forKey: UserDefaultConstants.Recipients) as? [String]
            data?.remove(at: index)
            //names
            var data2 = UserDefaults.standard.value(forKey: UserDefaultConstants.Recipientsid) as? [Int]
            data2?.remove(at: index)
            UserDefaults.standard.removeObject(forKey: UserDefaultConstants.Recipients)
            UserDefaults.standard.set(data, forKey: UserDefaultConstants.Recipients)
            UserDefaults.standard.removeObject(forKey: UserDefaultConstants.Recipientsid)
            UserDefaults.standard.set(data2, forKey: UserDefaultConstants.Recipientsid)
        }
        else{
            UserDefaults.standard.removeObject(forKey: UserDefaultConstants.Recipients)
            UserDefaults.standard.removeObject(forKey: UserDefaultConstants.Recipientsid)
        }
    }
}
struct UserDefaultConstants {
    static let LoggedIn                                 = "LoggedIn"
    static let PushTocken                               = "PushTocken"
    static let User                                     = "User"
    static let Recipients                               = "Recipients"
    static let Recipientsid                               = "Recipientsid"
}
