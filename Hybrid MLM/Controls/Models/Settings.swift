//
//  Settings.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 30/03/21.
//

import Foundation

struct Settings{
    var id = 0
    var user_id = 0
    var transaction_password = ""
    var ip = ""
    var ip_status = ""
    var status = 1
    
    init(data:[String:Any]) {
        if let temp = data["id"] as? Int {
            id = temp
        }
        if let temp = data["user_id"] as? Int{
            user_id = temp
        }
        if let temp = data["transaction_password"] as? String{
            transaction_password = temp
        }
        if let temp = data["ip"] as? String{
            ip = temp
        }
        if let temp = data["ip_status"] as? String{
            ip_status = temp
        }
        if let temp = data["status"] as? Int{
            status = temp
        }
    }
}
