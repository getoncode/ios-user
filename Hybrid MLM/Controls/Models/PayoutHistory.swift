//
//  PayoutHistory.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 05/07/21.
//

import Foundation

struct PayoutReqHistory{
    var walletName = ""
    var  dataAry:[String:Any] = [:]
    init(data:[String:Any]) {
        if let temp = data["payoutRequestData"] as? [String:Any] {
            dataAry = temp
        }
        if let temp = data["wallet"] as? String{
            walletName = temp
        }
}
}
struct PayoutRelHistory{
    var created_at = Date()
    var amount = 0.0
    var wallet = ""
    var dataAry:[String:Any] = [:]
    var transactionAry:[String:Any] = [:]
    init(data:[String:Any]) {
        if let temp = data["operation"] as? [String:Any] {
           dataAry = temp
       }
        if let temp = data["payer_user"] as? [String:Any] {
            transactionAry = temp
        }
        if let temp = dataAry["created_at"] as? String{
            created_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
        if let temp = data["actual_amount"] as? Double{
            amount = temp
        }
             
}
}
