//
//  Token.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/01/21.
//

import Foundation

struct Token {
    var expires = 0.0
    var tokenType = ""
    var refreshToken = ""
    var accessToken = ""
    
    init(data:[String:Any]) {
            if let temp = data["expires_in"] as? Double {
                expires = temp
            }
            if let temp = data["token_type"] as? String {
                tokenType = temp
            }
            if let temp = data["refresh_token"] as? String {
                refreshToken = temp
            }
            if let temp = data["access_token"] as? String {
                accessToken = temp
            }
    }
}
