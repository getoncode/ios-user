//
//  Personal.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 28/01/21.
//

import Foundation

struct Personal {
    var username = ""
    var profilePic = ""
    var firstname = ""
    var lastname = ""
    var email = ""
    var phone = ""
    var dob = ""
    var gender = ""
    var address = ""
    var state = ""
    var country = ""
    var city = ""
    var pin = 0
    var country_id = 0
    var state_id = 0
    
    init(data:[String:Any]) {
        if let temp = data["username"] as? String{
            username = temp
        }
        if let temp = data["profilePic"] as? String {
            profilePic = temp
        }
        if let temp = data["firstname"] as? String {
            firstname = temp
        }
        if let temp = data["lastname"] as? String {
            lastname = temp
        }
        if let temp = data["email"] as? String {
            email = temp
        }
        if let temp = data["phone"] as? String {
            phone = temp
        }
        if let temp = data["dob"] as? String {
            dob = temp
        }
        if let temp = data["gender"] as? String {
            gender = temp
        }
        if let temp = data["address"] as? String {
            address = temp
        }
        if let temp = data["state"] as? String {
            state = temp
        }
        if let temp = data["country"] as? String {
            country = temp
        }
        if let temp = data["city"] as? String{
            city = temp
        }
        if let temp = data["pin"] as? Int{
            pin = temp
        }
        if let temp = data["country_id"] as? Int{
            country_id = temp
        }
        if let temp = data["state_id"] as? Int{
            state_id = temp
        }
    }
}

struct Account {
    var memberId = ""
    var username = ""
    var sponsor = ""
    var placement = ""
    
    init(data:[String:Any]) {
            if let temp = data["memberId"] as? String {
                memberId = temp
            }
            if let temp = data["username"] as? String {
                username = temp
            }
            if let temp = data["sponsor"] as? String {
                sponsor = temp
            }
            if let temp = data["placement"] as? String {
                placement = temp
            }
    }
}



struct Social {
    var aboutMe = ""
    var facebook = ""
    var twitter = ""
    var linkedIn = ""
    var instagram = ""
    
    init(data:[String:Any]) {
        if let temp = data["aboutMe"] as? String {
            aboutMe = temp
        }
        if let temp = data["facebook"] as? String {
            facebook = temp
        }
        if let temp = data["twitter"] as? String {
            twitter = temp
        }
        if let temp = data["linkedIn"] as? String {
            linkedIn = temp
        }
        if let temp = data["instagram"] as? String {
            instagram = temp
        }
    }
}

