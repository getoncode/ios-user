//
//  Activity.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 10/04/21.
//

import Foundation
struct Activity {
    var timestamp = ""
    var description : [String:Any] = [:]

    init(data:[String:Any]) {
        if let temp = data["timestamp"] as? String {
            timestamp = temp
        }
        if let temp = data["description"] as? [String:Any]{
            description = temp
        }
    }
}
