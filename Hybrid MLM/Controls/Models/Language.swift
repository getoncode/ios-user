//
//  Language.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 23/01/21.
//

import Foundation

struct Language {
    var id = ""
    var name = ""
    var code = ""
    var active = ""
    var native = ""
    
    init(data:[String:Any]) {
        if let temp = data["id"] as? String {
            id = temp
        }
        if let temp = data["name"] as? String {
            name = temp
        }
        if let temp = data["code"] as? String {
            code = temp
        }
        if let temp = data["active"] as? String {
            active = temp
        }
        if let temp = data["native"] as? String {
            native = temp
        }
    }
}
