//
//  Transaction.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 03/02/21.
//

import Foundation

struct Transaction {
    var id = 0
    var operation = ""
    var amount = ""
    var payer = ""
    var payee = ""
    var operationMeta = ""
    var hour = ""
    var date = Date()
    var actualAamount = ""
    var charges = ""
    var orders = ""
    
    
    init(data:[String:Any]) {
        if let temp = data["id"] as? Int {
            id = temp
        }
        if let temp = data["operation"] as? String {
            operation = temp
        }
        if let temp = data["amount"] as? String {
            amount = temp
        }
        if let temp = data["payer"] as? String {
            payer = temp
        }
        if let temp = data["payee"] as? String {
            payee = temp
        }
        if let temp = data["operationMeta"] as? String {
            operationMeta = temp
        }
        if let temp = data["hour"] as? String {
            hour = temp
        }
        if let temp = data["date"] as? String {
            date = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
        if let temp = data["actualAamount"] as? String {
            actualAamount = temp
        }
        if let temp = data["charges"] as? String {
            charges = temp
        }
        if let temp = data["orders"] as? String {
            orders = temp
        }
    }
}
