//
//  Mail.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 18/05/21.
//

import Foundation

struct Inbox {
    var subject = ""
    var content = ""
    var sender_name = ""

    var mailId = 0
    var is_starred = 0
    var receiver_name = ""
    var created_at = Date()
    var updated_at = Date()

    init(data:[String:Any]) {
        if let temp = data["subject"] as? String {
            subject = temp
        }
        if let temp = data["content"] as? String {
            content = temp
        }
        if let temp = data["is_starred"] as? Int{
            is_starred = temp
        }
        if let temp = data["mailId"] as? Int{
            mailId = temp
        }
        if let temp = data["sender_name"] as? String{
            sender_name = temp
        }
        if let temp = data["receiver_name"] as? String {
            receiver_name = temp
        }
        if let temp = data["created_at"] as? String {
            created_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
        if let temp = data["updated_at"] as? String {
            updated_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
    }
}

struct SentMail {
    var subject = ""
    var content = ""
    var mailId = 0
    var is_starred = 0
    var sender_name = ""
    var created_at = Date()
    var updated_at = Date()

    init(data:[String:Any]) {
        if let temp = data["subject"] as? String {
            subject = temp
        }
        if let temp = data["content"] as? String {
            content = temp
        }
        if let temp = data["mailId"] as? Int{
            mailId = temp
        }
        if let temp = data["is_starred"] as? Int{
            is_starred = temp
        }
        if let temp = data["sender_name"] as? String {
            sender_name = temp
        }
        if let temp = data["created_at"] as? String {
            created_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
        if let temp = data["updated_at"] as? String {
            updated_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
    }
}


struct saveDraft {
    var id = 0
    var subject = ""
    var content = ""
    var reply_to = ""
    var created_at = Date()
    var updated_at = Date()

    init(data:[String:Any]) {
        if let temp = data["id"] as? Int {
            id = temp
        }
        if let temp = data["subject"] as? String {
            subject = temp
        }
        if let temp = data["content"] as? String{
            content = temp
        }
        if let temp = data["reply_to"] as? String{
            reply_to = temp
        }
        if let temp = data["created_at"] as? String {
            created_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
        if let temp = data["updated_at"] as? String {
            updated_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
    }
}

struct Draft {
    var mailId = 0
    var recipient_id = 0
    var subject = ""
    var content = ""
    var recipient_name = ""
    var sender_name = ""
    var created_at = Date()
    var updated_at = Date()

    init(data:[String:Any]) {
        if let temp = data["mailId"] as? Int {
            mailId = temp
        }
        if let temp = data["recipient_id"] as? Int {
            recipient_id = temp
        }
        if let temp = data["subject"] as? String {
            subject = temp
        }
        if let temp = data["content"] as? String{
            content = temp
        }
        if let temp = data["recipient_name"] as? String{
            recipient_name = temp
        }
        if let temp = data["sender_name"] as? String{
            sender_name = temp
        }
        if let temp = data["created_at"] as? String {
            created_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
        if let temp = data["updated_at"] as? String {
            updated_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
    }
}


struct StarredMail {
    var subject = ""
    var content = ""
    var receiver_name = ""
    var mailId = 0
    var created_at = Date()
    var updated_at = Date()

    init(data:[String:Any]) {
        if let temp = data["subject"] as? String {
            subject = temp
        }
        if let temp = data["content"] as? String {
            content = temp
        }
        if let temp = data["mailId"] as? Int{
            mailId = temp
        }
        if let temp = data["receiver_name"] as? String {
            receiver_name = temp
        }
        if let temp = data["created_at"] as? String {
            created_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
        if let temp = data["updated_at"] as? String {
            updated_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
    }
}


struct TrashedMail {
    var subject = ""
    var content = ""
    var receiver_name = ""
    var created_at = Date()
    var updated_at = Date()

    init(data:[String:Any]) {
        if let temp = data["subject"] as? String {
            subject = temp
        }
        if let temp = data["content"] as? String {
            content = temp
        }
        if let temp = data["receiver_name"] as? String {
            receiver_name = temp
        }
        if let temp = data["created_at"] as? String {
            created_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
        if let temp = data["updated_at"] as? String {
            updated_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
    }
}
