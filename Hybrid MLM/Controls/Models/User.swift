//
//  User.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 23/01/21.
//

import Foundation

struct User {
    var username = ""
    var profilePic = ""
    var firstname = ""
    var lastname = ""
    var email = ""
    var phone = ""
    var dob = ""
    var gender = ""
    var address = ""
    var state = ""
    var country = ""
    var city = ""
    var pin = ""
    var country_id = 0
    var state_id = 0
    var FullData:[String:Any] = [:]
    
    init(data:[String:Any]) {
        FullData = data
        if let temp = data["username"] as? String{
            username = temp
        }
        if let temp = data["profilePic"] as? String {
            profilePic = temp
        }
        if let temp = data["firstname"] as? String {
            firstname = temp
        }
        if let temp = data["lastname"] as? String {
            lastname = temp
        }
        if let temp = data["email"] as? String {
            email = temp
        }
        if let temp = data["phone"] as? String {
            phone = temp
        }
        if let temp = data["dob"] as? String {
            dob = temp
        }
        if let temp = data["gender"] as? String {
            gender = temp
        }
        if let temp = data["address"] as? String {
            address = temp
        }
        if let temp = data["state"] as? String {
            state = temp
        }
        if let temp = data["country"] as? String {
            country = temp
        }
        if let temp = data["city"] as? String{
            city = temp
        }
        if let temp = data["pin"] as? String{
            pin = temp
        }
        if let temp = data["country_id"] as? Int{
            country_id = temp
        }
        if let temp = data["state_id"] as? Int{
            state_id = temp
        }
    }
}

