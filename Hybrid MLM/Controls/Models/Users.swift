//
//  Users.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 17/06/21.
//

import Foundation

struct Users {
    var id = 0
    var username = ""
    var email = ""
    var created_at = Date()
    var updated_at = Date()

    init(data:[String:Any]) {
        if let temp = data["id"] as? Int {
            id = temp
        }
        if let temp = data["username"] as? String {
            username = temp
        }
        if let temp = data["email"] as? String {
            email = temp
        }
        if let temp = data["created_at"] as? String {
            created_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
        if let temp = data["updated_at"] as? String {
            updated_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
    }
}
