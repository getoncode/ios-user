//
//  Countries.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 29/01/21.
//
import Foundation

struct Countries {
    var id = 0
    var code = ""
    var name = ""
    var phonecode = ""
    var phonemask = ""
    var active = 1
   
    init(data:[String:Any]) {
        if let temp = data["id"] as? Int {
            id = temp
        }
        if let temp = data["code"] as? String {
            code = temp
        }
        if let temp = data["name"] as? String {
            name = temp
        }
        if let temp = data["phonecode"] as? String {
            phonecode = temp
        }
        if let temp = data["phonemask"] as? String {
            phonemask = temp
        }
        if let temp = data["active"] as? Int {
            active = temp
        }
    }
}


struct States {
    var id = 0
    var name = ""
    var active = 1
    var country_id = 0
   
    init(data:[String:Any]) {
        if let temp = data["id"] as? Int {
            id = temp
        }
        if let temp = data["name"] as? String {
            name = temp
        }
        if let temp = data["active"] as? Int {
            active = temp
        }
        if let temp = data["country_id"] as? Int {
            country_id = temp
        }
    }
}
