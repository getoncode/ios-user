//
//  Languages.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 22/01/21.
//

import Foundation

struct Languages {
    var id = ""
    var name = ""
    var code = ""
    var symbol = ""
    var format = ""
    var exchange_rate = ""
    var active = ""
    
    init(data:[String:Any]) {
        if let temp = data["id"] as? String {
            id = temp
        }
        if let temp = data["name"] as? String {
            name = temp
        }
        if let temp = data["code"] as? String {
            code = temp
        }
        if let temp = data["symbol"] as? String {
            symbol = temp
        }
        if let temp = data["format"] as? String {
            format = temp
        }
        if let temp = data["exchange_rate"] as? String {
            exchange_rate = temp
        }
        if let temp = data["active"] as? String {
            active = temp
        }
    }
}
