//
//  Register.swift
//  Hybrid MLM
//
//  Created by Acemero on 10/04/21.
//

import Foundation

struct Register {
    var username = ""
    var email = ""
    var phone = ""
    var member_id = ""
    var status = ""
    var updated_at = ""
    var created_at = ""
    var id = 0
    
    init(data:[String:Any]) {
        if let temp = data["username"] as? String {
            username = temp
        }
        if let temp = data["email"] as? String {
            email = temp
        }
        if let temp = data["phone"] as? String {
            phone = temp
        }
        if let temp = data["member_id"] as? String {
            member_id = temp
        }
        if let temp = data["status"] as? String {
            status = temp
        }
        if let temp = data["updated_at"] as? String {
            updated_at = temp
        }
        if let temp = data["created_at"] as? String {
            created_at = temp
        }
        if let temp = data["id"] as? Int {
            id = temp
        }
    }
}
