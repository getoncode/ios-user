//
//  Send.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 22/03/21.
//

import Foundation


struct Recipients{
    var id = 0
    var member_id = ""
    var username = ""
    var email = ""
    var phone = ""
    var google2fa_secret = ""
    
    init(data:[String:Any]) {
        if let temp = data["id"] as? Int {
            id = temp
        }
        if let temp = data["member_id"] as? String {
            member_id = temp
        }
        if let temp = data["username"] as? String {
            username = temp
        }
        if let temp = data["email"] as? String {
            email = temp
        }
        if let temp = data["phone"] as? String {
            phone = temp
        }
        if let temp = data["google2fa_secret"] as? String{
            google2fa_secret = temp
        }
    }
}


struct Transfer{
    var id = 0
    var name = ""
    init(data:[String:Any]) {
    if let temp = data["id"] as? Int {
        id = temp
    }
    if let temp = data["name"] as? String{
        name = temp
    }
    }
}
struct Payout{
    var id = 0
    var name = ""
    init(data:[String:Any]) {
    if let temp = data["id"] as? Int {
        id = temp
    }
    if let temp = data["name"] as? String{
        name = temp
    }
    }
}
