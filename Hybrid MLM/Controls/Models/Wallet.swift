//
//  Wallet.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 12/07/21.
//

import Foundation

struct OverView {
    var monthlyIncome : [Any] = []
    var xAxises : [String] = []
    
    init(data:[String:Any]) {
        if let temp = data["monthlyIncome"] as? [Any]{
            monthlyIncome = temp
        }
        if let temp = data["xAxises"] as? [String]{
            xAxises = temp
        }
    }
}
