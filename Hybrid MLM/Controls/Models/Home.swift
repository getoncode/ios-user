//
//  Home.swift
//  Hybrid MLM
//
//  Created by Acemero on 27/04/21.
//

import Foundation

struct Dashboard {
    var filters : [String:Any] = [:]
    var wallets : [String:Any] = [:]
    var timesAgo = ""
    
    init(data:[String:Any]) {
        if let temp = data["filters"] as? [String:Any] {
            filters = temp
        }
        if let temp = data["wallets"] as? [String:Any] {
            wallets = temp
        }
        if let temp = data["timesAgo"] as? String{
            timesAgo = temp
        }
    }
}

struct earningGraph {
    var xAxises : [String] = []
    var graph : [Any] = []
    
    init(data:[String:Any]) {
        if let temp = data["xAxises"] as? [String]{
            xAxises = temp
        }
        if let temp = data["graph"] as? [Any] {
            graph = temp
        }
    }
}


struct earningPie {
    var transactions : [Any] = []
    var totalEarnedAmount : Double = 0.0
    
    init(data:[String:Any]) {
        if let temp = data["transactions"] as? [Any]{
            transactions = temp
        }
        if let temp = data["totalEarnedAmount"] as? Double {
            totalEarnedAmount = temp
        }
    }
}


struct payoutHistory {
    var id  = 0
    var user_id = 0
    var request_amount = 0.0
    var release_amount = 0.0
    var wallet = 0
    var gateway = 0
    var remark = ""
    var status = 0
    var transaction_id = 0
    var account = 0
    var amount = 0.0
    var created_at = Date()
    
    
    init(data:[String:Any]) {
        if let temp = data["id"] as? Int{
            id = temp
        }
        if let temp = data["user_id"] as? Int {
            user_id = temp
        }
        if let temp = data["request_amount"] as? Double{
            request_amount = temp
        }
        if let temp = data["release_amount"] as? Double {
            release_amount = temp
        }
        if let temp = data["wallet"] as? Int{
            wallet = temp
        }
        if let temp = data["gateway"] as? Int {
            gateway = temp
        }
        if let temp = data["remark"] as? String{
            remark = temp
        }
        if let temp = data["status"] as? Int {
            status = temp
        }
        if let temp = data["transaction_id"] as? Int {
            transaction_id = temp
        }
        if let temp = data["account"] as? Int{
            account = temp
        }
        if let temp = data["created_at"] as? String {
          created_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
        if let temp = data["amount"] as? Double{
            amount = temp
        }
    }
}
