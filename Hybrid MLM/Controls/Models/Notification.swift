//
//  Notification.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 05/04/21.
//

import Foundation

struct Notification{
    var id = ""
    var type = ""
    var dataAry : [String:Any] = [:]
    var created_at = Date()
    init(data:[String:Any]) {
        if let temp = data["id"] as? String {
            id = temp
        }
        if let temp = data["type"] as? String{
            type = temp
        }
        if let temp = data["data"] as? [String:Any]{
            dataAry = temp
        }
        if let temp = data["created_at"] as? String {
            created_at = Helper.stringToDate(temp, "yyyy-MM-dd")
        }
    }
}

