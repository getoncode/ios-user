//
//  Commission.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 03/09/21.
//

import Foundation
struct Commission {
    var name  = ""
    var moduleId = 0
    init(data:[String:Any]){
        if let temp = data["name"] as? String{
           name = temp
        }
        if let temp = data["moduleId"] as? Int{
            moduleId = temp
        }
    }
}
