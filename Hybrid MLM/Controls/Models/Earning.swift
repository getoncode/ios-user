//
//  Earning.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 10/08/21.
//

import Foundation
struct Earning {
    var transaction_id = 0
    var commissionType = ""
    var transaction:[String:Any] = [:]
    var operation:[String:Any] = [:]
    var created_at = Date()

    init(data:[String:Any]) {
        if let temp = data["commissionType"] as? String {
            commissionType = temp
        }
        if let temp = data["operation"] as? [String:Any] {
            operation = temp
        }
        if let temp = data["transaction"] as? [String:Any] {
            transaction = temp
        }
        if let temp = data["transaction_id"] as? Int {
            transaction_id = temp
        }
        if let temp = data["created_at"] as? String {
            created_at  = Helper.stringToDate(temp, "dd-MM-yyyy")
        }
        
        
    }
}
