//
//  AttachmentCVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 01/12/20.
//

import UIKit

class AttachmentCVCell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 5.0
    }
}
