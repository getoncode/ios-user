//
//  ComposeVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 01/12/20.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit

class ComposeVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case Compose = 1
        case sendMail = 3
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var draftArray : [saveDraft] = []
   
    //compose
    func composeMail(recipient: Int, subject: String, content : String) {
        let params : [String:Any] = [
            "purpose" : "composeMail",
            "params[subject]" : subject,
            "params[content]" : content,
            "params[recipient]" : recipient,
            "params[reply_to]" : "0"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.draftArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    for item in [dataIn]{
                        self.draftArray.append(saveDraft.init(data: item["mail"] as! [String : Any]))
                    }
                }
                self.ResposeBack.onNext(typeHere.Compose)
            }
        }).disposed(by: disposeBag)
    }
    
    //save as new draft
    func sendMail(mailId: Int, recipient: Int) {
        let params : [String:Any] = [
            "purpose" : "sendMail",
            "params[recipient]" : recipient,
            "params[mail_id]" : mailId
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                self.ResposeBack.onNext(typeHere.sendMail)
            }
        }).disposed(by: disposeBag)
    }
}


