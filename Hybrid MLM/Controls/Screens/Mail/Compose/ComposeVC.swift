//
//  ComposeVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 01/12/20.
//

import UIKit

class ComposeVC: UIViewController, UITextViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var ToLabel: UILabel!
    @IBOutlet weak var recipientTF: UITextField!
    @IBOutlet weak var SendButton: UIButton!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var subjectTF: UITextField!
    @IBOutlet weak var mailArea: UITextView!
    var selectedRecipient = 0
    let composeVM = ComposeVM()
    var draftDetails : Draft? = nil
    var timer = Timer()
    var id = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        setVM()
        setData()
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.saveAsDraft), userInfo: nil, repeats: true)
    }
    
    //for draft
    func setData() {
        if draftDetails != nil{
            selectedRecipient = draftDetails?.recipient_id ?? 0
            mailArea.textColor = UIColor.darkGray
            subjectTF.text = draftDetails?.subject
            recipientTF.text = draftDetails?.recipient_name
            mailArea.text = draftDetails?.content
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    //set timer and call save to draft
    @objc func saveAsDraft()
    {
        if subjectTF.text?.count ?? 0 > 0 && mailArea.text.count > 0{
            composeVM.composeMail(recipient: selectedRecipient, subject: subjectTF.text ?? "", content: mailArea.text ?? "")
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sendMail(_ sender: Any) {
        //call saveAsDraft to get mailId
        if let draftId = draftDetails?.mailId{
            composeVM.sendMail(mailId: draftId, recipient: selectedRecipient)
        }else{
            self.saveAsDraft()
        }
        if self.id != 0{
        if recipientTF.text?.count ?? 0 > 0{
            if mailArea.text != nil{
                composeVM.sendMail(mailId: id, recipient: selectedRecipient)
            }
            else{
                Helper.showAlert(message: StringConstants.EmptyContent(), head: "")
            }
        }  
        else{
            Helper.showAlert(message: StringConstants.ChooseRecipient(), head: StringConstants.Notee())
        }
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == recipientTF{
            performSegue(withIdentifier: String(describing: ComposeVC.self)+String(describing: SelectUserVC.self), sender: nil)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        timer.invalidate()
        removeObserver()
    }
    //set placeholder for textview
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (mailArea.text == StringConstants.ComposeMail())
        {
            mailArea.text = nil
            mailArea.textColor = UIColor.darkGray
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if mailArea.text.isEmpty
        {
            mailArea.text = StringConstants.ComposeMail()
            mailArea.textColor = .placeholderText
        }
        textView.resignFirstResponder()
    }
    
    func setVM() {
        composeVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Compose:
                self?.id = self?.composeVM.draftArray[0].id ?? 0
                break
            case .sendMail:
                Helper.showAlertReturn(message: StringConstants.SuccessfullySend(), head: StringConstants.Mail(), type: StringConstants.OK(), Hide: true) { response in
                    if response {
                        self?.navigationController?.popViewController(animated: true)
                    }else{
                        Helper.showAlert(message: StringConstants.FailedtoSendMail(), head: StringConstants.Sorry())
                    }
                    self?.timer.invalidate()
                }
                break
            default:
                break
            }
        }).disposed(by: composeVM.disposeBag)
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: ComposeVC.self)+String(describing: SelectUserVC.self)
        {
            if let controller = segue.destination as? SelectUserVC {
                controller.responseBack.subscribe(onNext: { [weak self]data in
                    self?.recipientTF.text = data as? String
                    self?.recipientTF.resignFirstResponder()
                }).disposed(by: controller.disposebag)
            }
            if let controller = segue.destination as? SelectUserVC {
                controller.responseBack2.subscribe(onNext: { [weak self]data in
                    self?.selectedRecipient = data as! Int
                }).disposed(by: controller.disposebag)
            }
        }
    }
}
