//
//  ComposeUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 01/12/20.
//

import Foundation
import UIKit

extension ComposeVC
{
    func setUi()  {
        Helper.roundBorder(view: borderView, radius: 5.0, borderWidth: 0.7, color: #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1))
    }
}
