//
//  StarredMailVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 24/05/21.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit

class StarredMailVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case Starred = 1
        case Unstar = 2
        case Delete = 3
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var StarredMailArray : [StarredMail] = []
   
    //star
    func getStarredMail() {
        let params : [String:Any] = [
            "purpose" : "getStarredMails"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.StarredMailArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in dataIn{
                        self.StarredMailArray.append(StarredMail.init(data: item))
                    }
                }
                self.StarredMailArray.sort(by: {$0.created_at.compare($1.created_at) == .orderedDescending})
                self.ResposeBack.onNext(typeHere.Starred)
            }
        }).disposed(by: disposeBag)
    }
    
    //deleteMail
    func deleteMail(mailId : Int) {
        let params : [String:Any] = [
            "purpose" : "deleteMail",
            "params[mail_id]" : mailId
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.StarredMailArray.removeAll()
            if data.0 {
                self.ResposeBack.onNext(typeHere.Delete)
            }
        }).disposed(by: disposeBag)
    }
    
    //unstar
    func UnStarMail(mailId : Int) {
        let params : [String:Any] = [
            "purpose" : "unstarMail",
            "params[mail_id]" : mailId
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                self.ResposeBack.onNext(typeHere.Unstar)
            }
        }).disposed(by: disposeBag)
    }
}


