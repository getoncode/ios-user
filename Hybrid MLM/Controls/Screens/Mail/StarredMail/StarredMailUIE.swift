//
//  StarredMailUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 24/05/21.
//

import Foundation
import UIKit

extension StarredMailVC
{
    func setUi() {
     // ui changes here
        self.starredTable.isHidden = false
        self.noMailLabel.isHidden = true
        editView.isHidden = true
        composeButton.layer.cornerRadius = 25.0
    }
}
