//
//  StarredMailTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 26/05/21.
//

import UIKit

class StarredMailTVCell: UITableViewCell {
    @IBOutlet weak var checkBox: UIButton!
    @IBOutlet weak var receiverName: UILabel!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Helper.setButton(button: checkBox, view: checkBox, primaryColour: UIColor.white, seconderyColor: Colors.AppBaseColor, shadow: false, corner: 5, border: Colors.AppBaseColor, type: .system)
        checkBox.setImage(#imageLiteral(resourceName: "tick"), for: .selected)
        checkBox.imageView?.contentMode = .scaleAspectFit
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func checkBoxClick(_ sender: UIButton.State) {
        if checkBox.isSelected
        {
            checkBox.isSelected = false
            checkBox.backgroundColor = Colors.AppBaseColor
        }
        else
        {
            checkBox.isSelected = true
            checkBox.backgroundColor = UIColor.white
           
        }
    }
    
    func setData(_ data: StarredMail) {
        receiverName.text = data.receiver_name
        subjectLabel.text = data.subject
        dateLabel.text = Helper.dateToString(date: data.created_at, format: "dd MMM")
        descriptionLabel.text = data.content
    }

}
