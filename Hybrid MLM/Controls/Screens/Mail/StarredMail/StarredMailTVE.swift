//
//  StarredMailTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 26/05/21.
//

import Foundation
import UIKit

extension StarredMailVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        starredMailVM.StarredMailArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: StarredMailTVCell.self))) as! StarredMailTVCell
        tableView.rowHeight = 80
        cell.checkBox.isSelected = false
        cell.setData(starredMailVM.StarredMailArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard: UIStoryboard = UIStoryboard(name: StringConstants.Mail(), bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: ViewMailVC.self)) as! ViewMailVC
        vc.starredDetails = starredMailVM.StarredMailArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
