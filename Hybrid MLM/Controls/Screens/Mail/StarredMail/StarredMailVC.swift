//
//  StarredMailVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 24/05/21.
//

import UIKit

class StarredMailVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var composeButton: UIButton!
    @IBOutlet weak var noMailLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var starredTable: UITableView!
    var starredMailVM = StarredMailVM()
    var currentIndex : IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        starredMailVM.getStarredMail()
        setupLongPressGesture()
        setVM()
    }
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self
        self.starredTable.addGestureRecognizer(longPressGesture)
    }
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .began {
            let touchPoint = gestureRecognizer.location(in: self.starredTable)
            if let indexPath = starredTable.indexPathForRow(at: touchPoint) {
                editView.isHidden = false
                let cell = starredTable.cellForRow(at: indexPath) as? StarredMailTVCell
                currentIndex = indexPath
                cell?.checkBoxClick(UIControl.State.selected)
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    @IBAction func deleteMail(_ sender: Any) {
        starredMailVM.deleteMail(mailId:starredMailVM.StarredMailArray[currentIndex!.row].mailId)
    }
    @IBAction func unstarButton(_ sender: Any) {
        starredMailVM.UnStarMail(mailId: (starredMailVM.StarredMailArray[currentIndex!.row].mailId))
    }
    @IBAction func closeEditview(_ sender: Any) {
        editView.isHidden = true
        starredTable.reloadData()
    }
    
    @IBAction func composeMail(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: StringConstants.Mail(), bundle: nil)
        let pushVc = storyboard.instantiateViewController(withIdentifier: String(describing: ComposeVC.self))
        self.navigationController?.pushViewController(pushVc, animated: true)
    }
    func setVM() {
        starredMailVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Starred:
                if self?.starredMailVM.StarredMailArray.count == 0{
                    self?.starredTable.isHidden = true
                    self?.noMailLabel.isHidden = false
                }
                else{
                    self?.starredTable.reloadData()
                }
                break
            case .Delete:
                Helper.showAlert(message: StringConstants.SuccessfullyRemoved(), head: "")
                self?.starredMailVM.getStarredMail()
                self?.setUi()
            case .Unstar:
                Helper.showAlert(message: StringConstants.UnstarredSuccessfully(), head: StringConstants.Mail())
                self?.starredMailVM.getStarredMail()
                self?.setUi()
                break
            default:
                break
            }
        }).disposed(by: starredMailVM.disposeBag)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
