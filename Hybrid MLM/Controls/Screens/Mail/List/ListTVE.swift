//
//  ListTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/01/21.
//

import Foundation
import UIKit

extension ListVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: ListTableViewCell.self))) as! ListTableViewCell
        return cell
    }
}
