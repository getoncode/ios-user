//
//  ViewMailUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 30/11/20.
//

import Foundation
import UIKit

extension ViewMailVC
{
    func setUi() {
        deleteButton.layer.cornerRadius = 18.0
        replyButton.layer.cornerRadius = 17.0
        attachmentButton.layer.cornerRadius = 18.0
        Helper.roundBorder(view: replyView, radius: 20.0, borderWidth: 0.5, color: UIColor.lightGray.cgColor)
        imageContainer.layer.cornerRadius = 17.0
        outerView.layer.cornerRadius = 10.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 10.0)
    }
    
}
