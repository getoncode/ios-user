//
//  UploadsTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 30/11/20.
//

import UIKit

class UploadsTVCell: UITableViewCell {
    @IBOutlet weak var docImageContainer: UIView!
    @IBOutlet weak var uploadsButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        docImageContainer.layer.cornerRadius = 15.5
        uploadsButton.layer.cornerRadius = 18.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
