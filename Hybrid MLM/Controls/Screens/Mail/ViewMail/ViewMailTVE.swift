//
//  ViewMailTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/01/21.
//

import Foundation
import UIKit
extension ViewMailVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == messageTable{
            let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: messageTVCell.self))) as! messageTVCell
            if inboxDetails != nil{
            cell.contentLabel.text = inboxDetails?.content.htmlToAttributedString?.string.trimmingCharacters(in: .whitespacesAndNewlines)
            }else if sentItemsDetails != nil{
                cell.contentLabel.text = sentItemsDetails?.content.htmlToAttributedString?.string.trimmingCharacters(in: .whitespacesAndNewlines)
            }else{
                cell.contentLabel.text = starredDetails?.content.htmlToAttributedString?.string.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            return cell
        }
        else {
            let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: UploadsTVCell.self))) as! UploadsTVCell
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
