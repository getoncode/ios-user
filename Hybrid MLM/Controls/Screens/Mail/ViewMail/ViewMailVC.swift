//
//  ViewMailVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 30/11/20.
//

import UIKit

class ViewMailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var attachmentButton: UIButton!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var replyButton: UIButton!
    @IBOutlet weak var replyTF: UITextField!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var replyContainer: UIView!
    @IBOutlet weak var replyView: UIView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var messageTable: UITableView!
    @IBOutlet weak var msgTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var documentTable: UITableView!
    var inboxDetails : Inbox? = nil
    var sentItemsDetails : SentMail? = nil
    var starredDetails : StarredMail? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        setData()
    }
    
    func setData() {
        if inboxDetails != nil{
            subjectLabel.text = inboxDetails?.subject
            senderName.text = inboxDetails?.receiver_name
            date.text = Helper.dateToString(date: inboxDetails?.created_at ?? Date(), format: "dd MMM")
        }else if sentItemsDetails != nil{
            subjectLabel.text = sentItemsDetails?.subject
            senderName.text = sentItemsDetails?.sender_name
            date.text = Helper.dateToString(date: sentItemsDetails?.created_at ?? Date(), format: "dd MMM")
        }
        else{
            subjectLabel.text = starredDetails?.subject
            senderName.text = starredDetails?.receiver_name
            date.text = Helper.dateToString(date: starredDetails?.created_at ?? Date(), format: "dd MMM")
        }
        messageTable.reloadData()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    @IBAction func deleteAction(_ sender: Any) {
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
