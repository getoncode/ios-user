//
//  SideMenuMailTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 24/05/21.
//

import Foundation
import UIKit

extension SideMenuMail{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SideMenuMailTVCell.self), for: indexPath) as! SideMenuMailTVCell
        cell.menuLabel.text = data[indexPath.row].HeaderName
        cell.iconView.image = data[indexPath.row].image
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            self.dismiss(animated: true, completion: nil)
        }
        if indexPath.row == 1{
            performSegue(withIdentifier: String(describing: SideMenuMail.self)+String(describing: SentItemsVC.self), sender: nil)
        }
        if indexPath.row == 2{
            performSegue(withIdentifier: String(describing: SideMenuMail.self)+String(describing: DraftVC.self), sender: nil)
        }
        if indexPath.row == 3{
            performSegue(withIdentifier: String(describing: SideMenuMail.self)+String(describing: StarredMailVC.self), sender: nil)
        }
        if indexPath.row == 4{
            performSegue(withIdentifier: String(describing: SideMenuMail.self)+String(describing: TrashedMailVC .self), sender: nil)
        }
    }
}
