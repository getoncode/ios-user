//
//  SideMenuMail.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 24/05/21.
//

import UIKit
import SideMenu

class SideMenuMail: UIViewController,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var sideMenuTable: UITableView!
    var data = [DataModel(headername: StringConstants.Trash(),image: #imageLiteral(resourceName: "mail") ,subtype: [], inexpansion: false),DataModel(headername: StringConstants.SentItems(), image :#imageLiteral(resourceName: "sentItems") ,subtype: [], inexpansion: false),DataModel(headername: StringConstants.Draft(),image :#imageLiteral(resourceName: "draft") , subtype: [], inexpansion: false),DataModel(headername: StringConstants.StarredMail(), image :#imageLiteral(resourceName: "starredMail") ,subtype: [],inexpansion: false),DataModel(headername: StringConstants.Trash(),image :#imageLiteral(resourceName: "Trash") , subtype: [], inexpansion: false)]
    var selectedIndex : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImage.layer.cornerRadius = 40.0
    }

    override func viewWillAppear(_ animated: Bool) {
        nameLabel.text = Utility.UserData.username
        Helper.setImage(image:  APIKeys.baseURLFirst + Utility.UserData.profilePic, on: profileImage)
        emailLabel.text = Utility.UserData.email
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    //set logout
    @IBAction func logout(_ sender: Any) {
        Helper.logout()
    }
}

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */
