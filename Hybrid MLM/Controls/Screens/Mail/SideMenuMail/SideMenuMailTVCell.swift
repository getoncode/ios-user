//
//  SideMenuMailTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 24/05/21.
//

import UIKit

class SideMenuMailTVCell:  UITableViewCell {
    @IBOutlet weak var menuLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
