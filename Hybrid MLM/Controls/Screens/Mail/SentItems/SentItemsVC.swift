//
//  SentItemsVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 20/05/21.
//

import UIKit

class SentItemsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate  {
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var closeEditView: UIButton!
    @IBOutlet weak var sentItemsTable: UITableView!
    @IBOutlet weak var composeButton: UIButton!
    @IBOutlet weak var editView: UIView!
    var sentItemsVM = SentItemsVM()
    var currentIndex : IndexPath?
    var selectedIndex : [IndexPath] = []
    var deleteId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        sentItemsVM.getSentMail()
        setupLongPressGesture()
        setVM()
    }
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self
        self.sentItemsTable.addGestureRecognizer(longPressGesture)
    }
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .began {
            let touchPoint = gestureRecognizer.location(in: self.sentItemsTable)
            if let indexPath = sentItemsTable.indexPathForRow(at: touchPoint) {
                editView.isHidden = false
                let cell = sentItemsTable.cellForRow(at: indexPath) as? SentItemsTVCell
                currentIndex = indexPath
                if selectedIndex.contains(indexPath)
                {
                    if let removeIndex = selectedIndex.firstIndex(of: indexPath) {
                        selectedIndex.remove(at: removeIndex)
                    }
                }else{
                    selectedIndex.append(indexPath)
                }
                if sentItemsVM.SentArray[currentIndex!.row].is_starred == 1{
                    self.starButton.setImage(#imageLiteral(resourceName: "starActive"), for: .normal)
                }else{
                    self.starButton.setImage(#imageLiteral(resourceName: "starMail"), for: .normal)
                }
                cell?.checkBoxClick(UIControl.State.selected)
            }
        }
    }
    @IBAction func exportMail(_ sender: Any) {
    }
    @IBAction func deleteMail(_ sender: Any) {
        for item in selectedIndex{
            if deleteId != ""{
             deleteId = deleteId + "," + "\(sentItemsVM.SentArray[item.row].mailId)"
            }
            else{
                deleteId = "\(sentItemsVM.SentArray[item.row].mailId)"
            }
        }
        sentItemsVM.deleteMail(mailId: deleteId)
    }
    @IBAction func starMail(_ sender: Any) {
        if starButton.image(for: .normal) == #imageLiteral(resourceName: "starMail"){
            sentItemsVM.starMail(mailId: sentItemsVM.SentArray[currentIndex!.row].mailId)
        }
        else{
            sentItemsVM.UnStarMail(mailId: sentItemsVM.SentArray[currentIndex!.row].mailId)
        }
    }
    @IBAction func closeEdit(_ sender: Any) {
        editView.isHidden = true
        sentItemsTable.reloadData()
    }
    @IBAction func compose(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: StringConstants.Mail(), bundle: nil)
        let pushVc = storyboard.instantiateViewController(withIdentifier: String(describing: ComposeVC.self))
        self.navigationController?.pushViewController(pushVc, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    func setVM() {
        sentItemsVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Sent:
                if self?.sentItemsVM.SentArray.count == 0{
                    self?.sentItemsTable.isHidden = true
                    //self?.noMailLabel.isHidden = false
                }
                else{
                    self?.sentItemsTable.reloadData()
                }
                break
            case .Delete:
                Helper.showAlert(message: StringConstants.SuccessfullyRemoved(), head: "")
                self?.sentItemsVM.getSentMail()
                self?.setUi()
                self?.deleteId = ""
            case .Star:
                self?.starButton.setImage(#imageLiteral(resourceName: "starActive"), for: .normal)
                break
            case .Unstar:
                Helper.showAlert(message: StringConstants.UnstarredSuccessfully(), head: StringConstants.Mail())
                self?.sentItemsVM.getSentMail()
                self?.setUi()
                break
            default:
                break
            }
        }).disposed(by: sentItemsVM.disposeBag)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
