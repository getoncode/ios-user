//
//  SentItemsVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 21/05/21.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit

class SentItemsVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case Sent = 1
        case Delete = 2
        case Star = 3
        case Unstar = 4
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var SentArray : [SentMail] = []
   
    //sent items
    func getSentMail() {
        let params : [String:Any] = [
            "purpose" : "getSentMails"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.SentArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in dataIn{
                        self.SentArray.append(SentMail.init(data: item))
                    }
                }
                self.SentArray.sort(by: {$0.created_at.compare($1.created_at) == .orderedDescending})
                self.ResposeBack.onNext(typeHere.Sent)
            }
        }).disposed(by: disposeBag)
    }
    
    func deleteMail(mailId :String) {
        let params : [String:Any] = [
            "purpose" : "deleteMail",
            "params[mail_id]" : mailId
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.SentArray.removeAll()
            if data.0 {
                self.ResposeBack.onNext(typeHere.Delete)
            }
        }).disposed(by: disposeBag)
    }
    
    func starMail(mailId :Int) {
        let params : [String:Any] = [
            "purpose" : "starMail",
            "params[mail_id]" : mailId
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                self.ResposeBack.onNext(typeHere.Star)
            }
        }).disposed(by: disposeBag)
    }
    
    //unstar
    func UnStarMail(mailId : Int) {
        let params : [String:Any] = [
            "purpose" : "unstarMail",
            "params[mail_id]" : mailId
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                self.ResposeBack.onNext(typeHere.Unstar)
            }
        }).disposed(by: disposeBag)
    }
}


