//
//  SentItemsTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 20/05/21.
//

import UIKit

class SentItemsTVCell: UITableViewCell {
    @IBOutlet weak var checkBox: UIButton!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabe: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Helper.setButton(button: checkBox, view: checkBox, primaryColour: UIColor.white, seconderyColor: Colors.AppBaseColor, shadow: false, corner: 5, border: Colors.AppBaseColor, type: .system)
        checkBox.setImage(#imageLiteral(resourceName: "tick"), for: .selected)
        checkBox.imageView?.contentMode = .scaleAspectFit
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func checkBoxClick(_ sender: UIButton.State) {
        if checkBox.isSelected
        {
            checkBox.isSelected = false
            checkBox.backgroundColor = Colors.AppBaseColor
        }
        else
        {
            checkBox.isSelected = true
            checkBox.backgroundColor = UIColor.white
           
        }
    }
    
    func setData(_ data: SentMail) {
        senderName.text = data.sender_name
        subjectLabel.text = data.subject
        dateLabe.text = Helper.dateToString(date: data.created_at, format: "dd MMM")
        descriptionLabel.text = data.content.htmlToAttributedString?.string.trimmingCharacters(in: .whitespacesAndNewlines)
    }

}
