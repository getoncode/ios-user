//
//  SentItemsTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 20/05/21.
//

import Foundation
import UIKit

extension SentItemsVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        sentItemsVM.SentArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: SentItemsTVCell.self))) as! SentItemsTVCell
        tableView.rowHeight = 80
        cell.checkBox.isSelected = false
        cell.setData(sentItemsVM.SentArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard: UIStoryboard = UIStoryboard(name: StringConstants.Mail(), bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: ViewMailVC.self)) as! ViewMailVC
        vc.sentItemsDetails = sentItemsVM.SentArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
