//
//  SentItemsUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 20/05/21.
//

import Foundation
import UIKit

extension SentItemsVC
{
    func setUi() {
        editView.isHidden = true
        composeButton.layer.cornerRadius = 25.0
    }
}
