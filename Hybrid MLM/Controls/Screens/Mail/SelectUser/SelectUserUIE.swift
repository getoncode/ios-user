//
//  SelectUserUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 17/06/21.
//

import Foundation
import UIKit

extension SelectUserVC{
    func setUi() {
        //ui additionals here
    }
    
    //dismiss viewcontroller on tapping outside search view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.view {
//            let data = UserDefaults.standard.value(forKey: UserDefaultConstants.Recipients)
//            responseBack.onNext(data)
            self.dismiss(animated: true, completion: nil)
        }
    }
}
