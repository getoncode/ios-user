//
//  SelectUserVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 17/06/21.
//

import UIKit
import RxCocoa
import RxSwift

class SelectUserVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var userTable: UITableView!
    let selectUserVM = SelectUserVM()
    let responseBack = PublishSubject<Any>()
    let responseBack2 = PublishSubject<Any>()
    let disposebag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        selectUserVM.getUsers()
        setVM()
    }
    
    func setVM() {
        selectUserVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .User:
                self?.userTable.reloadData()
                break
            default:
                break
            }
        }).disposed(by: selectUserVM.disposeBag)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
