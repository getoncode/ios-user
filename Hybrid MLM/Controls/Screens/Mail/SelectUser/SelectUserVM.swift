//
//  SelectUserVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 17/06/21.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit

class SelectUserVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case User = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var UsersArray: [Users] = []
    
    
    //get users api call
    func getUsers() {
        let params : [String:Any] = [
            "purpose" : "getUserDetails"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.UsersArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in dataIn{
                        self.UsersArray.append(Users.init(data: item))
                    }
                }
                self.ResposeBack.onNext(typeHere.User)
            }
            
        }).disposed(by: disposeBag)
    }
}


