//
//  SelectUserTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 17/06/21.
//

import Foundation
import UIKit

extension SelectUserVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        selectUserVM.UsersArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: UserTVCell.self))) as! UserTVCell
        cell.setData(selectUserVM.UsersArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        responseBack.onNext(selectUserVM.UsersArray[indexPath.row].username)
        responseBack2.onNext(selectUserVM.UsersArray[indexPath.row].id)
        self.dismiss(animated: true, completion: nil)
    }
}
