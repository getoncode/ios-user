//
//  UserTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 17/06/21.
//

import UIKit

class UserTVCell: UITableViewCell {
    @IBOutlet weak var usernameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(_ data: Users) {
        usernameLabel.text = data.username
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
