//
//  DraftVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 21/05/21.
//

import UIKit

class DraftVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    @IBOutlet weak var noMailLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var composeButton: UIButton!
    @IBOutlet weak var draftTable: UITableView!
    @IBOutlet weak var starButton: UIButton!
    var draftVM = DraftVM()
    var currentIndex : IndexPath?
    var selectedIndex : [IndexPath] = []
    var deleteId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        draftVM.getDraftMail()
        setupLongPressGesture()
        setVM()
    }
    
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self
        self.draftTable.addGestureRecognizer(longPressGesture)
    }
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .began {
            let touchPoint = gestureRecognizer.location(in: self.draftTable)
            if let indexPath = draftTable.indexPathForRow(at: touchPoint) {
                editView.isHidden = false
                let cell = draftTable.cellForRow(at: indexPath) as? DraftTVCell
                currentIndex = indexPath
                if selectedIndex.contains(indexPath)
                {
                    if let removeIndex = selectedIndex.firstIndex(of: indexPath) {
                        selectedIndex.remove(at: removeIndex)
                    }
                }else{
                    selectedIndex.append(indexPath)
                }
//                if draftVM.DraftArray[currentIndex!.row].is_starred == 1{
//                    self.starButton.setImage(#imageLiteral(resourceName: "starActive"), for: .normal)
//                }else{
//                    self.starButton.setImage(#imageLiteral(resourceName: "starMail"), for: .normal)
//                }
                cell?.checkBoxClick(UIControl.State.selected)
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    @IBAction func exportMail(_ sender: Any) {
    }
    @IBAction func deleteMail(_ sender: Any) {
        for item in selectedIndex{
//            if deleteId != ""{
//             deleteId = deleteId + "," + "\(draftVM.DraftArray[item.row].mailId)"
//            }
//            else{
//                deleteId = "\(draftVM.DraftArray[item.row].mailId)"
//            }
        }
        draftVM.deleteMail(mailId: deleteId)
    }
    @IBAction func starMail(_ sender: Any) {
//        if starButton.image(for: .normal) == #imageLiteral(resourceName: "starMail"){
//            draftVM.starMail(mailId: draftVM.DraftArray[currentIndex!.row].mailId)
//        }
//        else{
//            draftVM.UnStarMail(mailId: draftVM.DraftArray[currentIndex!.row].mailId)
//        }
    }
    @IBAction func closeEditView(_ sender: Any) {
        editView.isHidden = true
        draftTable.reloadData()
    }
    @IBAction func compose(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: StringConstants.Mail(), bundle: nil)
        let pushVc = storyboard.instantiateViewController(withIdentifier: String(describing: ComposeVC.self))
        self.navigationController?.pushViewController(pushVc, animated: true)
    }
    func setVM() {
        draftVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Draft:
                if self?.draftVM.DraftArray.count == 0{
                    self?.draftTable.isHidden = true
                    self?.noMailLabel.isHidden = false
                }
                else{
                    self?.draftTable.reloadData()
                }
                break
            case .Delete:
                Helper.showAlert(message: StringConstants.SuccessfullyRemoved(), head: "")
                self?.draftVM.getDraftMail()
                self?.setUi()
                self?.deleteId = ""
            case .Star:
                self?.starButton.setImage(#imageLiteral(resourceName: "starActive"), for: .normal)
                break
            case .Unstar:
                Helper.showAlert(message: StringConstants.UnstarredSuccessfully(), head: StringConstants.Mail())
                self?.draftVM.getDraftMail()
                self?.setUi()
                break
            default:
                break
            }
        }).disposed(by: draftVM.disposeBag)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
