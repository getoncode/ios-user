//
//  DraftTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 21/05/21.
//

import Foundation
import UIKit

extension DraftVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        draftVM.DraftArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: DraftTVCell.self))) as! DraftTVCell
        tableView.rowHeight = 80
        cell.checkBox.isSelected = false
        cell.setData(draftVM.DraftArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard: UIStoryboard = UIStoryboard(name: StringConstants.Mail(), bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: ComposeVC.self)) as! ComposeVC
        vc.draftDetails = draftVM.DraftArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
