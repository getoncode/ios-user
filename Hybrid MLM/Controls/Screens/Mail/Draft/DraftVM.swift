//
//  DraftVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 21/05/21.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit

class DraftVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case Draft = 1
        case Delete = 2
        case Star = 3
        case Unstar = 4
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var DraftArray : [Draft] = []
   
    //draft
    func getDraftMail() {
        let params : [String:Any] = [
            "purpose" : "getDraftMails"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.DraftArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in dataIn{
                        self.DraftArray.append(Draft.init(data: item))
                    }
                }
                self.DraftArray.sort(by: {$0.created_at.compare($1.created_at) == .orderedDescending})
                self.ResposeBack.onNext(typeHere.Draft)
            }
        }).disposed(by: disposeBag)
    }
    
    func deleteMail(mailId :String) {
        let params : [String:Any] = [
            "purpose" : "deleteMail",
            "params[mail_id]" : mailId
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.DraftArray.removeAll()
            if data.0 {
                self.ResposeBack.onNext(typeHere.Delete)
            }
        }).disposed(by: disposeBag)
    }
    
    func starMail(mailId :Int) {
        let params : [String:Any] = [
            "purpose" : "starMail",
            "params[mail_id]" : mailId
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                self.ResposeBack.onNext(typeHere.Star)
            }
        }).disposed(by: disposeBag)
    }
    
    //unstar
    func UnStarMail(mailId : Int) {
        let params : [String:Any] = [
            "purpose" : "unstarMail",
            "params[mail_id]" : mailId
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                self.ResposeBack.onNext(typeHere.Unstar)
            }
        }).disposed(by: disposeBag)
    }

}


