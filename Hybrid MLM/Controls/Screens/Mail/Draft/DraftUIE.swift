//
//  DraftUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 21/05/21.
//

import Foundation
import UIKit

extension DraftVC
{
    func setUi() {
     // ui changes here
        self.draftTable.isHidden = false
        self.noMailLabel.isHidden = true
        editView.isHidden = true
        composeButton.layer.cornerRadius = 25.0
    }
}
