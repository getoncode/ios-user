//
//  TrashedMailVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 26/05/21.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit

class TrashedMailVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case Trashed = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var TrashedMailArray : [TrashedMail] = []
   
    //draft
    func getTrashedMail() {
        let params : [String:Any] = [
            "purpose" : "getTrashedMails"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.TrashedMailArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in dataIn{
                        self.TrashedMailArray.append(TrashedMail.init(data: item))
                    }
                }
                self.TrashedMailArray.sort(by: {$0.created_at.compare($1.created_at) == .orderedDescending})
                self.ResposeBack.onNext(typeHere.Trashed)
            }
        }).disposed(by: disposeBag)
    }
}


