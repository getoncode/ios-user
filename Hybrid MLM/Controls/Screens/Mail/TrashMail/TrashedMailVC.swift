//
//  TrashedMailVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 26/05/21.
//

import UIKit

class TrashedMailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var noMailLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var TrashTable: UITableView!
    var trashedMailVM = TrashedMailVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        trashedMailVM.getTrashedMail()
        setVM()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    func setVM() {
        trashedMailVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Trashed:
                if self?.trashedMailVM.TrashedMailArray.count == 0{
                    self?.TrashTable.isHidden = true
                    self?.noMailLabel.isHidden = false
                }
                else{
                    self?.TrashTable.reloadData()
                }
                break
            default:
                break
            }
        }).disposed(by: trashedMailVM.disposeBag)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
