//
//  TrashedMailTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 26/05/21.
//

import Foundation
import UIKit

extension TrashedMailVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        trashedMailVM.TrashedMailArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: TrashedMailTVCell.self))) as! TrashedMailTVCell
        tableView.rowHeight = 80
        cell.setData(trashedMailVM.TrashedMailArray[indexPath.row])
        return cell
    }
}
