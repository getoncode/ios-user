//
//  InboxVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 27/11/20.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit

class InboxVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case Inbox = 1
        case Delete = 2
        case Star = 3
        case Unstar = 4
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var InboxArray : [Inbox] = []
   
    //inbox
    func getInbox() {
        let params : [String:Any] = [
            "purpose" : "getMailInbox"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.InboxArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in dataIn{
                        self.InboxArray.append(Inbox.init(data: item))
                    }
                }
                self.InboxArray.sort(by: {$0.created_at.compare($1.created_at) == .orderedDescending})
                self.ResposeBack.onNext(typeHere.Inbox)
            }
        }).disposed(by: disposeBag)
    }
    //inbox
    func deleteMail(mailId :String) {
        let params : [String:Any] = [
            "purpose" : "deleteMail",
            "params[mail_id]" : mailId
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.InboxArray.removeAll()
            if data.0 {
                self.ResposeBack.onNext(typeHere.Delete)
            }
        }).disposed(by: disposeBag)
    }
    
    func starMail(mailId :Int) {
        let params : [String:Any] = [
            "purpose" : "starMail",
            "params[mail_id]" : mailId
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                self.ResposeBack.onNext(typeHere.Star)
            }
        }).disposed(by: disposeBag)
    }
    
    //unstar
    func UnStarMail(mailId : Int) {
        let params : [String:Any] = [
            "purpose" : "unstarMail",
            "params[mail_id]" : mailId
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                self.ResposeBack.onNext(typeHere.Unstar)
            }
        }).disposed(by: disposeBag)
    }
}


