//
//  InboxTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/01/21.
//

import Foundation
import UIKit

extension InboxVC{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return inboxVM.InboxArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section  == 0
        {
            let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: InboxTVCell.self))) as! InboxTVCell
            tableView.rowHeight = 80
            cell.checkBox.isSelected = false
            cell.setData(inboxVM.InboxArray[indexPath.row])
            return cell
        }
//        else if indexPath.section == 1
//        {
//            let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: ReadTVCell.self))) as! ReadTVCell
//            tableView.rowHeight = 80
//            if indexPath.row <= 3
//            {
//                cell.senderName.textColor = colors[indexPath.row]
//            }
//            return cell
//        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard: UIStoryboard = UIStoryboard(name: StringConstants.Mail(), bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: ViewMailVC.self)) as! ViewMailVC
        vc.inboxDetails = inboxVM.InboxArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
