//
//  InboxUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 27/11/20.
//

import Foundation
import UIKit

extension InboxVC
{
    func setUi() {
        Helper.setImage(image:  APIKeys.baseURLFirst + Utility.UserData.profilePic, on: profileImage)
        profileImage.layer.cornerRadius = 20.0
        imageContainer.layer.cornerRadius = 20.0
        composeButton.layer.cornerRadius = 25.0
        colors = [#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), #colorLiteral(red: 0.5818830132, green: 0.2156915367, blue: 1, alpha: 1), #colorLiteral(red: 0.3763252825, green: 0.9415396002, blue: 1, alpha: 1), #colorLiteral(red: 0.365560965, green: 0.3519866773, blue: 1, alpha: 1)]
        editView.isHidden = true
    }
}
