//
//  InboxVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 27/11/20.
//

import UIKit

class InboxVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var exportButton: UIButton!
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var composeButton: UIButton!
    @IBOutlet weak var inboxTable: UITableView!
    var colors : [UIColor] = []
    var inboxVM = InboxVM()
    var currentIndex : IndexPath?
    var selectedIndex : [IndexPath] = []
    var deleteId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        setupLongPressGesture()
        inboxVM.getInbox()
        setVM()
    }
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 0.5 
        longPressGesture.delegate = self
        self.inboxTable.addGestureRecognizer(longPressGesture)
    }
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .began {
            let touchPoint = gestureRecognizer.location(in: self.inboxTable)
            if let indexPath = inboxTable.indexPathForRow(at: touchPoint) {
                editView.isHidden = false 
                let cell = inboxTable.cellForRow(at: indexPath) as? InboxTVCell
                currentIndex = indexPath
                if selectedIndex.contains(indexPath)
                {
                    if let removeIndex = selectedIndex.firstIndex(of: indexPath) {
                        selectedIndex.remove(at: removeIndex)
                    }
                }else{
                    selectedIndex.append(indexPath)
                }
                if inboxVM.InboxArray[currentIndex!.row].is_starred == 1{
                    self.starButton.setImage(#imageLiteral(resourceName: "starActive"), for: .normal)
                }else{
                    self.starButton.setImage(#imageLiteral(resourceName: "starMail"), for: .normal)
                }
                cell?.checkBoxClick(UIControl.State.selected)
            }
        }
    }
    @IBAction func closeButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    @IBAction func deleteMail(_ sender: Any) {
        for item in selectedIndex{
            if deleteId != ""{
             deleteId = deleteId + "," + "\(inboxVM.InboxArray[item.row].mailId)"
            }
            else{
                deleteId = "\(inboxVM.InboxArray[item.row].mailId)"
            }
        }
        inboxVM.deleteMail(mailId: deleteId)
    }
    @IBAction func starMail(_ sender: Any) {
        if starButton.image(for: .normal) == #imageLiteral(resourceName: "starMail"){
            inboxVM.starMail(mailId: inboxVM.InboxArray[currentIndex!.row].mailId)
        }
        else{
            inboxVM.UnStarMail(mailId: inboxVM.InboxArray[currentIndex!.row].mailId)
        }
    }
    @IBAction func closeEditView(_ sender: Any) {
        editView.isHidden = true
        inboxTable.reloadData()
    }
    func setVM() {
        inboxVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Inbox:
                self?.inboxTable.reloadData()
                break
            case .Delete:
                Helper.showAlert(message: StringConstants.SuccessfullyRemoved(), head: "")
                self?.inboxVM.getInbox()
                self?.setUi()
                self?.deleteId = ""
                break
            case .Star:
                self?.starButton.setImage(#imageLiteral(resourceName: "starActive"), for: .normal)
                break
            case .Unstar:
                Helper.showAlert(message: StringConstants.UnstarredSuccessfully(), head: StringConstants.Mail())
                self?.inboxVM.getInbox()
                self?.setUi()
                break
            default:
                break
            }
        }).disposed(by: inboxVM.disposeBag)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
