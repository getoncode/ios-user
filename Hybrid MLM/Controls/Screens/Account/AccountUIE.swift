//
//  AccountUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 28/01/21.
//

import Foundation
import  UIKit

extension AccountInfoVC{
    func setUi() {
        self.navigationController?.navigationBar.isHidden = false
        userImageOuterView.layer.cornerRadius = 50.0
        userImageView.layer.cornerRadius = 50.0
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 15.0)
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
    }
    func setData(data: [Account], image: String) {
        Helper.setImage(image:  APIKeys.baseURLFirst + image , on: userImageView)
        memberIdTF.text = data[0].memberId
        userNameTF.text = data[0].username
        sponsorTF.text = data[0].sponsor
        placementTF.text = data[0].placement
    }
}
