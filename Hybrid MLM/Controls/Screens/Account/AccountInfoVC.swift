//
//  AccountInfoVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 28/01/21.
//

import UIKit

class AccountInfoVC: UIViewController {
    @IBOutlet weak var memberIdTF: UITextField!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var sponsorTF: UITextField!
    @IBOutlet weak var placementTF: UITextField!
    @IBOutlet weak var userImageOuterView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var outerView: UIView!
    var accountInfo : [Account]?
    var profileUrl : String?
    
    let profileVM = ProfileVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUi()
        self.setData(data: accountInfo ?? [], image: profileUrl ?? "")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    @IBAction func showNotofication(_ sender: Any) {
        let notify = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
