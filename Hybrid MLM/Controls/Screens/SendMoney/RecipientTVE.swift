//
//  RecipientTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 25/03/21.
//

import Foundation
import UIKit

extension SendMoneyVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let data = UserDefaults.standard.value(forKey: UserDefaultConstants.Recipients) as? [String]
        return data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = UserDefaults.standard.value(forKey: UserDefaultConstants.Recipients) as? [String]
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: RecipientTVCell.self))) as! RecipientTVCell
        tableView.rowHeight = 60
        cell.recipientName.text = data?[indexPath.row] ?? ""
        cell.amountLabel.text = "Pay $\(self.currentValue) to \(self.wallet)"
        return cell
    }
    
    //delete recipient
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let data = UserDefaults.standard.value(forKey: UserDefaultConstants.Recipients) as? [String]
            Utility.clearRecipientData(String(indexPath.row))
            if data?.count == 1{
                setUi()
            }
            else{
            setHeight()
            }
            recipientTableView.reloadData()
        }
    }
}
