   //
//  SendMoneyVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 19/03/21.
//

import UIKit

class SendMoneyVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextViewDelegate{
    @IBOutlet weak var addNewButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var addNewButton: UIButton!
    @IBOutlet weak var chooseWalletView: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var walletImageView: UIView!
    @IBOutlet weak var walletName: UILabel!
    @IBOutlet weak var chooseWalletButton: UIButton!
    @IBOutlet weak var recipientViewHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var maximumAmount: UILabel!
    @IBOutlet weak var recipientView: UIView!
    @IBOutlet weak var amountSlider: UISlider!
    @IBOutlet weak var amountTF: UITextField!
    @IBOutlet weak var recipientTableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addNoteView: UIView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var chooseRecipientButton: UIButton!
   
    @IBOutlet weak var addNoteTxtView: UITextView!
    let sendMoneyVM = SendMoneyVM()
    var currentValue = ""
    var wallet = ""
    var walletid = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        amountSlider.minimumValue = 0
        setUi()
        if Helper.callRefreshToken(){
            sendMoneyVM.getWallets()
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        amountSlider.isSelected = true
        amountSlider.layoutIfNeeded()
        setVM()
        amountTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
    }
    //setvm
    func setVM() {
        sendMoneyVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Wallet:
                self?.walletName.text = self?.sendMoneyVM.WalletArray[0].name
                self?.wallet = self?.sendMoneyVM.WalletArray[0].name ?? ""
                self?.walletid = (String(describing: (self?.sendMoneyVM.WalletArray[0].id)!))
                self?.maximumAmount.text = self?.sendMoneyVM.balance
                var str = self?.sendMoneyVM.balance ?? ""
                if str.contains("$"){
                    str.remove(at: str.startIndex)
                }
                self?.amountSlider.maximumValue = Float(str) ?? 0.00
                break
            default:
                break
            }
        }).disposed(by: sendMoneyVM.disposeBag)
    }
  
    //choose recipient
    @IBAction func chooseRecipient(_ sender: Any) {
        if amountTF.text?.count == 0 || amountTF.text == "$" || currentValue == "0.00"{
            Helper.showAlert(message: StringConstants.ChooseAmount(), head: StringConstants.Alert())
        }else{
            performSegue(withIdentifier: String(describing: SendMoneyVC.self)+String(describing: SearchRecipientVC.self), sender: nil)
        }
    }
    //choose amount
    @IBAction func sliderAction(_ sender: UISlider) {
        currentValue =  String(format:"%.2f",Double(sender.value))
        amountTF.text = "$\(currentValue)"
    }
    
    @objc private func textFieldDidChange(textField: UITextField) {
        if textField == amountTF
        {
            if textField.text?.count ?? 0 <= 1
                {
                //user enter values changes
//                    textField.text = ""
//                    amountSlider.value = 0
                }
            else{
                var str = textField.text!
                if str.contains("$"){
                    str.remove(at: str.startIndex)
                }
                if let intValue = Float(str){
                    amountSlider.setValue(intValue, animated: true)
                    textField.text = "$" + str
                }
            }
        }
    }
    
    //choose desired wallet
    @IBAction func chooseWallet(_ sender: Any) {
        performSegue(withIdentifier: String(describing: SendMoneyVC.self)+String(describing: ChooseWalletVC.self), sender: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        Utility.clearRecipientData(StringConstants.all())
    }
    @IBAction func sendMoneyClick(_ sender: Any) {
        if amountTF.text?.count == 0 || amountTF.text == "$" || currentValue == "0.00" {
            Helper.showAlert(message: StringConstants.ChooseAmount(), head: "")
        }else{
            if UserDefaults.standard.value(forKey: UserDefaultConstants.Recipients) != nil{
                performSegue(withIdentifier: String(describing: SendMoneyVC.self)+String(describing: TransactionPasswordVC.self), sender: nil)
            }else{
                Helper.showAlert(message: StringConstants.ChooseRecipient(), head: "")
            }
        }
    }
    @IBAction func showNotification(_ sender: Any) {
        let notify = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //refresh after choosing recipient from SearchRecipientVC
        if segue.identifier == String(describing: SendMoneyVC.self)+String(describing: SearchRecipientVC.self)
        {
            if let controller = segue.destination as? SearchRecipientVC {
                controller.responseBack.subscribe(onNext: { [weak self]data in
                    self?.chooseRecipientButton.isHidden = true
                    self?.recipientTableView.isHidden = false
                    self?.setHeight()
                    self?.recipientTableView.reloadData()
                }).disposed(by: controller.disposebag)
            }
        }
        else if segue.identifier == String(describing: SendMoneyVC.self)+String(describing: ChooseWalletVC.self){
            if let controller = segue.destination as? ChooseWalletVC{
                controller.dataArray = sendMoneyVM.WalletArray
                controller.responseBack.subscribe(onNext: { [weak self]data in
                    self?.walletName.text = data
                    self?.wallet = data
                }).disposed(by: controller.disposebag)
                controller.responseBack1.subscribe(onNext: { [weak self]data in
                    self?.walletid = data
                }).disposed(by: controller.disposebag)
            }
        }
        else if segue.identifier == String(describing: SendMoneyVC.self)+String(describing: TransactionPasswordVC.self){
            if let controller = segue.destination as? TransactionPasswordVC{
                controller.amount = "\(amountSlider.value)"
                controller.walletId = walletid
                controller.remarks = addNoteTxtView.text ?? ""
            }
        }
    }
    
    
}
