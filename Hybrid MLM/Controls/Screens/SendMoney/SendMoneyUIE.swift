//
//  SendMoneyUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 19/03/21.
//

import Foundation
import UIKit

extension SendMoneyVC{
    func setUi() {
        addNewButton.isHidden = true
        chooseRecipientButton.isHidden = false
        addNewButtonHeight.constant = 0
        recipientTableView.isHidden = true
        addNewButton.layer.cornerRadius = 4.0
        sendButton.layer.cornerRadius = 8.0
        walletImageView.layer.cornerRadius = 8.0
        recipientView.layer.cornerRadius = 8.0
        chooseWalletView.layer.cornerRadius = 8.0
        amountView.layer.cornerRadius = 8.0
        addNoteView.layer.cornerRadius = 8.0
        addNoteTxtView.text = StringConstants.AddNote()
        addNoteTxtView.textColor = .lightGray
        addNoteTxtView.delegate = self
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 10.0)
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
        Helper.roundBorder(view: recipientView, radius: 8.0, borderWidth: 0.5, color: #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1))
        Helper.roundBorder(view: chooseWalletView, radius: 8.0, borderWidth: 0.5, color: #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1))
        Helper.roundBorder(view: amountView, radius: 8.0, borderWidth: 0.5, color: #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1))
        Helper.roundBorder(view: addNoteView, radius: 8.0, borderWidth: 0.5, color: #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1))
    }
    //add and remove observers
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    func setHeight() {
        addNewButton.isHidden = false
        addNewButtonHeight.constant = 30
        let ary = UserDefaults.standard.value(forKey: UserDefaultConstants.Recipients) as? [String]
        if ary?.count ?? 0 >= 5{
            self.recipientViewHeightContraint.constant = 330
            self.tableViewHeightConstraint.constant = 300
        }
        else
        {
            self.recipientViewHeightContraint.constant = 30 + CGFloat(ary?.count ?? 0) * 60
            //self.outerViewHeight.constant = 624 +  CGFloat(ary.count * 50)
            self.tableViewHeightConstraint.constant = CGFloat(ary?.count ?? 0) * 60
        }
        
    }
    //       Add placeholder to textview
    func textViewDidBeginEditing(_ textView: UITextView) {
        if addNoteTxtView.textColor == .lightGray {
            addNoteTxtView.text = ""
            addNoteTxtView.textColor = .black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if addNoteTxtView.text == "" {
            addNoteTxtView.text = StringConstants.AddNote()
            addNoteTxtView.textColor = .lightGray
        }
    }
}
