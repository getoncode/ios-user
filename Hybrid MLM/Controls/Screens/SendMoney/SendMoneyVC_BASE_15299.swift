//
//  SendMoneyVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 19/03/21.
//

import UIKit

class SendMoneyVC: UIViewController{
    @IBOutlet weak var chooseWalletView: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var walletImageView: UIView!
    @IBOutlet weak var walletName: UILabel!
    @IBOutlet weak var chooseWalletButton: UIButton!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var maximumAmount: UILabel!
    @IBOutlet weak var recipientView: UIView!
    @IBOutlet weak var amountSlider: UISlider!
    @IBOutlet weak var amountTF: UITextField!
    @IBOutlet weak var recipientTableView: UITableView!
    @IBOutlet weak var addNoteView: UIView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var chooseRecipientButton: UIButton!
    @IBOutlet weak var addNoteTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        amountSlider.minimumValue = 0
        amountSlider.maximumValue = 2000
        setUi()
    }

    @IBAction func chooseRecipient(_ sender: Any) {
        performSegue(withIdentifier: String(describing: SendMoneyVC.self)+String(describing: SearchRecipientVC.self), sender: nil)
    }
    @IBAction func sliderAction(_ sender: UISlider) {
        let sliderValue = sender.value
        amountTF.text = "$\(String(format: "%.2f", sliderValue))"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
