//
//  SendMoneyVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 19/03/21.
//

import Foundation
import RxCocoa
import RxSwift

class SendMoneyVM{
    enum typeHere: Int{
        case Default = 0
        case Wallet = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var WalletArray: [Transfer] = []
    var balance = ""
    
    func getWallets() {
        let params : [String:Any] = [
            "purpose" : "eWalletTransfer"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    for item in [dataIn] {
                        let dataSort = item["wallets"] as! [[String: Any]]
                        self.balance = item["currentBalance"] as! String
                        self.WalletArray.append(Transfer.init(data: dataSort[0]))
                    }
                }
                self.ResposeBack.onNext(typeHere.Wallet)
            }
            
        }).disposed(by: disposeBag)
    }
}
