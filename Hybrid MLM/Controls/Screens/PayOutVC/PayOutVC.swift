//
//  PayOutVC.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 23/06/21.
//

import UIKit

class PayOutVC: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var payoutTitleView: UIView!
    @IBOutlet weak var payoutOuterView: UIView!
    @IBOutlet weak var chooseWalletLabel: UILabel!
    @IBOutlet weak var amountTxtField: UITextField!
    @IBOutlet weak var chooseWalletView: UIView!
    @IBOutlet weak var addNoteTxtView: UITextView!
    @IBOutlet weak var walletName: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var walletImage: UIImageView!
    @IBOutlet weak var addnoteView: UIView!
    @IBOutlet weak var amountSlider: UISlider!
    @IBOutlet weak var requestBtn: UIButton!
    @IBOutlet weak var MinmValueLabel: UILabel!
    @IBOutlet weak var MaxmValueLabel: UILabel!
    @IBOutlet weak var amountTxtOuterView: UIView!
    @IBOutlet weak var amountOuterView: UIView!
    @IBOutlet weak var BtnArrow: UIButton!
    @IBOutlet weak var walletSelectionButton: UIButton!
    let payOutVM = PayOutVM()
    var currentValue = ""
    var wallet = ""
    var walletId = 0
    override func viewDidLoad() {
        super.viewDidLoad()
      amountSlider.minimumValue = 0
      amountSlider.isSelected = true
        amountSlider.minimumValue = 0
        
        setUi()
        if Helper.callRefreshToken(){
            payOutVM.getWallets()
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        
        setVM()
        amountTxtField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
    }
    //setvm
    func setVM() {
        payOutVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Wallet:
                self?.walletName.text = self?.payOutVM.WalletArray[0].name
                self?.wallet = self?.payOutVM.WalletArray[0].name ?? ""
                self?.MaxmValueLabel.text = self?.payOutVM.balance
                self?.walletId = self?.payOutVM.WalletArray[0].id ?? 0
                var str = self?.payOutVM.balance ?? ""
                if str.contains("$"){
                    str.remove(at: str.startIndex)
                }
                self?.amountSlider.maximumValue = Float(str) ?? 0.00
                break
            default:
                break
            }
        }).disposed(by: payOutVM.disposeBag)
    }
    
  
    @IBAction func ChoosePayoutBtn(_ sender: UIButton) {
        performSegue(withIdentifier: String(describing: PayOutVC.self)+String(describing: ChoosePayoutVC.self), sender: nil)
    }
    @IBAction func amountSliderAction(_ sender: UISlider) {
        currentValue =  String(format:"%.2f",Double(sender.value))
        amountTxtField.text = "$\(currentValue)"
    }
    
    @IBAction func btnRequestClick(_ sender: UIButton) {
        if amountTxtField.text?.count == 0 || amountTxtField.text == "$" || currentValue == "0.00"{
            Helper.showAlert(message: "Choose Amount", head: "Alert")
        }else{
            performSegue(withIdentifier: String(describing: PayOutVC.self)+String(describing: PayOutConfirmationVC.self), sender: nil)
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        Utility.clearRecipientData("all")
    }
    @IBAction func showNotification(_ sender: Any) {
        let notify = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: PayOutVC.self)+String(describing: ChoosePayoutVC.self){
            if let controller = segue.destination as? ChoosePayoutVC{
                controller.dataArray = payOutVM.WalletArray
                controller.responseBack.subscribe(onNext: { [weak self]data in
                    self?.walletName.text = data
                    self?.wallet = data
                }).disposed(by: controller.disposebag)
                controller.responseBack1.subscribe(onNext: { [weak self]data in
                    self?.walletId = data
                }).disposed(by: controller.disposebag)
            }
        }else{
            if segue.identifier == String(describing:PayOutVC.self) + String(describing:PayOutConfirmationVC.self){
                if let vc = segue.destination as? PayOutConfirmationVC {
                    vc.amountDatas = amountTxtField.text
                    vc.notesData = addNoteTxtView.text
                    vc.walletDetails = walletName.text
                    vc.walletId = walletId
                    
                }
            }
        }
    }
   
    
    //amount display on textfield corresponding slider moves
    @objc private func textFieldDidChange(textField: UITextField) {
        if textField == amountTxtField
        {
            if textField.text?.count ?? 0 <= 1
            {
                // textField.text = ""
                // amountSlider.value = 0
            }
            else{
                var str = textField.text!
                if str.contains("$"){
                    str.remove(at: str.startIndex)
                }
                if let intValue = Float(str){
                    amountSlider.setValue(intValue, animated: true)
                    textField.text = "$" + str
                }
            }
        }
    }
    //    override func viewWillDisappear(_ animated: Bool) {
    //        Utility.clearRecipientData("all")
    //    }
    
}
