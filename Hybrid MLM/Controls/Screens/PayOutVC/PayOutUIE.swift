//
//  PayOutUIE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 23/06/21.
//

import Foundation
import UIKit
extension PayOutVC{
    func setUi() {
        addNoteTxtView.text = "Add Note"
        addNoteTxtView.textColor = .lightGray
        addNoteTxtView.delegate = self
        amountSlider.sizeToFit()
        addNoteTxtView.leftSpace()
        requestBtn.layer.cornerRadius = 8.0
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
        Helper.roundBorder(view: amountTxtOuterView, radius: 8.0, borderWidth: 1.0, color: #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1))
        Helper.roundBorder(view: chooseWalletView, radius: 8.0, borderWidth: 1.0, color: #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1))
        Helper.roundBorder(view: addNoteTxtView, radius: 8.0, borderWidth: 1.0, color: #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1))
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
        amountSlider.isContinuous = true

    }
  
    //  Add placeholder to textview
    func textViewDidBeginEditing(_ textView: UITextView) {
        if addNoteTxtView.textColor == .lightGray {
            addNoteTxtView.text = ""
            addNoteTxtView.textColor = .black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if addNoteTxtView.text == "" {
            addNoteTxtView.text = "Add Note"
            addNoteTxtView.textColor = .lightGray
        }
    }
}


