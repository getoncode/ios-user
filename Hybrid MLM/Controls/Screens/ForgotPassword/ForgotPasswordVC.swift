//
//  ForgotPasswordVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 25/11/20.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    @IBOutlet weak var sendPasswordBUtton: UIButton!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var outerView: UIView!
    let forgotPasswordVM = ForgotPasswordVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        setVM()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    @IBAction func sendPasswordAction(_ sender: Any) {
        if Helper.isValidEmail(text: emailTF.text ?? ""){
            forgotPasswordVM.getResetPasswordMail(email: emailTF.text!)
        }else{
            Helper.showAlert(message: StringConstants.EnteraValidEmail(), head: StringConstants.Sorry())
        }
    }
    
    func setVM() {
        forgotPasswordVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Mail:
                self?.performSegue(withIdentifier: String(describing: ForgotPasswordVC.self)+String(describing: OTPViewController.self), sender: nil)
                break
            default:
                break
            }
        }).disposed(by: forgotPasswordVM.disposeBag)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
