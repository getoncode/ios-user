//
//  ForgotPasswordUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 25/11/20.
//

import Foundation
import UIKit

extension ForgotPasswordVC
{
    func setUi(){
        Helper.roundBorderButton(view: sendPasswordBUtton, radius: 5.0, borderWidth: 0, color: UIColor.clear.cgColor)
        self.navigationController?.navigationBar.isHidden = false
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color:Colors.ShadowColor, radius: 20.0)
    }
}
