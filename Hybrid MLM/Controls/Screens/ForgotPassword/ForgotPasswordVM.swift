//
//  ForgotPasswordVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 25/11/20.
//

import Foundation
import RxCocoa
import RxSwift

class ForgotPasswordVM: NSObject {
    enum typeHere:Int {
        case Default = 0
        case Mail = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    
    func getResetPasswordMail(email: String) {
        let params : [String:Any] = [
            "purpose" : "forgotPasswordMail",
            "params[email]" : email
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                // response here
            }
            self.ResposeBack.onNext(typeHere.Mail)
        }).disposed(by: disposeBag)
    }
    
}
