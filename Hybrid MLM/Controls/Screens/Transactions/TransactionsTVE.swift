//
//  TransactionsTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 03/02/21.
//

import Foundation
import UIKit

extension TransactionsVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionsVM.TransactionArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: TransactionsTVCell.self))) as! TransactionsTVCell
        cell.setData(transactionsVM.TransactionArray[indexPath.row])
        cell.selectionStyle = .blue
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: String(describing: TransactionsVC.self) + String(describing: TransactionDetailVC.self), sender: nil)
      
    }
}
