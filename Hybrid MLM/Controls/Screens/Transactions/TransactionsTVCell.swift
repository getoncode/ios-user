//
//  TransactionsTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 03/02/21.
//

import UIKit

class TransactionsTVCell: UITableViewCell {
    @IBOutlet weak var txnLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    var data : [String:Any]?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(_ data: Transaction) {
        txnLabel.text = "#" + String(describing: data.id)
        if data.amount.contains("+"){
            amountLabel.textColor = UIColor.green
        }
        else{
            amountLabel.textColor = UIColor.red
        }
        amountLabel.text = data.amount
        //set date
        dateLabel.text = Helper.dateToString(date: data.date, format: "dd MMM yyyy")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
