//
//  TransactionDetailVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 04/02/21.
//

import UIKit

class TransactionDetailVC: UIViewController {
   
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var operationLabel: UILabel!
    @IBOutlet weak var chargesLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tnxLabel: UILabel!
    @IBOutlet weak var payeeLabel: UILabel!
    @IBOutlet weak var payerLabel: UILabel!
    @IBOutlet weak var operationMetaLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var actualAmount: UILabel!
    
    var transactionDetails : Transaction?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        setData(data: transactionDetails!)
    }
    func setData(data: Transaction) {
        payerLabel.text = data.payer
        payeeLabel.text = data.payee
        dateLabel.text =  Helper.dateToString(date: data.date, format: "dd MMM yyyy")
        totalLabel.text = data.amount
        tnxLabel.text = StringConstants.TXN() + String(describing: data.id)
        operationLabel.text = data.operation
        operationMetaLabel.text = data.operationMeta
        if data.charges == ""
        {
            chargesLabel.text = StringConstants.NoCharges()
        }
        actualAmount.text = data.actualAamount
    }
    @IBAction func closeButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
