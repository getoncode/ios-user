//
//  TransactionsVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 03/02/21.
//

import UIKit

class TransactionsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var TransactionTV: UITableView!
    
    var transactionsVM = TransactiontVM()
    var selectedIndex : Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Helper.callRefreshToken(){
            transactionsVM.getEwalletTransactions()
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        setVM()
    }
    @IBAction func showNotification(_ sender: Any) {
        let notify = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    
    func setVM() {
        transactionsVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Transactions:
                self?.TransactionTV.reloadData()
                break
            default:
                break
            }
        }).disposed(by: transactionsVM.disposeBag)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: TransactionsVC.self) + String(describing: TransactionDetailVC.self){
            if let controller = segue.destination as? TransactionDetailVC {
                controller.transactionDetails = transactionsVM.TransactionArray[selectedIndex ?? 0]
            }
        }
    }
    
    
}
