//
//  TransactionDetailUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 04/02/21.
//

import Foundation
import UIKit
extension TransactionDetailVC{
    func setUi() {
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 10.0)
        Helper.gradientLight(view: topView)
        topView.layer.cornerRadius = 8.0
        bottomView.layer.cornerRadius = 8.0
    }
}
