//
//  TransactilonVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 03/02/21.
//

import Foundation
import RxCocoa
import RxSwift

class TransactiontVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case Transactions = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var TransactionArray: [Transaction] = []
    
    
    func getEwalletTransactions() {
        let params : [String:Any] = [
            "purpose" : "getEwalletTransactions"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.TransactionArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in [dataIn] {
                        for itm in item{
                        self.TransactionArray.append(Transaction.init(data: itm))
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.Transactions)
            }
            
        }).disposed(by: disposeBag)
    }
}


