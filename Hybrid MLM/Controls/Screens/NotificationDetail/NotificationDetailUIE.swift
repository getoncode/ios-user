//
//  NotificationDetailUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 05/04/21.
//

import Foundation
import UIKit

extension NotificationDetailVC{
    func setUi(){
        closeButton.layer.cornerRadius = 17.5
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 15.0)
    }
}
