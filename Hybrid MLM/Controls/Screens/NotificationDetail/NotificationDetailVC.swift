//
//  NotificationDetailVC.swift
//  Hybrid MLM
//
//  Created by Acemero on 05/04/21.
//

import UIKit

class NotificationDetailVC: UIViewController {
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var notificationDetail: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    var data : Notification?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        setData(data: data!)
    }
    
    func setData(data: Notification) {
        headLabel.text = data.dataAry["subject"]  as? String
        notificationDetail.text = data.dataAry["body"] as? String
        dateLabel.text = Helper.dateToString(date: data.created_at, format: "dd MMM yyyy")
    }
    
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //dismiss viewcontroller on tapping outside search view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.view {
            self.dismiss(animated: true, completion: nil)
        }
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
