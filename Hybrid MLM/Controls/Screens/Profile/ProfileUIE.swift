//
//  ProfileUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/11/20.
//

import Foundation
import UIKit

extension ProfileVC
{
    func setUi()
    {
        if Security == true{
            securityView.isHidden = false
        }
        else
        {
            securityView.isHidden = true
        }        
        changePasswordImageView.layer.cornerRadius = 15.0
        twoFactorAuthImageView.layer.cornerRadius = 15.0
        securityPINimgView.layer.cornerRadius = 15.0
        localizationImgView.layer.cornerRadius = 15.0
        currencyImageView.layer.cornerRadius = 15.0
        languageImgView.layer.cornerRadius = 15.0
        profileView.layer.cornerRadius = 55.0
        profileImageView.layer.cornerRadius = 55.0
        localizationSeperator.isHidden = true
        
        stckView.layer.cornerRadius = 8.0
        tableViewContainer.layer.cornerRadius = 8.0
        MenuTableView.layer.cornerRadius = 8.0
        localizationView.layer.cornerRadius = 8.0
        localizationSubView.layer.cornerRadius = 8.0
        Helper.setShadow(view: tableViewContainer, color: Colors.ShadowColor, radius: 15.0)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    func setLocalizationView() {
        if localizationSubView.isHidden
        {
            localizationSubView.isHidden = false
            localizationSeperator.isHidden = false
            localizationRightArrow.image = #imageLiteral(resourceName: "downArrow")
        }
        else
        {
            localizationSubView.isHidden = true
            localizationSeperator.isHidden = true
            localizationRightArrow.image = #imageLiteral(resourceName: "rightArrow")
        }
    }
}
