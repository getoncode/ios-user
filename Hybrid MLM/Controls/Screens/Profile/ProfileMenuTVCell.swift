//
//  ProfileMenuTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/11/20.
//

import UIKit

class ProfileMenuTVCell: UITableViewCell {
    @IBOutlet weak var menuItemName: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var rightArrowImgView: UIImageView!
    @IBOutlet weak var seperator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageIcon.layer.cornerRadius = 15.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
