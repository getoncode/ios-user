//
//  ProfileMenuTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/01/21.
//

import Foundation
import UIKit
extension ProfileVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: ProfileMenuTVCell.self))) as! ProfileMenuTVCell
        tableView.rowHeight = 60.0
        if indexPath.row == 3
        {
        if securityView.isHidden == true
        {
            cell.rightArrowImgView.image = #imageLiteral(resourceName: "rightArrow")
        }
        else
        {
            cell.rightArrowImgView.image = #imageLiteral(resourceName: "downArrow")
        }
        }
        cell.imageIcon.backgroundColor = colorsARRAY[indexPath.row]
        cell.menuItemName.text = titleArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            performSegue(withIdentifier: String(describing: ProfileVC.self)+String(describing: AccountInfoVC.self), sender: nil)
        }
        if indexPath.row == 1{
            performSegue(withIdentifier: String(describing: ProfileVC.self)+String(describing: EditProfileVC.self), sender: nil)
        }
        if indexPath.row == 2{
            performSegue(withIdentifier: String(describing: ProfileVC.self) + String(describing: SocialVC.self), sender: nil)
        }
        if indexPath.row == 3
        {
            if securityView.isHidden == true
            {
                Security = true
                securityView.isHidden = false
                tableView.reloadData()
            }
            else
            {
                Security = false
                securityView.isHidden = true
                tableView.reloadData()
            }
           
        }
        if indexPath.row == 4
        {
            performSegue(withIdentifier: String(describing: ProfileVC.self)+String(describing: LocalizationVC.self), sender: nil)
        }
    }
}
