//
//  ProfileVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/11/20.
//

import UIKit

class ProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var localizationRightArrow: UIImageView!
    @IBOutlet weak var localizationImgView: UIImageView!
    @IBOutlet weak var localizationView: UIView!
    @IBOutlet weak var stckView: UIStackView!
    @IBOutlet weak var tableViewContainer: UIView!
    @IBOutlet weak var securityPINimgView: UIImageView!
    @IBOutlet weak var twoFactorAuthImageView: UIImageView!
    @IBOutlet weak var changePasswordImageView: UIImageView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var MenuTableView: UITableView!
    @IBOutlet weak var securityView: UIView!
    @IBOutlet weak var localizationButton: UIButton!
    @IBOutlet weak var localizationSubView: UIView!
    @IBOutlet weak var localizationSeperator: UIView!
    @IBOutlet weak var currencyImageView: UIImageView!
    @IBOutlet weak var languageImgView: UIImageView!
    var Security : Bool?
    
    var colorsARRAY = [ #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 0.5), #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 0.5), #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 0.5), #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 0.5) ]
    var titleArray  = [StringConstants.AccountInfo(),StringConstants.Personal(),StringConstants.Social(),StringConstants.Security()]
    let profileVM = ProfileVM()
    let loginVM = LoginVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        if Helper.callRefreshToken(){
            profileVM.getProfileDetails()
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        securityView.isHidden = true
        localizationSubView.isHidden = true
        setVM()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: true)
    }
    @IBAction func changePasswordClick(_ sender: Any) {
        performSegue(withIdentifier: String(describing: ProfileVC.self)+String(describing: ChangePasswordVC.self), sender: nil)
    }
    @IBAction func twoFactorAuthenticationClick(_ sender: Any) {
        performSegue(withIdentifier: String(describing: ProfileVC.self)+String(describing: TwoFactorAuthenticationVC.self), sender: nil)
    }
    @IBAction func securityPINClick(_ sender: Any) {
        performSegue(withIdentifier: String(describing: ProfileVC.self)+String(describing: SecurityPinVC.self), sender: nil)
    }
    
    @IBAction func expandLocalizationView(_ sender: Any) {
        setLocalizationView()
    }
    @IBAction func pushToCurrencyScreen(_ sender: Any) {
        let nextViewController = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: String(describing: LocalizationVC.self)) as! LocalizationVC
        nextViewController.lType = StringConstants.Currency()
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func pushToLangugaeScreen(_ sender: Any) {
        let nextViewController = UIStoryboard(name: StringConstants.Main(), bundle:nil).instantiateViewController(withIdentifier: String(describing: LocalizationVC.self)) as! LocalizationVC
        nextViewController.lType = StringConstants.Language()
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func showNotification(_ sender: Any) {
        let notify = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    
    func setVM() {
        profileVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Profile:
                self?.nameLabel.text = self?.profileVM.PersonalArray[0].username
                self?.userNameLabel.text = self?.profileVM.PersonalArray[0].email
                Helper.setImage(image: APIKeys.baseURLFirst + (self?.profileVM.PersonalArray[0].profilePic ?? "")!, on: (self?.profileImageView)!)
                break
            default:
                break
            }
        }).disposed(by: profileVM.disposeBag)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: ProfileVC.self) + String(describing: SocialVC.self) {
            if let controller = segue.destination as? SocialVC {
                controller.socialData = self.profileVM.SocialArray
                controller.profileUrl = self.profileVM.PersonalArray[0].profilePic
                controller.responseBack.subscribe(onNext: { [weak self]data in
                    //response back
                }).disposed(by: controller.disposebag)
                
            }
        }
        if segue.identifier == String(describing: ProfileVC.self) + String(describing: AccountInfoVC.self){
            if let controller = segue.destination as? AccountInfoVC{
                controller.accountInfo = self.profileVM.AccountArray
                controller.profileUrl = self.profileVM.PersonalArray[0].profilePic
            }
        }
    }
    

}
