//
//  ProfileVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/11/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire

class ProfileVM : NSObject{
    enum typeHere :Int {
        case Default = 0
        case Profile = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var PersonalArray: [Personal] = []
    var AccountArray : [Account] = []
    var SocialArray : [Social] = []
    var editProfileVm = EditProfileVM()
    
    
    
    func getProfileDetails() {
        let params : [String:Any] = [
            "purpose" : "getProfile",
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.PersonalArray.removeAll()
            self.AccountArray.removeAll()
            self.SocialArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    if let array = dataIn as? [String:Any] {
                        for item in [array] {
                            self.PersonalArray.append(Personal.init(data: item["personal"] as? [String : Any] ?? [:]))
                            self.editProfileVm.countryId = self.PersonalArray[0].country_id
                            self.editProfileVm.stateId = self.PersonalArray[0].state_id
                            self.AccountArray.append(Account.init(data: item["account"] as? [String : Any] ?? [:]))
                            self.SocialArray.append(Social.init(data: item["social"] as? [String : Any] ?? [:]))
                            Utility.saveuserData(value: item["personal"] as? [String : Any] ?? [:])
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.Profile)
            }
        }).disposed(by: disposeBag)
    }
    
    
}



