//
//  ReportFilterVC.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 11/08/21.
//

import UIKit
import RxCocoa
import RxSwift
import DropDown
import RangeSeekSlider
class ReportFilterVC: UIViewController,UITextFieldDelegate,RangeSeekSliderDelegate {
    
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var rangeSliderCurrency: RangeSeekSlider!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var filterName: UILabel!
    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var amountBtn: UIButton!
    @IBOutlet weak var btnDescending: UIButton!
    @IBOutlet weak var btnAscending: UIButton!
    @IBOutlet weak var lblOrderBy: UILabel!
    @IBOutlet weak var lblSortBy: UILabel!
    @IBOutlet weak var toDate: UITextField!
    @IBOutlet weak var fromDate: UITextField!
    @IBOutlet weak var chooseCommissionView: UIView!
    @IBOutlet weak var chooseWalletView: UIView!
    @IBOutlet weak var chooseWallet: UIButton!
    @IBOutlet weak var lblWallet: UILabel!
    @IBOutlet weak var chooseCommssion:UIButton!
    @IBOutlet weak var lblCommission: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var fromDateView: UIView!
    @IBOutlet weak var ToDateView: UIView!
    var fromdatePicker = UIDatePicker()
    var toDatePicker = UIDatePicker()
    let commission = DropDown()
    let wallet = DropDown()
    var dataArray:[Filter] = []
    var datePick = UIDatePicker()
    let reportFilterVM = ReportFilterVM()
    let earningreportVM = EarningReportVM()
    let responseBack = PublishSubject<Any>()
    let responseBack2 = PublishSubject<Any>()
    let disposebag = DisposeBag()
    var commissionAry:[String] = []
    var index:Int = 0
    var commissionId:Int = 0
    var walletId:Int = 0
    var amount:Float = 0.0
    var FromDate = Date()
    var ToDate = Date()
    var filterArray:[String:Any] = [:]
    var sortingOrder = ""
    var sortArry = ""
    var sortBy :[String:Any] = [:]
    var value:CGFloat = 0.00
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpFromDatePicker()
        setUpToDatePicker()
        setup()
        if Helper.callRefreshToken(){
            reportFilterVM.getCommissions()
            reportFilterVM.getWallets()
            reportFilterVM.getTotalAmount()
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        setUi()
        setVM()
    }
    // rangslider setup
    private func setup() {
        rangeSliderCurrency.delegate = self
//        rangeSliderCurrency.numberFormatter.positivePrefix = "$"
        rangeSliderCurrency.numberFormatter.numberStyle = .currency
        rangeSliderCurrency.numberFormatter.locale = Locale(identifier: "en_US")
        rangeSliderCurrency.numberFormatter.maximumFractionDigits = 2
        rangeSliderCurrency.minValue = 0.0
        rangeSliderCurrency.selectedMinValue = 0.0
        rangeSliderCurrency.minLabelFont = UIFont.systemFont(ofSize: 14)
        rangeSliderCurrency.maxLabelFont = UIFont.systemFont(ofSize: 14)
    }
    //setVM
    func setVM(){
        reportFilterVM.ResposeBack.subscribe(onNext: { [weak self]data in
        switch data {
            case .TotalAmount:
                self?.rangeSliderCurrency.maxValue = CGFloat(self?.reportFilterVM.totalAmount ?? 0.00)
                self?.value = self?.rangeSliderCurrency.maxValue ?? 0.00
                NumberFormatter.localizedString(from: NSNumber(nonretainedObject: self?.value), number: .currency)
                break
            default:

                break
            }
        }).disposed(by: reportFilterVM.disposeBag)
    }
    
    @IBAction func closeBtn(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    //ChooseCommision
    @IBAction func ChooseCommision(_ sender: UIButton){
        commission.dataSource = reportFilterVM.commissionAry
        commission.anchorView = sender
        commission.bottomOffset = CGPoint(x: 0, y: sender.frame.size.height)
        commission.show()
        self.commissionId = self.reportFilterVM.commissionArray[index].moduleId
        commission.selectionAction = { [weak self] (index:Int , item: String) in
            switch self?.commission.indexForSelectedRow {
            case 0:
                self?.chooseCommssion.setTitle(self?.reportFilterVM.commissionAry[0], for: .normal)
                self?.commissionId = 0
            case 1:
                self?.chooseCommssion.setTitle(self?.reportFilterVM.commissionAry[1], for: .normal)
            case 2:
                self?.chooseCommssion.setTitle(self?.reportFilterVM.commissionAry[2], for: .normal)
            default:
                break
            }
     }
      commission.cancelAction = { [unowned self] in
      // You could for example deselect the selected item
      commission.deselectRow(at: commission.indexForSelectedRow)
            self.chooseCommssion.setTitle("  -- Choose Commission --  ", for: .normal)
        }
    }
    // walletSelection
    @IBAction func walletSelection(_ sender: UIButton) {
        wallet.dataSource = reportFilterVM.walletAry
        wallet.anchorView = sender
        wallet.bottomOffset = CGPoint(x: 0, y: sender.frame.size.height)
        wallet.show()
        wallet.selectionAction = { [weak self] (index:Int , item: String) in
        self?.walletId = self?.reportFilterVM.WalletArray[index].moduleId ?? 0
        self?.chooseWallet.setTitle(self?.reportFilterVM.WalletArray[index].name, for: .normal)
     }
    }
    //click Done Btn
    @IBAction func doneBtnAction(_ sender: UIButton) {
        let params : [String:Any] = [
            "purpose" : "earningReportData",
            "params[filters][commission]": commissionId,
            "params[filters][wallet]"    : walletId,
            "params[filters][fromAmount]": rangeSliderCurrency.selectedMinValue,
            "params[filters][toAmount]"  : rangeSliderCurrency.selectedMaxValue,
            "params[filters][fromDate]"  : FromDate,
            "params[filters][toDate]"    : ToDate
        ]
        //for sorting
        let sort :[String:Any] = [
            "purpose" : "earningReportData",
            "params[orderBy]": sortingOrder,
            "params[sortBy]" : sortArry
        ]
        filterArray = params
        sortBy = sort
        responseBack.onNext(filterArray)
        responseBack2.onNext(sortBy)//responseback object created for sorting ascending or desending
        dismiss(animated: true, completion: nil)
    }
   //Sorting
    @IBAction func sortingBtn(_ sender: UIButton) {
        let sortBtnTags = [10, 20]
        let currentButtonTag = sender.tag
        sortBtnTags.filter { $0 != currentButtonTag }.forEach {tag in
            if let button = self.view.viewWithTag(tag) as? UIButton {
                button.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                button.layer.borderWidth = 0.5
                sender.setTitleColor(UIColor.black, for: .normal)
                button.isSelected = false
            }
        }
        sender.layer.borderColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        sender.layer.borderWidth = 0.5
        sender.setTitleColor(UIColor.black, for: .selected)
        sender.isSelected = true
        switch sender.tag {
        case 10:
            if btnAscending.isSelected == true{ //ascending button
                sortArry = "ASC"
            }
        case 20:
            if btnDescending.isSelected == true{
                sortArry = "DESC"
            }
            default:
               break
        }
    }
 //OrderBy
    @IBAction func orderByBtnAction(_ sender: UIButton) {
        let allButtonTags = [1, 2, 3 , 4]
        let currentButtonTag = sender.tag
        allButtonTags.filter { $0 != currentButtonTag }.forEach { tag in
            if let button = self.view.viewWithTag(tag) as? UIButton {
                button.titleColor(for: .normal)
                button.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                button.layer.borderWidth = 0.5
                button.setTitleColor(UIColor.black, for: .normal)
                button.isSelected = false
            }
        }
        sender.layer.borderColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        sender.layer.borderWidth = 0.5
        sender.setTitleColor(UIColor.black, for: .normal)
        sender.isSelected = true
        switch sender.tag {
        case 1:
            if amountBtn.isSelected == true{
                sortingOrder = "Amount"
            }
        case 2:
            if dateBtn.isSelected == true{
                sortingOrder = "date"
            }
        default:
            break
        }
    }
}
