//
//  ReportFilterVM.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 11/08/21.
//

import Foundation
import RxCocoa
import RxSwift

class ReportFilterVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case Filter = 1
        case Commission = 2
        case Wallets = 3
        case TotalAmount = 4
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var filterArray : [Filter] = []
    var commissionArray:[Commission] = []
    var WalletArray:[Wallets] = []
    var totalAmount:Double = 0.00
    var commissionAry : [String] = []
    var walletAry : [String] = []
    var filtrArray:[Any] = []
    var none = "None"
    func getCommissions(){
        let params : [String:Any] = ["purpose" : "getCommissions"]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: "accessToken") as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in dataIn{
                        self.commissionArray.append(Commission.init(data: item))
                    }
                }
              self.commissionAry.insert(self.none, at: 0)
                for item in self.commissionArray{
                    self.commissionAry.append(item.name)
                }
                self.ResposeBack.onNext(typeHere.Commission)
            }
            
        }).disposed(by: disposeBag)
    }
    
    func getWallets(){
        let params : [String:Any] = ["purpose" : "getWallets"]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: "accessToken") as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in dataIn{
                        self.WalletArray.append(Wallets.init(data: item))
                    }
                }
                for item in self.WalletArray{
                    self.walletAry.append(item.name)
                }
                self.ResposeBack.onNext(typeHere.Wallets)
            }
        }).disposed(by: disposeBag)
    }
    
    func getTotalAmount(){
        let params : [String:Any] = [
            "purpose" : "getEarnedAmount"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: "accessToken") as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    self.totalAmount = dataIn[0]["earnedAmount"] as? Double ?? 0.00
                }
                self.ResposeBack.onNext(typeHere.TotalAmount)
            }
        }).disposed(by: disposeBag)
    }

}
