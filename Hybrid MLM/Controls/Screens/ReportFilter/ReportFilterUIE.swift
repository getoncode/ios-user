//
//  ReportFilterUIE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 11/08/21.
//
import Foundation
import UIKit
extension ReportFilterVC{
    
    func setUi() {
        dateBtn.layer.cornerRadius          = 8.0
        amountBtn.layer.cornerRadius        = 8.0
        chooseWalletView.layer.cornerRadius = 8.0
        chooseCommissionView.layer.cornerRadius = 8.0
        chooseWalletView.layer.borderWidth = 0.1
        chooseCommissionView.layer.borderWidth = 0.1
        fromDate.inputView = fromdatePicker
        toDate.inputView = toDatePicker
        amountBtn.layer.borderWidth     = 0.1
        dateBtn.layer.borderWidth       = 0.1
        Helper.roundBorder(view: chooseCommissionView, radius: 6.0, borderWidth: 1.0, color: #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1))
        Helper.roundBorder(view: chooseWalletView, radius: 6.0, borderWidth: 1.0, color: #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1))
        Helper.roundBorder(view: fromDateView, radius: 6.0, borderWidth: 1.0, color: #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1))
        Helper.roundBorder(view: ToDateView, radius: 6.0, borderWidth: 1.0, color: #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1))
        btnAscending.layer.cornerRadius  = 8.0
        btnDescending.layer.cornerRadius = 8.0
        btnAscending.layer.borderWidth   = 0.1
        btnDescending.layer.borderWidth  = 0.1
        outerView.layer.cornerRadius     = 15.0
        filterView.layer.cornerRadius    = 15.0
        fromdatePicker.isUserInteractionEnabled = true
        chooseWallet.titleEdgeInsets.left    = 6.0
        chooseCommssion.titleEdgeInsets.left = 6.0
        doneBtn.layer.cornerRadius = 10
    }
    // Add FromDate Picker
    func setUpFromDatePicker() {
        self.fromdatePicker = UIDatePicker.init(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 200))
        fromdatePicker.datePickerMode = .date
        fromdatePicker.addTarget(self, action: #selector(self.FromdateChanged), for: UIControl.Event.allEvents)
        fromdatePicker.preferredDatePickerStyle = .automatic
        self.fromDate.inputView = fromdatePicker
        let toolbar:UIToolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        let donebutton:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.tapondonebtn))
        toolbar.setItems([donebutton], animated: true)
        fromDate.inputAccessoryView = toolbar
    }
    
    @objc func FromdateChanged(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.dateStyle = .medium
        FromDate =  fromdatePicker.date 
        self.fromDate.text = formatter.string(from: FromDate)
    }
    
    @objc func tapondonebtn(){
        fromDate.resignFirstResponder()
    }
    // Add ToDate Picker
    func setUpToDatePicker() {
        self.toDatePicker = UIDatePicker.init(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 200))
        toDatePicker.datePickerMode = .date
        toDatePicker.addTarget(self, action: #selector(self.TodateChanged), for: UIControl.Event.allEvents)
        toDatePicker.preferredDatePickerStyle = .automatic
        self.toDate.inputView = toDatePicker
        let toolbar:UIToolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        let donebutton:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.taponTodonebtn))
        toolbar.setItems([donebutton], animated: true)
        toDate.inputAccessoryView = toolbar
    }
    
    @objc func TodateChanged(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.dateStyle = .medium
        ToDate = toDatePicker.date
        self.toDate.text = formatter.string(from: ToDate)
    }
    
    @objc func taponTodonebtn(){
        toDate.resignFirstResponder()
    }
    
}
