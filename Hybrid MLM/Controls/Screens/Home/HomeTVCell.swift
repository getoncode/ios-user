//
//  HomeTVCell.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 20/04/21.
//

import UIKit

class HomeTVCell: UITableViewCell {

    @IBOutlet weak var lblWithdrawn: UILabel!
    @IBOutlet weak var lblTimeStamp: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(_ data : payoutHistory) {
        if data.amount != 0.0{
            let round = Double(round(100*data.amount)/100)
            lblAmount.text = "$\(round)"
        }
        else{
            let round = Double(round(100*data.request_amount)/100)
            lblAmount.text = "$\(round)"
        }
        lblTimeStamp.text = Helper.dateToString(date: data.created_at, format: "dd MMM yyyy")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
