//
//  HomeTVE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 20/04/21.
//

import Foundation
import UIKit
extension HomeVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if homeVM.payoutHistoryArray.count > 2{
            return 3
        }else{
            return homeVM.payoutHistoryArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = payOutTableView.dequeueReusableCell(withIdentifier: String(describing: HomeTVCell.self), for: indexPath) as! HomeTVCell
        cell.setData(homeVM.payoutHistoryArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
}
