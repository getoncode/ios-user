//
//  HomeVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 30/12/20.
//

import UIKit
import SideMenu
import DropDown
import LifecycleHooks

class HomeVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout,SideMenuNavigationControllerDelegate{
    
    @IBOutlet weak var noGraphAvailablePie: UILabel!
    @IBOutlet weak var pieChartView: UIView!
    @IBOutlet weak var pieChartInnerView: UIView!
    @IBOutlet weak var barGraphView: UIView!
    @IBOutlet weak var pageCntrlRef: UIPageControl!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var barGraphInnrerView: UIView!
    @IBOutlet weak var sliderCV: UICollectionView!
    @IBOutlet weak var payOutView: UIView!
    @IBOutlet weak var lblPayOut: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var payOutTableView: UITableView!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var amountOuterView: UIView!
    @IBOutlet weak var noGraphAvailableBar: UILabel!
    @IBOutlet weak var nodataLabelPayout: UILabel!
    let dropdown = DropDown()
    let amountArray = [StringConstants.All(),StringConstants.lastWeek(),StringConstants.lastMonth(),StringConstants.lastYear()]
    var profileVM = ProfileVM()
    var homeVM = HomeVM()
    var barGraphValues : [Any] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //set ui
        setUi()
        setPage()
        profileVM.getProfileDetails()
        homeVM.getEarningPieDetails()
        homeVM.getPayoutHistory(filter: StringConstants.all())
        if Helper.callRefreshToken(){
            homeVM.getDashboardDetails()
            homeVM.getEarningGraphDetails()
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        setVM()
        //dropDown
        btnDropDown.setTitle(amountArray[0], for: .normal)
        btnDropDown.titleEdgeInsets.left = 6.0
        dropdown.anchorView = dropDownView
        dropdown.dataSource = amountArray
        dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.topOffset = CGPoint(x: 0, y:-(dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.direction = .bottom
        dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btnDropDown.setTitle("\(amountArray[index])", for: .normal)
            switch index {
            case 0:
                homeVM.getPayoutHistory(filter: StringConstants.all())
                break
            case 1:
                homeVM.getPayoutHistory(filter: StringConstants.week())
                break
            case 2:
                homeVM.getPayoutHistory(filter: StringConstants.month())
                break
            case 3:
                homeVM.getPayoutHistory(filter: StringConstants.year())
                break
            default:
                break
            }
        }
        //hook demo
        let vc = SideMenuViewController()
        vc.on(.viewDidAppear) { animated in
            print("View did appear", animated)
        }
        setPieChart()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: true)
    }
    @IBAction func showNotifications(_ sender: Any) {
        let notify = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    
    @IBAction func btnDropDown(_ sender: UIButton) {
        dropdown.show()
    }
    
    @IBAction func btnViewAll(_ sender: UIButton) {
        performSegue(withIdentifier: String(describing: HomeVC.self)+String(describing: PayoutHistoryVC.self), sender: nil)
    }
    
    func setVM() {
        homeVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Dashboard:
                self?.sliderCV.reloadData()
                break
            case .earningGraph:
                self?.setChart(values:self?.homeVM.earningGraphArray ?? [])
                break
            case .earningPie:
                self?.setPieChart()
                break
            case  .Payout:
                if self?.homeVM.payoutHistoryArray.count == 0{
                    self?.nodataLabelPayout.isHidden = false
                    self?.btnViewAll.isHidden = true
                }else{
                    self?.nodataLabelPayout.isHidden = true
                    self?.btnViewAll.isHidden = false
                }
                self?.lblPrice.text = "$ \((self?.homeVM.totalAmount)!)"
                self?.payOutTableView.reloadData()
                break
            default:
                break
            }
        }).disposed(by: homeVM.disposeBag)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
