//
//  HomeVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 30/12/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire

class HomeVM : NSObject{
    enum typeHere :Int {
        case Default = 0
        case Dashboard = 1
        case earningGraph = 2
        case earningPie = 3
        case Payout = 4
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var DashboardArray: [Dashboard] = []
    var sliderArray = [[String: Any]]()
    var earningGraphArray : [earningGraph] = []
    var earningPieArray : [earningPie] = []
    var payoutHistoryArray : [payoutHistory] = []
    var totalAmount = 0.0
    
    func getDashboardDetails() {
        let params : [String:Any] = [
            "purpose" : "dashboard"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.DashboardArray.removeAll()
            self.sliderArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    for item in [dataIn] {
                        self.DashboardArray.append(Dashboard.init(data: item))
                    }
                    //for slider
                    if let sliderData = dataIn["wallets"] as? [String:Any] {
                        for item in [sliderData]{
                            self.sliderArray.append(item)
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.Dashboard)
            }
        }).disposed(by: disposeBag)
    }
    
    func getEarningGraphDetails() {
        let params : [String:Any] = [
            "purpose" : "dashboard",
            "params[slice]" : "earningGraph"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.earningGraphArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    self.earningGraphArray.append(earningGraph.init(data: dataIn))
                }
            }
            self.ResposeBack.onNext(typeHere.earningGraph)
        }).disposed(by: disposeBag)
    }
    
    
    func getEarningPieDetails() {
        let params : [String:Any] = [
            "purpose" : "dashboard",
            "params[slice]" : "earningPie"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.earningPieArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    self.earningPieArray.append(earningPie.init(data: dataIn))
                }
            }
            self.ResposeBack.onNext(typeHere.earningPie)
        }).disposed(by: disposeBag)
    }
    
    func getPayoutHistory(filter: String) {
        let params : [String:Any] = [
            "purpose" : "getPayoutHistory",
            "params[filters]" : filter
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.payoutHistoryArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    if let payoutHistoryData = dataIn["payoutHistoryData"] as? [String:Any]
                    {
                    if let data =  payoutHistoryData["payoutRequest"] as? [[String:Any]]{
                        for item in data{
                            self.payoutHistoryArray.append(payoutHistory.init(data:item))}
                     }
                    }
                        self.totalAmount = dataIn["totalAmount"] as! Double
                }
            }
            self.ResposeBack.onNext(typeHere.Payout)
        }).disposed(by: disposeBag)
    }
    
}
