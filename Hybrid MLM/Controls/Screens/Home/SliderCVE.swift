//
//  SliderCVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/01/21.
//

import Foundation
import UIKit

extension HomeVC{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if homeVM.sliderArray.count > 0 {
            return 1
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SliderCVCell.self), for: indexPath as IndexPath) as! SliderCVCell
        cell.setData(homeVM.sliderArray, indexPath.row)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
      {
          return 0.0
      }
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          return 0.0
          
      }
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          let width = CGFloat (sliderCV.frame.size.width)
          let height = CGFloat (sliderCV.frame.size.height)
          return CGSize(width: width , height: height)
      }
      
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
      }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            //set page control index
            //get index
            let visibleRect = CGRect(origin: sliderCV.contentOffset, size: sliderView.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            let visibleIndexPath = sliderCV.indexPathForItem(at: visiblePoint)
            let visibleRow = visibleIndexPath?.row
            pageCntrlRef.currentPage = visibleRow!
        }
   
}
