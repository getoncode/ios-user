//
//  HomeUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 30/12/20.
//

import Foundation
import UIKit
import Highcharts
import SideMenu

extension HomeVC {
    //ui additionals
    func setUi()  {
        pageCntrlRef.isHidden = true
        noGraphAvailableBar.isHidden = true
        noGraphAvailablePie.isHidden = true
        nodataLabelPayout.isHidden = true
        sliderView.layer.cornerRadius = 15.0
        payOutView.layer.cornerRadius = 15
        sliderCV.layer.cornerRadius = 15.0
        Helper.setShadow(view: sliderView, color: Colors.ShadowColor, radius: 10.0)
        barGraphView.layer.cornerRadius = 15.0
        Helper.setShadow(view: barGraphView, color: Colors.ShadowColor, radius: 10.0)
        pieChartView.layer.cornerRadius = 15.0
        Helper.setShadow(view: pieChartView, color:Colors.ShadowColor , radius: 10.0)
        Helper.setShadow(view: payOutView, color: Colors.ShadowColor, radius: 10.0)
    }
    
    //set pagecontrol number(temporarly static)
    func setPage() {
        pageCntrlRef.numberOfPages = 3
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    
    //set bar graph
    func setChart(values: [Any]) {
        //set graph values
        for item in homeVM.earningGraphArray {
            for value in item.graph {
                if let val = (value as? [String:Any]){
                    self.barGraphValues.append(contentsOf: val["graph"] as! Array)
                }
            }
        }
        //hide if there is no value
        if self.barGraphValues.count == 0 {
            noGraphAvailableBar.isHidden = false
        }
        
        let chartView = HIChartView(frame: barGraphInnrerView.bounds)
        let options = HIOptions()
        let chart = HIChart()
        chart.type = StringConstants.column()
        options.chart = chart
        //title
        let title = HITitle()
        title.text = ""
        options.title = title
        //subtitle
        let subtitle = HISubtitle()
        subtitle.text = ""
        options.subtitle = subtitle
        
        //y axis
        let yAxis = HIYAxis()
        yAxis.min = 0
        yAxis.title = HITitle()
        yAxis.title.text = ""
        options.yAxis = [yAxis]
        
        let tooltip = HITooltip()
        tooltip.formatter = HIFunction(jsFunction: "function () { return '<b>' + this.series.name + '</b><br/>' + this.point.y + ' ' + this.point.name.toLowerCase(); }")
        options.tooltip = tooltip
        
        let data = HIData()
        data.table = "<thead> <tr> <th></th> <th>Jane</th> <th>John</th> </tr> </thead> <tbody> <tr> <th>Apples</th> <td>3</td> <td>4</td> </tr> <tr> <th>Pears</th> <td>2</td> <td>0</td> </tr> <tr> <th>Plums</th> <td>5</td> <td>11</td> </tr> <tr> <th>Bananas</th> <td>1</td> <td>1</td> </tr> <tr> <th>Oranges</th> <td>2</td> <td>4</td> </tr> </tbody>"
        
        
        //x axis
        if homeVM.earningGraphArray.count != 0{
            let xAxis = HIXAxis()
            xAxis.categories = homeVM.earningGraphArray[0].xAxises
            options.xAxis = [xAxis]
            xAxis.title = HITitle()
            xAxis.title.text = ""
        }
        
        //plot options
        let plotOptions = HIPlotOptions()
        plotOptions.column = HIColumn()
        //let series
        var series = [HIColumn]()
        if homeVM.earningGraphArray.count != 0{
            for item in homeVM.earningGraphArray[0].graph {
                if let items = item as? [String:Any] {
                    let col = HIColumn()
                    if let key = items["key"] as? String {
                        col.name = key
                        col.data = items["graph"] as? [Any]
                        var color = HIColor()
                        // color = "FFFFFF"
                        col.color = color
                        //col.color =
                        series.append(col)
                    }
                }
            }
        }
        
        options.series = series
        
        plotOptions.column.pointPadding = 0.1
        plotOptions.column.borderWidth = 0
        options.plotOptions = plotOptions
        plotOptions.area = HIArea()
        plotOptions.area.marker = HIMarker()
        plotOptions.area.marker.states = HIStates()
        plotOptions.area.marker.states.hover = HIHover()
        plotOptions.area.marker.states.hover.enabled = false
        plotOptions.area.showInLegend = false
        //remove hamburger and watermark
        let exporting = HIExporting()
        exporting.enabled = false
        options.exporting = exporting
        let creditss = HICredits()
        creditss.enabled = false
        options.credits = creditss
        chartView.options = options
        self.barGraphInnrerView.addSubview(chartView)
    }
    
    //set donut chart
    func setPieChart() {
        noGraphAvailablePie.isHidden = true
        let chartView = HIChartView(frame: pieChartInnerView.bounds)
        //set options
        let options = HIOptions()
        let chart = HIChart()
        chart.type = StringConstants.pie()
        chart.options3d = HIOptions3d()
        chart.options3d.enabled = true
        chart.options3d.alpha = 45
        options.chart = chart
        //title
        let title = HITitle()
        title.text = ""
        options.title = title
        //subtitle
        let subtitle = HISubtitle()
        subtitle.text = ""
        options.subtitle = subtitle
        //plot options
        let plotOptions = HIPlotOptions()
        plotOptions.pie = HIPie()
        plotOptions.pie.innerSize = 50
        plotOptions.pie.depth = 30
        options.plotOptions = plotOptions
        //set data
        let amount = HIPie()
        amount.name = StringConstants.Amount()
        
        if homeVM.earningPieArray.count > 0{
            amount.data = homeVM.earningPieArray[0].transactions
        }
        else{
            noGraphAvailablePie.isHidden = false
        }
        
        //remove hamburger and watermark
        let exporting = HIExporting()
        exporting.enabled = false
        options.exporting = exporting
        let creditss = HICredits()
        creditss.enabled = false
        options.credits = creditss
        //set options
        options.series = [amount]
        chartView.options = options
        //dark theme
        if UITraitCollection.current.userInterfaceStyle == .dark {
            chartView.overrideUserInterfaceStyle = .dark}
            else {
                chartView.overrideUserInterfaceStyle = .light }
        self.pieChartInnerView.addSubview(chartView)
    }
    
}
