//
//  SliderCVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 06/01/21.
//

import UIKit
import Highcharts
class SliderCVCell: UICollectionViewCell{
    var homeVM = HomeVM()
    var dataKeys = [String]()
    var dataValues = [[String:Any]]()
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var noGraphLabel: UILabel!
    @IBOutlet weak var lastSeenTime: UILabel!
    @IBOutlet weak var lastSeenHead: UILabel!
    @IBOutlet weak var walletName: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var graphView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        noGraphLabel.isHidden = true
    }
    func setChart(_ data : [Any]) {
        let chartView = HIChartView(frame: graphView.bounds)
        let options = HIOptions()
        let chart = HIChart()
        chart.type = StringConstants.area()
        chart.margin = [0,0,0,0]
        options.chart = chart
        let title = HITitle()
        title.text = ""
        options.title = title
        
        let subtitle = HISubtitle()
        subtitle.text = ""
        options.subtitle = subtitle
        
        let xAxis = HIXAxis()
        xAxis.labels = HILabels()
        xAxis.accessibility = HIAccessibility()
        options.xAxis = [xAxis]
        xAxis.visible = false
        
        let yAxis = HIYAxis()
        yAxis.title = HITitle()
        yAxis.title.text = ""
        yAxis.labels = HILabels()
        options.yAxis = [yAxis]
        yAxis.visible = false
        let tooltip = HITooltip()
        tooltip.pointFormat = ""
        options.tooltip = tooltip
        
        let creditss = HICredits()
        creditss.enabled = false
        options.credits = creditss
        
        let plotOptions = HIPlotOptions()
        plotOptions.area = HIArea()
        plotOptions.area.pointStart = 0
        plotOptions.area.marker = HIMarker()
        plotOptions.area.marker.enabled = false
        plotOptions.area.marker.symbol = StringConstants.circle()
        plotOptions.area.marker.radius = 0
        plotOptions.area.marker.states = HIStates()
        plotOptions.area.marker.states.hover = HIHover()
        plotOptions.area.marker.states.hover.enabled = false
        plotOptions.area.showInLegend = false
        options.plotOptions = plotOptions
        
        let exporting = HIExporting()
        exporting.enabled = false
        options.exporting = exporting
        
        let hiData = HIArea()
        hiData.data = (data[0] as? [Any])
        options.series = [hiData]
        if hiData.data.count <= 1{
            noGraphLabel.isHidden = false
        }
        chartView.options = options
        self.graphView.addSubview(chartView)
    }
    
    
    
    func setData(_ data : [[String: Any]],_ index : Int) {
        self.lastSeenTime.text = StringConstants.timesAgo()
        if data.count > 0{
            for key in data[0].keys{
                dataKeys.append(key)
            }
            for value in data[0].values{
                dataValues.append(value as! [String : Any])
            }
        }
        //set labels
        walletName.text = dataKeys[index]
        if let balance = dataValues[index]["balance"] as? Double{
            let blnc = Double(round(100*balance)/100)
            balanceLabel.text = "$\(blnc)"
        }
        
        //set last activity time
        if let timesAgo = dataValues[index]["lastTransaction"] as? [String:Any]{
            lastSeenTime.text = timesAgo["timesAgo"] as? String
        }
        
        //set graph
        setChart(dataValues[index]["graph"] as! [Any])
        
    }
}
