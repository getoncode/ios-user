//
//  TransactionPasswordVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 03/04/21.
//

import UIKit

class TransactionPasswordVC: UIViewController {
    @IBOutlet weak var TransactionPassView: UIView!
    @IBOutlet weak var processButton: UIButton!
    @IBOutlet weak var passwordTF: UITextField!
    var transactionPasswordVM = TransactionPasswordVM()
    let transactionSuccessVm = TransactionSuccessVM()
    var walletId = ""
    var amount = ""
    var remarks = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        setVM()
    }
    @IBAction func process(_ sender: Any) {
        if passwordTF.text?.count ?? 0 > 0{
            var recipients = ""
            for i in UserDefaults.standard.value(forKey: UserDefaultConstants.Recipientsid) as! [Int]{
                if recipients == ""{
                    recipients = recipients + "\(i)"
                }else{
                    recipients = recipients + "," + "\(i)"
                }
            }
            if remarks == "Add Note"{
                remarks = ""
            }
            transactionPasswordVM.sendMoney(password: passwordTF.text!, wallet: walletId, amount: amount, remarks: remarks, recipients: recipients)
        }else{
            Helper.showAlert(message: StringConstants.Enteryourtransactionpassword(), head: "")
        }
    }
    
    
    //setvm
    func setVM() {
        transactionPasswordVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Success:
                let cont = self?.presentingViewController ?? self
                self?.dismiss(animated: true, completion: {
                    let successVC = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: TransactionSuccessVC.self)) 
                    UserDefaults.standard.setValue(self?.amount, forKey: StringConstants.TransferredSuccessfully())
                    successVC.modalPresentationStyle = .overFullScreen
                    cont?.present(successVC, animated: true, completion: nil)
                })
                Helper.showAlert(message: StringConstants.Success(), head: "")
                break
            default:
                break
            }
        }).disposed(by: transactionPasswordVM.disposeBag)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
