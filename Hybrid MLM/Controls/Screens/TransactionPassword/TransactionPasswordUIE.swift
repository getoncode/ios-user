//
//  TransactionPasswordUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 03/04/21.
//

import Foundation
import UIKit

extension TransactionPasswordVC{
    func setUi() {
        processButton.layer.cornerRadius = 8.0
    }
    
    //add and remove observers
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    //dismiss viewcontroller on tapping outside search view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.view {
//            let data = UserDefaults.standard.value(forKey: UserDefaultConstants.Recipients)
//            responseBack.onNext(data)
            self.dismiss(animated: true, completion: nil)
        }
    }
}
