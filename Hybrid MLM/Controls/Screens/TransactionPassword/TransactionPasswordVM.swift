//
//  TransactionPasswordVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 22/04/21.
//

import Foundation
import Foundation
import RxCocoa
import RxSwift

class TransactionPasswordVM: NSObject{
    enum typeHere:Int {
        case Default = 0
        case Success = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var SettingsArray: [Settings] = []
    
    func sendMoney(password: String, wallet: String, amount: String, remarks: String,recipients: String) {
        let params : [String:Any] = [
            "purpose" : "initializeEwalletTransaction",
            "params[transaction_password]" : password,
            "params[requests][wallet]" : wallet,
            "params[requests][user_id]" : recipients,
            "params[requests][amount]" : amount,
            "params[remarks]" : remarks
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.SettingsArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    for item in [dataIn] {
                        self.SettingsArray.append(Settings.init(data: item))
                    }
                }
                self.ResposeBack.onNext(typeHere.Success)
            }
            
        }).disposed(by: disposeBag)
    }
}
