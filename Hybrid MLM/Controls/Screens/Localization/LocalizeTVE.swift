//
//  LocalizeTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/01/21.
//

import Foundation
import UIKit

extension LocalizationVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if lType == StringConstants.Currency(){
            return localizationVM.currencyArray.count
        }
        return localizationVM.LanguageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: LocalizeTVCell.self))) as! LocalizeTVCell
        tableView.rowHeight = 60.0
        if lType == StringConstants.Currency(){
            cell.setData(localizationVM.currencyArray[indexPath.row])
            if indexPath.row == (localizationVM.currencyArray.count - 1)
            {
                cell.seperatorView.isHidden = true
            }
            else{
                cell.seperatorView.isHidden = false
            }
        }else{
            cell.setLanguageData(localizationVM.LanguageArray[indexPath.row])
            if indexPath.row == (localizationVM.LanguageArray.count - 1)
            {
                cell.seperatorView.isHidden = true
            }
            else{
                cell.seperatorView.isHidden = false
            }
        }
        
        return cell
    }
    func setTableHeight() {
        var height : CGFloat = 0.0
        if lType == StringConstants.Currency(){
            height = CGFloat(60.0 * Double(localizationVM.currencyArray.count))
        }
        else
        {
            height = CGFloat(60.0 * Double(localizationVM.LanguageArray.count))
        }
        if height ?? 0 > self.view.frame.height - 100
        {
            if notchDevice.contains(modelName)
            {
                containerHeightConstraint.constant = self.view.frame.height - 100
            }
            else
            {
                containerHeightConstraint.constant = self.view.frame.height - 50
            }
        }
        else
        {
            containerHeightConstraint.constant = height + 20
        }
    }
}
