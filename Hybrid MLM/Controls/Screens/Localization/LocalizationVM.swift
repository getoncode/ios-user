//
//  LocalizationVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 02/12/20.
//

import Foundation
import RxCocoa
import RxSwift

class LocalizationVM: NSObject {
    enum typeHere:Int {
        case Default = 0
        case Currency = 1
        case Language = 2
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var currencyArray: [Currency] = []
    var LanguageArray: [Language] = []
    
    func getCurrencies() {
        let params : [String:Any] = [
            "purpose" : "getCurrencies"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.currencyArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in dataIn {
                        for item1 in [item] {
                            self.currencyArray.append(Currency.init(data: item1))
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.Currency)
            }
            
        }).disposed(by: disposeBag)
    }
    
    func getLanguages() {
        let params : [String:Any] = [
            "purpose" : "getLanguages"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.LanguageArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in dataIn {
                        for item1 in [item] {
                            self.LanguageArray.append(Language.init(data: item1))
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.Language)
            }
            
        }).disposed(by: disposeBag)
    }
}
