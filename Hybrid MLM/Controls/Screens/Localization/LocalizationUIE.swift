//
//  LocalizationUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 02/12/20.
//

import Foundation
import UIKit

extension LocalizationVC {
    func setUi() {
        container.layer.cornerRadius = 8.0
        Helper.setShadow(view: container, color: Colors.ShadowColor, radius: 15.0)
        //show navigationBar
        self.navigationController?.navigationBar.isHidden = false
        notchDevice  = [StringConstants.iPhoneX(),StringConstants.iPhoneXS(),StringConstants.iPhoneXSMax(),StringConstants.iPhoneXR(),StringConstants.iPhone11(),StringConstants.iPhone11Pro(),StringConstants.iPhone11ProMax(),StringConstants.iPhone12mini(),StringConstants.iPhone12(),StringConstants.iPhone12Pro(),StringConstants.iPhone12ProMax(),StringConstants.iPhone13mini(),StringConstants.iPhone13(),StringConstants.iPhone13Pro(),StringConstants.iPhone13ProMax()]
        
        setTableHeight()
        headLabel.text = lType!
    }
}

