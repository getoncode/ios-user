//
//  LocalizeTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 02/12/20.
//

import UIKit

class LocalizeTVCell: UITableViewCell {

    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var currencyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(_ data: Currency) {
        currencyLabel.text = data.name
        }
    func setLanguageData(_ data: Language) {
        currencyLabel.text = data.name
        }

}
