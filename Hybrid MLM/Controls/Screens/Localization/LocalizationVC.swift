//
//  LocalizationVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 02/12/20.
//

import UIKit

class LocalizationVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var locTableView: UITableView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    var lType: String?
    
    let modelName = UIDevice.modelName
    var notchDevice : [String] = []
    let localizationVM = LocalizationVM()
    override func viewDidLoad() {
        super.viewDidLoad()

        fetchData()
        setUi()
        setVM()
    }
    
    func fetchData(){
        if Helper.callRefreshToken(){
        if lType == StringConstants.Currency(){
            self.localizationVM.getCurrencies()
        }else{
            self.localizationVM.getLanguages()
        }
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    @IBAction func showNotification(_ sender: Any) {
        let notify = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    
    func setVM() {
        localizationVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Currency:
                self?.setTableHeight()
                self?.locTableView.reloadData()
                break
            case .Language:
                self?.setTableHeight()
                self?.locTableView.reloadData()
                break
            default:
                break
            }
        }).disposed(by: localizationVM.disposeBag)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
