//
//  OtpUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 10/11/20.
//

import Foundation
import UIKit

extension OTPViewController
{
    func setUi()  {
        Helper.roundBorder(view: OtpViewOne, radius: 30.0, borderWidth: 1.5, color: Colors.AppBaseColor.cgColor)
        Helper.roundBorder(view: OtpViewTwo, radius: 30.0, borderWidth: 1.5, color: Colors.AppBaseColor.cgColor)
        Helper.roundBorder(view: OtpViewThree, radius: 30.0, borderWidth: 1.5, color: Colors.AppBaseColor.cgColor)
        Helper.roundBorder(view: OtpViewFour, radius: 30.0, borderWidth: 1.5, color: Colors.AppBaseColor.cgColor)
        
        self.navigationController?.navigationBar.isHidden = false
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 20.0)
        
    }
    func setOtpTFs()  {
        otpTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpTwoTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpThreeTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpFourTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpTF.becomeFirstResponder()
    }
    //setting OTP textfield count minimum 1
    @objc func textFieldDidChange(textField: UITextField){
        
        let text = textField.text
        
        if text?.utf16.count==1{
            switch textField{
            case otpTF:
                otpTwoTF.becomeFirstResponder()
            case otpTwoTF:
                otpThreeTF.becomeFirstResponder()
            case otpThreeTF:
                otpFourTF.becomeFirstResponder()
            case otpFourTF:
                otpFourTF.resignFirstResponder()
            default:
                break
            }
        }else{
            
        }
    }
    //set maximum length of otp textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        switch textField {
        case otpTF:
            if ((textField.text?.length)! + (string.length - range.length)) > 1 {
                return false
            }

        case otpTwoTF:
            if ((textField.text?.length)! + (string.length - range.length)) > 1 {
                return false
            }
        case otpThreeTF:
            if ((textField.text?.length)! + (string.length - range.length)) > 1 {
                return false
            }
        case otpFourTF:
            if ((textField.text?.length)! + (string.length - range.length)) > 1 {
                return false
            }
        default:
            return true
        }
        return true
    }
}

