//
//  OTPControllerVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 29/06/21.
//

import Foundation
import RxCocoa
import RxSwift

class OTPControllerVM: NSObject {
    enum typeHere:Int {
        case Default = 0
        case Reset = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    
    func resetPassword(email: String,password :String,confirmPass: String) {
        let params : [String:Any] = [
            "purpose" : "resetPassword",
            "params[email]" : email,
            "params[password]" : password,
            "params[password_confirmation]" : confirmPass
            
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                // response here
            }
            self.ResposeBack.onNext(typeHere.Reset)
        }).disposed(by: disposeBag)
    }
    
}
