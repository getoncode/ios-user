//
//  OTPViewController.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 10/11/20.
//

import UIKit

class OTPViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var OtpViewOne: UIView!
    @IBOutlet weak var OtpViewTwo: UIView!
    @IBOutlet weak var OtpViewThree: UIView!
    @IBOutlet weak var OtpViewFour: UIView!
    @IBOutlet weak var otpTF: UITextField!
    @IBOutlet weak var otpTwoTF: UITextField!
    @IBOutlet weak var otpThreeTF: UITextField!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var otpFourTF: UITextField!
    let oTPControllerVM = OTPControllerVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        otpTF.delegate = self
        otpTwoTF.delegate = self
        otpThreeTF.delegate = self
        otpFourTF.delegate = self
        setUi()
        setOtpTFs()
        oTPControllerVM.resetPassword(email: "", password: "", confirmPass: "")
        setVM()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool){
    removeObserver()
    }
    
    @IBAction func resendCodeClick(_ sender: Any) {
    }
    
    func setVM() {
        oTPControllerVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Reset:
                //redirect to home vc
                break
            default:
                break
            }
        }).disposed(by: oTPControllerVM.disposeBag)
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
