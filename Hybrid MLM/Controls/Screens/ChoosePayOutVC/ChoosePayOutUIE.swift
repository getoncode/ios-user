//
//  ChoosePayOutUIE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 24/06/21.
//

import Foundation
import UIKit
extension ChoosePayoutVC{
    func setUi() {
        Helper.setShadow(view: payoutTable, color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.7), radius: 20.0)
        payoutTable.layer.cornerRadius = 8.0
    }
    //add and remove observers
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    //dismiss viewcontroller on tapping outside search view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.view {
            //            let data = UserDefaults.standard.value(forKey: UserDefaultConstants.Recipients)
            //            responseBack.onNext(data)
            self.dismiss(animated: true, completion: nil)
        }
    }
}
