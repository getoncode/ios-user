//
//  ChoosePayoutVC.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 24/06/21.
//

import UIKit
import RxCocoa
import RxSwift
class ChoosePayoutVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var payOutOuterView: UIView!
    @IBOutlet weak var payoutTitle: UILabel!
    @IBOutlet weak var payoutTable: UITableView!
    let sendMoneyVM = PayOutVM()
    var dataArray : [Payout] = []
    let responseBack = PublishSubject<String>()
    let responseBack1 = PublishSubject<Int>()
    let disposebag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        
        // Do any additional setup after loading the view
        
    }
}
