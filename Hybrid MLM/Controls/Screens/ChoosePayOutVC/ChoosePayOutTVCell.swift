//
//  ChoosePayOutTVCell.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 24/06/21.
//

import UIKit

class ChoosePayOutTVCell: UITableViewCell {
    
    @IBOutlet weak var walletImage: UIImageView!
    @IBOutlet weak var walletNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    //set data after api call
    func setData(_ data: Payout) {
        walletNameLabel.text = data.name
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
