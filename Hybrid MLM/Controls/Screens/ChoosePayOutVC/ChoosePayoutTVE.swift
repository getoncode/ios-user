//
//  ChoosePayoutTVE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 24/06/21.
//

import Foundation
import UIKit
extension ChoosePayoutVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: ChoosePayOutTVCell.self))) as! ChoosePayOutTVCell
        cell.layer.cornerRadius = 8.0
        tableView.rowHeight = 70
        cell.setData(dataArray[indexPath.row])
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        responseBack.onNext(dataArray[indexPath.row].name)
        self.dismiss(animated: true, completion: nil)
    }
    
}
