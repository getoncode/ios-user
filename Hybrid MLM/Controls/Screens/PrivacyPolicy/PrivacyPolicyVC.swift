//
//  PrivacyPolicyVC.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 23/09/21.
//

import UIKit

class PrivacyPolicyVC: UIViewController {
    @IBOutlet weak var privacyPolicyLbl: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var privacyPolicyTxtView: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
    }

}
