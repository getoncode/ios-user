//
//  PayerTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 27/04/21.
//

import Foundation
import UIKit

extension TransactionSuccessVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if transactionSuccessVm.payoutRequestArray.count >= 4{
            return 4
        }else{
        return transactionSuccessVm.payoutRequestArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: PayerTVCell.self))) as! PayerTVCell
        if UserDefaults.standard.value(forKey: StringConstants.Request()) as? String == StringConstants.Request(){
            cell.setDataRelease(transactionSuccessVm.payoutReleaseArray[indexPath.row])
        }
        cell.setData(transactionSuccessVm.payoutRequestArray[indexPath.row])
        return cell
    }
}

