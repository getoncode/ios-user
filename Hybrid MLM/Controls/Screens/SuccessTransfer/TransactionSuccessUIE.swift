//
//  TransactionSuccessUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 22/04/21.
//

import Foundation
import UIKit

extension TransactionSuccessVC{
    func setUi() {
        UIImage.gifAnimatedImageWithName(StringConstants.loading(), animationImage, 2, 1.5)
        TransactionTable.rowHeight = 91.0
        TransactionTable.layer.cornerRadius = 15.0
        Helper.setShadow(view: TransactionTable, color: Colors.ShadowColor, radius: 10.0)
        animationImage.layer.cornerRadius = 75.0
        
        if UserDefaults.standard.value(forKey: StringConstants.TransferredSuccessfully()) != nil{
            let amount = Double(UserDefaults.standard.value(forKey: StringConstants.TransferredSuccessfully()) as! String)
            let amountString = String(format:"%.2f",amount!)
            transactionSuccessLAbel.text = "Transferred  $ \(amountString) Successfully!"}
        else{
            transactionSuccessLAbel.text = StringConstants.TransferredSuccessfully()
        }
    }    
}
