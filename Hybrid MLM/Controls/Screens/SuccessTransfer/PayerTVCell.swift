//
//  PayerTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 27/04/21.
//

import UIKit

class PayerTVCell: UITableViewCell {
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var payerLabel: UILabel!
    @IBOutlet weak var transactionIdLabel: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        outerView.layer.cornerRadius = 15.0
       
    }
    func setData(_ data: PayoutReqHistory) {
        let amountValue = data.dataAry["request_amount"] as? Double
        let amountStr = String(format: "%.2f",amountValue!)
        lblPrice.text = "$ \(amountStr)"
        let transaction = data.dataAry["id"] as? Int
        transactionIdLabel.text = "#\(String(describing: transaction!))"
        payerLabel.text = Utility.UserData.username
    }
    func setDataRelease(_ data: PayoutRelHistory) {
        let amountValue = data.dataAry["request_amount"] as? Double
        let amountStr = String(format: "%.2f",amountValue!)
        lblPrice.text = "$ \(amountStr)"
        let transaction = data.dataAry["id"] as? Int
        transactionIdLabel.text = "#\(String(describing: transaction!))"
        payerLabel.text = Utility.UserData.username
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
