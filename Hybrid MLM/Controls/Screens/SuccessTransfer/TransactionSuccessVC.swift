//
//  TransactionSuccessVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 22/04/21.
//

import UIKit
import UIKit

class TransactionSuccessVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var TransactionTable: UITableView!
    @IBOutlet weak var animationImage: UIImageView!
    @IBOutlet weak var seperationView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var transactionSuccessLAbel: UILabel!
    let transactionSuccessVm = TransactionSuccessVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        setVM()
        transactionSuccessVm.getPayoutRequestHistory()
        if UserDefaults.standard.value(forKey: StringConstants.Request()) as? String == StringConstants.Request(){
            transactionSuccessVm.getPayoutRequestHistory()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        UserDefaults.standard.removeObject(forKey: StringConstants.Request())
        UserDefaults.standard.removeObject(forKey: StringConstants.TransferredSuccessfully())
    }
    @IBAction func closeBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //setvm
    func setVM() {
        transactionSuccessVm.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .RequestHistory:
                self?.TransactionTable.reloadData()
                break
            case .ReleaseHistory:
                self?.TransactionTable.reloadData()
                break
            default:
                break
            }
        }).disposed(by: transactionSuccessVm.disposeBag)
    }
    
}

