 //
 //  TransactionSuccessVM.swift
 //  Hybrid MLM
 //
 //  Created by Shahna Nabeel on 22/04/21.
 //
 
 import Foundation
 import RxCocoa
 import RxSwift
 
 class TransactionSuccessVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case RequestHistory = 1
        case ReleaseHistory = 2
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var payoutRequestArray : [PayoutReqHistory] = []
    var payoutReleaseArray : [PayoutRelHistory] = []
    
    func getPayoutRequestHistory() {
        let params : [String:Any] = [
            "purpose" : "payoutRequestHistory"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.payoutRequestArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in [dataIn] {
                        for itm in item{
                            self.payoutRequestArray.append(PayoutReqHistory.init(data: itm))
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.RequestHistory)
            }
            
        }).disposed(by: disposeBag)
    }
    
    func getPayoutReleaseHistory() {
        let params : [String:Any] = [
            "purpose" : "payoutReleaseHistory"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.payoutReleaseArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in [dataIn] {
                        for itm in item{
                            self.payoutReleaseArray.append(PayoutRelHistory.init(data: itm))
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.ReleaseHistory)
            }
            
        }).disposed(by: disposeBag)
    }
 }
 
 
