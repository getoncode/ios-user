//
//  WalletVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 23/11/20.
//

import UIKit

class WalletVC: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var overView: UIView!
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var graphViewContainer: UIView!
    @IBOutlet weak var noGraphLabel: UILabel!
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var balanceLabel: UILabel!
    let walletVM = WalletVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        if Helper.callRefreshToken(){
            walletVM.getWalletBalance()
            walletVM.getIncomeChart()
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        setVM()
    }
    override func viewWillDisappear(_ animated: Bool) {
        overView.backgroundColor = .white
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        overView.backgroundColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: true)
    }
    @IBAction func TransactionButtonClick(_ sender: Any) {
        overView.backgroundColor = .white
        performSegue(withIdentifier: String(describing: WalletVC.self) + String(describing: TransactionsVC.self), sender: nil)
    }
    @IBAction func settingsAction(_ sender: Any) {
        overView.backgroundColor = .white
        performSegue(withIdentifier: String(describing: WalletVC.self) + String(describing: SettingsVC.self), sender: nil)
    }
    @IBAction func sendAction(_ sender: Any) {
        overView.backgroundColor = .white
        performSegue(withIdentifier: String(describing: WalletVC.self)+String(describing: SendMoneyVC.self), sender: nil)
    }
    func setVM() {
        walletVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Balance:
                self?.balanceLabel.text = self?.walletVM.balance
                break
            case .Overview:
                self?.setGraph(values:self?.walletVM.OverViewArray ?? [])
                break
            default:
                break
            }
        }).disposed(by: walletVM.disposeBag)
    }
    
    
}
