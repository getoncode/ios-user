//
//  WalletVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 23/11/20.
//

import Foundation
import RxCocoa
import RxSwift

class WalletVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case Balance = 1
        case Overview = 2
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var balance : String?
    var OverViewArray : [OverView] = []
    
    func getWalletBalance() {
        let params : [String:Any] = [
            "purpose" : "getEwalletBalance"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    for item in [dataIn] {
                        self.balance = item["data"] as? String
                    }
                }
                self.ResposeBack.onNext(typeHere.Balance)
            }
            
        }).disposed(by: disposeBag)
    }
    
    func getIncomeChart() {
        let params : [String:Any] = [
            "purpose" : "getIncomeChart"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.OverViewArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    for item in [dataIn] {
                        self.OverViewArray.append(OverView.init(data: item))
                    }
                }
                self.ResposeBack.onNext(typeHere.Overview)
            }
            
        }).disposed(by: disposeBag)
    }
}


