//
//  WalletUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 23/11/20.
//

import Foundation
import UIKit
import Highcharts

extension WalletVC
{
    func setUi()  {
        noGraphLabel.isHidden = true
        topView.layer.cornerRadius = 8.0
        graphViewContainer.layer.cornerRadius = 8.0
        graphView.layer.cornerRadius = 8.0
        graphView.translatesAutoresizingMaskIntoConstraints = false
        balanceView.layer.cornerRadius = 8.0
        Helper.setShadow(view: balanceView, color: Colors.ShadowColor, radius: 10.0)
        Helper.setShadow(view: topView, color: Colors.ShadowColor, radius: 10.0)
        Helper.setShadow(view: graphViewContainer, color: Colors.ShadowColor, radius: 10.0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    
    func setGraph(values: [Any]){
        if values .count <= 1{
            noGraphLabel.isHidden = false
        }
        let chartView = HIChartView(frame: graphView.bounds)
        let options = HIOptions()
        let chart = HIChart()
        chart.type = StringConstants.area()
        chart.margin = [0,0,0,0]
        options.chart = chart
        options.chart.borderRadius = 8.0
        let title = HITitle()
        title.text = ""
        options.title = title
        
        let subtitle = HISubtitle()
        subtitle.text = ""
        options.subtitle = subtitle
        
        let xAxis = HIXAxis()
        xAxis.labels = HILabels()
        xAxis.accessibility = HIAccessibility()
        options.xAxis = [xAxis]
        xAxis.visible = false
        
        let yAxis = HIYAxis()
        yAxis.title = HITitle()
        yAxis.title.text = ""
        yAxis.labels = HILabels()
        options.yAxis = [yAxis]
        yAxis.visible = false
        let tooltip = HITooltip()
        tooltip.pointFormat = ""
        options.tooltip = tooltip
        
        let creditss = HICredits()
        creditss.enabled = false
        options.credits = creditss
        
        let plotOptions = HIPlotOptions()
        plotOptions.area = HIArea()
        plotOptions.area.pointStart = 0
        plotOptions.area.marker = HIMarker()
        plotOptions.area.marker.enabled = false
        plotOptions.area.marker.symbol = StringConstants.circle()
        plotOptions.area.marker.radius = 0
        plotOptions.area.marker.states = HIStates()
        plotOptions.area.marker.states.hover = HIHover()
        plotOptions.area.marker.states.hover.enabled = false
        plotOptions.area.showInLegend = false
        options.plotOptions = plotOptions
       
        let exporting = HIExporting()
        exporting.enabled = false
        options.exporting = exporting

        let ussrRussia = HIArea()
        ussrRussia.data = (values[0] as? [Any])
        options.series = [ussrRussia]
        chartView.options = options
        self.graphView.addSubview(chartView)
    }
}
