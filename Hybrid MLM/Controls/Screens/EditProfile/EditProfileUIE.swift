//
//  EditProfileUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/11/20.
//

import Foundation
import UIKit

extension EditProfileVC
{
    func setUi()  {
        //show navigationBar
        self.navigationController?.navigationBar.isHidden = false
        userImageContainer.layer.cornerRadius = 50.0
        userImageView.layer.cornerRadius = 50.0
        topView.layer.cornerRadius = 50.0
        topView.isHidden = true
        changeProfileButton.isHidden = true
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 20.0)
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
        genderDropDown.anchorView = genderButton
        genderDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.genderButton.setTitle(item, for: .normal)
        }
        genderDropDown.dataSource = [StringConstants.Male(),StringConstants.Female()]
    }
    func setTFInteraction()
    {
        if FirstNameTF.isUserInteractionEnabled == false
        {
            FirstNameTF.isUserInteractionEnabled = true
            FirstNameTF.becomeFirstResponder()
            LasttNameTF.isUserInteractionEnabled = true
            EmailTF.isUserInteractionEnabled = true
            PhoneTF.isUserInteractionEnabled = true
            genderButton.isUserInteractionEnabled = true
            DateOfBirthTF.isUserInteractionEnabled = true
            addressTF.isUserInteractionEnabled = true
            stateButton.isUserInteractionEnabled = true
            countryButton.isUserInteractionEnabled = true
            cityTF.isUserInteractionEnabled = true
            pinTF.isUserInteractionEnabled = true
            topView.isHidden = false
            changeProfileButton.isHidden = false
        }
        else
        {
            FirstNameTF.isUserInteractionEnabled = false
            LasttNameTF.isUserInteractionEnabled = false
            EmailTF.isUserInteractionEnabled = false
            PhoneTF.isUserInteractionEnabled = false
            genderButton.isUserInteractionEnabled = false
            DateOfBirthTF.isUserInteractionEnabled = false
            addressTF.isUserInteractionEnabled = false
            stateButton.isUserInteractionEnabled = false
            countryButton.isUserInteractionEnabled = false
            cityTF.isUserInteractionEnabled = false
            pinTF.isUserInteractionEnabled = false
            topView.isHidden = true
            changeProfileButton.isHidden = true
        }
    }
    //set user data
    func setUserData() {
        selectionType = ""
        let data = Utility.UserData.FullData
        FirstNameTF.text = data["firstname"] as? String
        LasttNameTF.text = data["lastname"] as? String
        EmailTF.text = data["email"] as? String
        PhoneTF.text = data["phone"] as? String
        if data["gender"] as? String == "M"{
            genderButton.setTitle(StringConstants.Male(), for: .normal)
        }
        else{
            genderButton.setTitle(StringConstants.Female(), for: .normal)
        }
        DateOfBirthTF.text = data["dob"] as? String
        let url = data["profilePic"] as? String ?? ""
        Helper.setImage(image:  APIKeys.baseURLFirst + url , on: userImageView)
        addressTF.text = data["address"] as? String
        stateButton.setTitle(String(describing: data["state"] ?? "") , for: .normal)
        countryButton.setTitle(String(describing:data["country"] ?? ""), for: .normal)
        pinTF.text = String(describing: data["pin"] ?? "")
        cityTF.text = data["city"] as? String
    }
    
    func validateFields(){
        if FirstNameTF.text?.count == 0{
            Helper.showAlert(message: StringConstants.FirstNameMissing(), head: StringConstants.Error())
        }
        else{
            if Helper.validateAlphabets(password: FirstNameTF.text!){
                if LasttNameTF.text?.count == 0{
                    Helper.showAlert(message: StringConstants.LastNameMissing(), head: StringConstants.Error())
                }
                else{
                    if Helper.validateAlphabets(password: LasttNameTF.text!){
                        if Helper.isValidEmail(text: EmailTF.text ?? ""){
                            if Helper.validateNumeric(password: PhoneTF.text ?? ""){
                                if DateOfBirthTF.text?.count == 0{
                                    Helper.showAlert(message: StringConstants.EnterYourDOB(), head: StringConstants.Error())
                                }
                                else{
                                    if genderButton.currentTitle == nil{
                                        Helper.showAlert(message: StringConstants.EnterYourGender(), head: StringConstants.Error())
                                    }else{
                                        if addressTF.text?.count == 0{
                                            Helper.showAlert(message: StringConstants.EnterYourAddress(), head: StringConstants.Error())
                                        }else{
                                            if stateButton.currentTitle == nil{
                                                Helper.showAlert(message: StringConstants.EnterYourState(), head: StringConstants.Error())
                                            }else{
                                                if countryButton.currentTitle == nil{
                                                    Helper.showAlert(message: StringConstants.EnterYourCountry(), head: StringConstants.Error())
                                                }else{
                                                    if  cityTF.text?.count == 0{
                                                        Helper.showAlert(message: StringConstants.EnterYourCity(), head: StringConstants.Error())
                                                    }
                                                    else{
                                                        if pinTF.text?.count == 0{
                                                            Helper.showAlert(message: StringConstants.EnterYourPincode(), head: StringConstants.Error())
                                                        }else{
                                                            if Helper.validateNumeric(password: pinTF.text ?? ""){
                                                                EditProfileButton.setTitle(StringConstants.Edit(), for: .normal)
                                                                setTFInteraction()
                                                                //let cntry = editProfileVM.stateId ?? 0
                                                                //let stat = editProfileVM.countryId ?? 0
                                                                var gender = genderButton.currentTitle ?? ""
                                                                if gender == StringConstants.Male(){
                                                                    gender = "M"
                                                                }
                                                                else{
                                                                    gender = "F"
                                                                }
                                                                if Helper.callRefreshToken(){
                                                                editProfileVM.updateProfile(firstName: FirstNameTF.text!, lastName: LasttNameTF.text!, email: EmailTF.text!, phone: PhoneTF.text!, dob: DateOfBirthTF.text!, pic: ProfileString, gender: gender, address: addressTF.text!, city: cityTF.text!, pin: pinTF.text!)
                                                                }else{
                                                                    Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                                                                        Helper.logout()
                                                                    })
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Helper.showAlert(message: StringConstants.EnteraValidPincode(), head: StringConstants.Error())
                                                            } } } }  } } }}
                                
                            }
                            else{
                                Helper.showAlert(message: StringConstants.EnteraValidPhoneNumber(), head: StringConstants.Error())
                            }
                        }
                        else{
                            Helper.showAlert(message: StringConstants.EnteraValidEmail(), head: StringConstants.Error())
                        }
                    }
                    else{
                        Helper.showAlert(message: StringConstants.LastnameOnlyAlphabets(), head: StringConstants.Error())
                    }
                } }
            else{
                Helper.showAlert(message: StringConstants.FirstnameOnlyAlphabets(), head: StringConstants.Error())
            }
        }
    }
    
}

