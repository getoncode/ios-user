//
//  selectionTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 29/01/21.
//

import UIKit

class selectionTVCell: UITableViewCell {
    @IBOutlet weak var headLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCountries(_ data: Countries) {
        headLabel.text = data.name
    }
    func setStates(_ data: States) {
        headLabel.text = data.name
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
