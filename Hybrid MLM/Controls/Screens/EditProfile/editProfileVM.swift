//
//  editProfileVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 30/01/21.
//

import Foundation
import RxCocoa
import RxSwift

class EditProfileVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case Edit = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var PersonalArray: [Personal] = []
    var stateId : Int?
    var countryId : Int?
    
    func updateProfile(firstName: String,lastName: String,email:String, phone: String,dob:String, pic:String,gender: String, address: String, city:String, pin:String) {
        if countryId == nil{
            countryId = Utility.UserData.country_id
        }
        let params : [String:Any] = [
            "purpose" : "updateProfile",
            "params[section]" : "personal",
            "params[firstname]" : firstName,
            "params[lastname]" : lastName,
            "params[email]" : email,
            "params[phone]" : phone,
            "params[dob]" : dob,
            "params[profilePic]" : pic,
            "params[gender]" : gender,
            "params[address]" : address,
            "params[country_id]" : countryId ?? 0,
            "params[state_id]" : stateId ?? 0,
            "params[city]" : city,
            "params[pin]" : Int(pin) ?? 0
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.PersonalArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    for item in [dataIn] {
                        //set data after profile edit
                            self.PersonalArray.append(Personal.init(data: item))
                            Utility.saveuserData(value: item["personal"] as? [String : Any] ?? [:])
                    }
                }
                self.ResposeBack.onNext(typeHere.Edit)
            }
            
        }).disposed(by: disposeBag)
    }
}

                        
