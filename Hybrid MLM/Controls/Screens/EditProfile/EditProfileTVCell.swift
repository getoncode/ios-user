//
//  EditProfileTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 16/11/20.
//

import UIKit

class EditProfileTVCell: UITableViewCell {
    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var userDataLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
