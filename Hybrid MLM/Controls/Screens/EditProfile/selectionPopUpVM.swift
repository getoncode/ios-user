//
//  selectionPopUpVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 29/01/21.
//

import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire

class SelectionPopUPVM : NSObject{
    enum typeHere :Int {
        case Default = 0
        case Country = 1
        case State = 2
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var CountryList: [Countries] = []
    var StateList: [States] = []
    
    
    func getCountries() {
        let params : [String:Any] = [
            "purpose" : "getCountries",
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.CountryList.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                        for item in dataIn {
                            self.CountryList.append(Countries.init(data: item))
                        }
                    
                }
                self.ResposeBack.onNext(typeHere.Country)
            }
        }).disposed(by: disposeBag)
    }
    
    func getStates(cntryId: Int) {
        let params : [String:Any] = [
            "purpose" : "getStates",
            "params[countryId]" : cntryId
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.StateList.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                        for item in dataIn {
                            self.StateList.append(States.init(data: item))
                        }     }
                self.ResposeBack.onNext(typeHere.State)
            }
        }).disposed(by: disposeBag)
    }
    
    
}



