//
//  SelectionPopUpVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 29/01/21.
//

import UIKit
import RxCocoa
import RxSwift


class SelectionPopUpVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var selectionTableView: UITableView!
    @IBOutlet weak var outerView: UIView!
    let responseBack = PublishSubject<Countries>()
    let responseBack1 = PublishSubject<States>()
    let disposebag = DisposeBag()
    var selectionPopUpVM = SelectionPopUPVM()
    var type : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        closeButton.layer.cornerRadius = 17.5
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.7), radius: 20.0)
        if type == StringConstants.State(){
            if UserDefaults.standard.value(forKey: StringConstants.SelectedCountry()) != nil{
                selectionPopUpVM.getStates(cntryId: UserDefaults.standard.value(forKey: StringConstants.SelectedCountry()) as! Int)
            }
            else{
            selectionPopUpVM.getStates(cntryId: Utility.UserData.country_id)
            }
        }
        else if type == StringConstants.Country(){
        selectionPopUpVM.getCountries()
        }
        setVM()
    }
    

    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setVM() {
        selectionPopUpVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Country:
                self?.selectionTableView.reloadData()
                break
            case .State:
                self?.selectionTableView.reloadData()
            default:
                break
            }
        }).disposed(by: selectionPopUpVM.disposeBag)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if type == StringConstants.Country(){
            return selectionPopUpVM.CountryList.count
        }
        else{
            return selectionPopUpVM.StateList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: selectionTVCell.self))) as! selectionTVCell
        if type == StringConstants.Country(){
             cell.setCountries(selectionPopUpVM.CountryList[indexPath.row])
        }
        else {
            cell.setStates(selectionPopUpVM.StateList[indexPath.row])
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if type == StringConstants.Country(){
            UserDefaults.standard.setValue(selectionPopUpVM.CountryList[indexPath.row].id, forKey: StringConstants.SelectedCountry())
            responseBack.onNext(selectionPopUpVM.CountryList[indexPath.row])
        }
        else{
            responseBack1.onNext(selectionPopUpVM.StateList[indexPath.row])
        }
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
