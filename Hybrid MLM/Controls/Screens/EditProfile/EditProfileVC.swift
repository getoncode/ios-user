//
//  EditProfileVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/11/20.
//

import UIKit
import DropDown

class EditProfileVC: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var pinLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var changeProfileButton: UIButton!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userImageContainer: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var FirstNameTF: UITextField!
    @IBOutlet weak var LasttNameTF: UITextField!
    @IBOutlet weak var EmailTF: UITextField!
    @IBOutlet weak var PhoneTF: UITextField!
    @IBOutlet weak var editProfileScrollView: UIScrollView!
    @IBOutlet weak var genderButton: UIButton!
    @IBOutlet weak var DateOfBirthTF: UITextField!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var addressTF: FloatingLabelInput!
    @IBOutlet weak var stateButton: UIButton!
    @IBOutlet weak var cityTF: FloatingLabelInput!
    @IBOutlet weak var pinTF: FloatingLabelInput!
    @IBOutlet weak var EditProfileButton: UIButton!
    
    var imagePicker = UIImagePickerController()
    let profileVM = ProfileVM()
    var selectionType = ""
    var ProfileString = ""
    var editStatus : Bool?
    var editProfileVM = EditProfileVM()
    let datePicker = UIDatePicker()
    var genderDropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDatePicker()
    }
    override func viewWillAppear(_ animated: Bool) {
        setUi()
        setUserData()
        setVM()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    
    @IBAction func editProfileClick(_ sender: Any) {
        if EditProfileButton.titleLabel?.text == StringConstants.Edit()
        {
            EditProfileButton.setTitle(StringConstants.Save(), for: .normal)
            setTFInteraction()
        }
        else if EditProfileButton.titleLabel?.text == StringConstants.Save()
        {
            validateFields()
        }
    }
    @IBAction func chooseState(_ sender: Any) {
        selectionType = StringConstants.State()
        performSegue(withIdentifier: String(describing: EditProfileVC.self) + String(describing: SelectionPopUpVC.self), sender: nil)
    }
    
    @IBAction func chooseCountry(_ sender: Any) {
        selectionType = StringConstants.Country()
        performSegue(withIdentifier: String(describing: EditProfileVC.self) + String(describing: SelectionPopUpVC.self), sender: nil)
    }
    //choose profile image
    @IBAction func chooseImage(_ sender: Any) {
        imagePicker.delegate = self
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = false
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            userImageView.image = originalImage
            dismiss(animated: true, completion: nil)
        }
    }
    
    func setDatePicker() {
        //Format Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: StringConstants.Done(), style: .plain, target: self, action: #selector(doneDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: StringConstants.Cancel(), style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        DateOfBirthTF.inputAccessoryView = toolbar
        DateOfBirthTF.inputView = datePicker
        DateOfBirthTF.inputView?.frame = CGRect(x: 0, y: 100, width: 100, height: self.view.frame.height / 3)
    }
    
    @objc func doneDatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        DateOfBirthTF.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    @IBAction func chooseGender(_ sender: Any) {
        genderDropDown.show()
    }
    @IBAction func showNotification(_ sender: Any) {
        let notify = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    
    func setVM() {
        editProfileVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Edit:
                self?.setUserData()
                Helper.showAlert(message: StringConstants.ProfileUpdatedSuccessfully(), head: StringConstants.Success())
                //scroll scrollview to top
                self?.editProfileScrollView.setContentOffset(.zero, animated: true)
                break
            default:
                break
            }
        }).disposed(by: editProfileVM.disposeBag)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: EditProfileVC.self) + String(describing: SelectionPopUpVC.self) {
            if let controller = segue.destination as? SelectionPopUpVC {
                controller.type = selectionType
                //response back for country
                controller.responseBack.subscribe(onNext: { [weak self]data in
                    self?.countryButton.setTitle(data.name, for: .normal)
                    self?.editProfileVM.countryId = data.id
                    self?.stateButton.setTitle("", for: .normal)
                }).disposed(by: controller.disposebag)
                
                //response back for state
                controller.responseBack1.subscribe(onNext: { [weak self]data in
                    self?.editProfileVM.stateId = data.id
                    self?.stateButton.setTitle(data.name, for: .normal)
                }).disposed(by: controller.disposebag)
            }
        }
    }
}

