//
//  SupportHomeUIE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 30/09/21.
//

import Foundation
import UIKit
extension SupportHomeVC{
    func setUi(){
        supportImg.layer.cornerRadius = 100
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 15.0)
        activeticketBtn.layer.cornerRadius = 4.0
    }
}
