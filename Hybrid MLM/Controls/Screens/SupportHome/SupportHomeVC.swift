//
//  SupportHomeVC.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 30/09/21.
//

import UIKit

class SupportHomeVC: UIViewController {

    @IBOutlet weak var supportLbl: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var supportImg: UIImageView!
    @IBOutlet weak var activeTcketLbl: UILabel!
    @IBOutlet weak var ticketDetailsLbl: UILabel!
    @IBOutlet weak var activeticketBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
            setUi()
    }
    
    @IBAction func activeTicktsBtnAction(_ sender: UIButton){
//        let story = UIStoryboard(name: "Main", bundle:nil)
//        let vc = story.instantiateViewController(withIdentifier: "SupportVC") as! SupportVC
//        UIApplication.shared.windows.first?.rootViewController = vc
//        UIApplication.shared.windows.first?.makeKeyAndVisible()
        performSegue(withIdentifier: "SupportVC", sender: nil)
    }
    
    

}
