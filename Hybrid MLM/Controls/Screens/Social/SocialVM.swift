//
//  SocialVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 02/02/21.
//

import Foundation
import RxCocoa
import RxSwift

class SocialVM: NSObject {
    enum typeHere:Int {
        case Default = 0
        case Edit = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var SocialArray: [Social] = []
    var stateId : Int?
    var countryId : Int?
    
    func updateSocialInfo(about: String,fb: String,twt:String, lnkdIn: String, insta:String) {
        let params : [String:Any] = [
            "purpose" : "updateProfile",
            "params[section]" : "social",
            "params[about_me]" : about,
            "params[facebook]" : fb,
            "params[twitter]" : twt,
            "params[linked_in]" : lnkdIn,
            "params[instagram]" : insta
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.SocialArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    for item in [dataIn] {
                            self.SocialArray.append(Social.init(data: item))
                    }
                }
                self.ResposeBack.onNext(typeHere.Edit)
            }
        }).disposed(by: disposeBag)
    }
}
