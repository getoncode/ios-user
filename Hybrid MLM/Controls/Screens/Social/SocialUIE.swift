//
//  SocialUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 28/01/21.
//

import Foundation
import UIKit

extension SocialVC{
    func setUi()  {
        self.navigationController?.navigationBar.isHidden = false
        profileImageOuterView.layer.cornerRadius = 50.0
        profileImageView.layer.cornerRadius = 50.0
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 15.0)
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
    }
    
    func setTFInteraction()
    {
        if aboutMeTF.isUserInteractionEnabled == false
        {
            aboutMeTF.isUserInteractionEnabled = true
            aboutMeTF.becomeFirstResponder()
            FbTF.isUserInteractionEnabled = true
            twitterTF.isUserInteractionEnabled = true
            linkedInTF.isUserInteractionEnabled = true
            instagramTF.isUserInteractionEnabled = true
        }
        else
        {
            aboutMeTF.isUserInteractionEnabled = false
            FbTF.isUserInteractionEnabled = false
            linkedInTF.isUserInteractionEnabled = false
            instagramTF.isUserInteractionEnabled = true
            editButton.setTitle(StringConstants.Edit(), for: .normal)
        }
    }
    
    func setUserData(data: [Social], profile: String) {
        Helper.setImage(image:  APIKeys.baseURLFirst + profile , on: profileImageView)
        aboutMeTF.text = data[0].aboutMe
        FbTF.text = data[0].facebook
        twitterTF.text = data[0].twitter
        linkedInTF.text = data[0].linkedIn
        instagramTF.text = data[0].instagram
    }
}

