//
//  SocialVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 28/01/21.
//

import UIKit
import RxCocoa
import RxSwift

class SocialVC: UIViewController {
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileImageOuterView: UIView!
    @IBOutlet weak var aboutMeTF: UITextField!
    @IBOutlet weak var FbTF: UITextField!
    @IBOutlet weak var twitterTF: UITextField!
    @IBOutlet weak var linkedInTF: UITextField!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var instagramTF: UITextField!
    let socialVM = SocialVM()
    let responseBack = PublishSubject<[Social]>()
    let disposebag = DisposeBag()
    var socialData : [Social]?
    var profileUrl : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        setUserData(data: socialData ?? [], profile: profileUrl ?? "")
        setVM()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    @IBAction func editAction(_ sender: Any) {
        if editButton.titleLabel?.text == StringConstants.Edit()
        {
            editButton.setTitle(StringConstants.Save(), for: .normal)
            setTFInteraction()
        }
        else if editButton.titleLabel?.text == StringConstants.Save()
        {
            if Helper.callRefreshToken(){
                socialVM.updateSocialInfo(about: aboutMeTF.text ?? "",fb: FbTF.text ?? "",twt: twitterTF.text ?? "", lnkdIn: linkedInTF.text ?? "", insta:instagramTF.text ?? "")
            }else{
                Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                    Helper.logout()
                })
            }
        }
    }
    @IBAction func showNotification(_ sender: Any) {
        let notify = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    
    func setVM() {
        socialVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Edit:
                Helper.showAlert(message: StringConstants.SocialInfoUpdated(), head: StringConstants.Success())
                self?.setTFInteraction()
                //scroll scrollview to top
                self?.scrollView.setContentOffset(.zero, animated: true)
                break
            default:
                break
            }
        }).disposed(by: socialVM.disposeBag)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
