//
//  PayOutSuccessUIE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 28/06/21.
//

import Foundation
import UIKit
extension PayOutConfirmationVC{
    func setUi() {
        Helper.setShadow(view:PayoutSuccessOuterVIew, color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.7), radius: 20.0)
        addNoteView.layer.borderWidth = 0.5
        Helper.roundBorder(view: addNoteView, radius: 5.0, borderWidth: 0.7, color: #colorLiteral(red: 0.737254902, green: 0.7725490196, blue: 0.8901960784, alpha: 1))
        addNoteViewData.setLineSpacing()
        Helper.setShadow(view: payOutTable, color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.7), radius: 10.0)
    }
    //add and remove observers
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    //dismiss viewcontroller on tapping outside search view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.view {
            //            let data = UserDefaults.standard.value(forKey: UserDefaultConstants.Recipients)
            //            responseBack.onNext(data)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
