//
//  PayOutSuccessVC.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 28/06/21.
//

import UIKit

class PayOutConfirmationVC: UIViewController{
    
    @IBOutlet weak var PayoutSuccessOuterVIew: UIView!
    @IBOutlet weak var payOutTable: UIView!
    @IBOutlet weak var tiltleWithdrawl:UILabel!
    @IBOutlet weak var titleUnderLine: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var amountDetails: UILabel!
    @IBOutlet weak var walletLbl: UILabel!
    @IBOutlet weak var WalletData: UILabel!
    @IBOutlet weak var addNoteView: UIView!
    @IBOutlet weak var addNoteViewData: UILabel!
    
    var amountDatas:String?
    var walletDetails:String?
    var notesData:String?
    var walletId : Int?
    var payoutConfirmationVm = PayOutConfirmationVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        amountDetails.text = amountDatas
        WalletData.text = walletDetails
        addNoteViewData.text = notesData
        setUi()
        setVM()
    }
    
    @IBAction func ContinueButtonAction(_ sender: UIButton) {
        var str = amountDatas ?? ""
        if str.contains("$"){
            str.remove(at: str.startIndex)
        }
        if Double(str) ?? 0 > 500{
            payoutConfirmationVm.payoutRequest(wallet: walletId ?? 0, amount: Double(str) ?? 0.0)
        }else{
            Helper.showAlert(message: "Requested amount is less than the minimum payout amount (500)", head: "")
        }
    }
    
    //setvm
    func setVM() {
        payoutConfirmationVm.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Request:
                //Helper.showAlert(message: "Payout Requested Successfully", head: "")
                let cont = self?.presentingViewController ?? self
                self!.dismiss(animated: true, completion: {
                    let successVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: String(describing: TransactionSuccessVC.self))
                    UserDefaults.standard.setValue(StringConstants.Request(), forKey: StringConstants.Request())
                    successVC.modalPresentationStyle = .overFullScreen
                    cont?.present(successVC, animated: true, completion: nil)
                })
                break
            default:
                break
            }
        }).disposed(by: payoutConfirmationVm.disposeBag)
    }
    
    
}
