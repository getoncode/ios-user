//
//  PayOutConfirmationVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 26/07/21.
//

import Foundation
import RxCocoa
import RxSwift

class PayOutConfirmationVM{
    enum typeHere: Int{
        case Default = 0
        case Request = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    
    func payoutRequest(wallet: Int, amount: Double) {
        let params : [String:Any] = [
            "purpose" : "requestPayout",
            "params[wallet]" : wallet,
            "params[amount]" : amount
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: "accessToken") as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                  print(dataIn)
                }
                self.ResposeBack.onNext(typeHere.Request)
            }
            
        }).disposed(by: disposeBag)
    }
}
