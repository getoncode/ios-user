//
//  SingUpUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 07/11/20.
//

import Foundation
import UIKit

extension SignUpVC
{
    func setUi() {
        //other UI updates
        sponsorTF.attributedPlaceholder = NSAttributedString(text: StringConstants.SponsorName(), aligment: .left, color:#colorLiteral(red: 0.4235294118, green: 0.5411764706, blue: 0.6823529412, alpha: 1))
        userNameTF.attributedPlaceholder = NSAttributedString(text: StringConstants.UserName(), aligment: .left, color:#colorLiteral(red: 0.4235294118, green: 0.5411764706, blue: 0.6823529412, alpha: 1))
        emailTF.attributedPlaceholder = NSAttributedString(text: StringConstants.Email(), aligment: .left, color:#colorLiteral(red: 0.4235294118, green: 0.5411764706, blue: 0.6823529412, alpha: 1))
        phoneNumberTF.attributedPlaceholder = NSAttributedString(text: StringConstants.PhoneNumber(), aligment: .left, color:#colorLiteral(red: 0.4235294118, green: 0.5411764706, blue: 0.6823529412, alpha: 1))
        sponsorIconImg.layer.cornerRadius = 11.0
        sponsorIconImg.backgroundColor = #colorLiteral(red: 0.6078431373, green: 0.6549019608, blue: 0.8117647059, alpha: 1)
        self.navigationController?.navigationBar.isHidden = false
        if self.navigationController != nil{
            Helper.roundBorderButton(view: singUpButton, radius: 5.0, borderWidth: 0, color: UIColor.clear.cgColor)
            Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)}
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 20.0)
    }
}
