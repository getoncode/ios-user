//
//  SignUpVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 07/11/20.
//
import UIKit

class SignUpVC: UIViewController {
    
    @IBOutlet weak var sponsorIconImg: UIImageView!
    @IBOutlet weak var sponsorView: UIView!
    @IBOutlet weak var sponsorTF: UITextField!
    @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var singUpButton: UIButton!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var signinButton: UIButton!
    let signUpVM = SignUpVM()
    var loginVM = LoginVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        //temporary
        loginVM.getAccessToken("admin@email.com", "admin")
        setUi()
        setVM()
    }
    //set signin button for networkvc
    override func viewWillAppear(_ animated: Bool) {
        let prvsVC = NSStringFromClass(Helper.previousController(vc: self).classForCoder)
        if prvsVC == NSStringFromClass(NetworkVC().classForCoder){
            signinButton.isHidden = true
        }else{
            signinButton.isHidden = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    @IBAction func signUpAction(_ sender: Any) {
        signUpVM.registerUser(sponsor: sponsorTF.text ?? "", username: userNameTF.text ?? "", email: emailTF.text ?? "", phone: phoneNumberTF.text ?? "")
    }
    
    func setVM() {
        signUpVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Register:
                Helper.showAlert(message: StringConstants.SuccessfullyRegistered(), head: StringConstants.Success())
                self?.navigationController?.popViewController(animated: true)
                break
            default:
                break
            }
        }).disposed(by: signUpVM.disposeBag)
    }
    
    @IBAction func singinButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
