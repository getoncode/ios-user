//
//  SignUpVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 07/11/20.
//

import Foundation
import RxCocoa
import RxSwift
import RxAlamofire
import Alamofire

class SignUpVM : NSObject{
    enum typeHere :Int {
        case Default = 0
        case Register = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var RegistrationArray: [Register] = []
    
    func registerUser(sponsor: String,username: String, email: String,phone: String ) {
        let params : [String:Any] = [
            "purpose" : "register",
            "params[sponsor]" : sponsor,
            "params[username]" : username,
            "params[email]" : email,
            "params[phone]" : phone
        ]
        let headers = ["Authorization" : "Bearer \(String(describing: UserDefaults.standard.value(forKey: StringConstants.accessToken())!))"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.RegistrationArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    if let array = dataIn as? [String:Any] {
                        for item in [array] {
                            self.RegistrationArray.append(Register.init(data: item["personal"] as? [String : Any] ?? [:]))
                            Utility.saveuserData(value: item)
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.Register)
            }
        }).disposed(by: disposeBag)
    }
    
    
}
