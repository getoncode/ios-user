//
//  NotificationTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 27/11/20.
//

import UIKit

class NotificationTVCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    //set data after api call
    func setData(_ data: Notification) {
        titleLabel.text = data.dataAry["subject"] as? String
        dateLabel.text = Helper.dateToString(date: data.created_at, format: "dd MMM yyyy")
        }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

