//
//  NotificationTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/01/21.
//

import Foundation
import UIKit

extension NotificationVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationVM.notificationArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: NotificationTVCell.self))) as! NotificationTVCell
        cell.setData(notificationVM.notificationArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        passIndex = indexPath.row
        performSegue(withIdentifier: String(describing: NotificationVC.self)+String(describing: NotificationDetailVC.self), sender: nil)
    }
}
