//
//  NotificationUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 27/11/20.
//

import Foundation
import UIKit

extension NotificationVC
{
    func setUi() {
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 10.0)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
}

