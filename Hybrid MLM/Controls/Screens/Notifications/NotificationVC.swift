//
//  NotificationVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 27/11/20.
//

import UIKit

class NotificationVC: UIViewController , UITableViewDelegate, UITableViewDataSource{
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var notificationTable: UITableView!
    let notificationVM = NotificationVM()
    var passIndex : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        if Helper.callRefreshToken(){
            notificationVM.getNotifications()
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        setVM()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
    }
       
    func setVM() {
        notificationVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Notification:
                self?.notificationTable.reloadData()
                break
            default:
                break
            }
        }).disposed(by: notificationVM.disposeBag)
    }

  
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: NotificationVC.self)+String(describing: NotificationDetailVC.self)
        {
            if let controller = segue.destination as? NotificationDetailVC {
                controller.data = notificationVM.notificationArray[passIndex ?? 0]
            }
        }
    }
   

}
