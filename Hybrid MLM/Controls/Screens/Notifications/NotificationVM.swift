//
//  NotificationVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 27/11/20.
//

import Foundation
import RxCocoa
import RxSwift

class NotificationVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case Notification = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var notificationArray : [Notification] = []
   
    func getNotifications() {
        let params : [String:Any] = [
            "purpose" : "getNotifications"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in [dataIn] {
                        for itm in item{
                        self.notificationArray.append(Notification.init(data: itm))
                        }
                    }
                }
                self.notificationArray.sort(by: {$0.created_at.compare($1.created_at) == .orderedDescending})
                self.ResposeBack.onNext(typeHere.Notification)
            }
            
        }).disposed(by: disposeBag)
    }
}


