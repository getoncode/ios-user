//
//  LoginVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 06/11/20.
//

import Foundation
import RxCocoa
import RxSwift

class LoginVM: NSObject {
    enum typeHere:Int {
        case Default = 0
        case Login = 1
        case Refresh = 2
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var TokenArray: [Token] = []
    
    func getAccessToken(_ username:String, _ password:String) {
        let params : [String:Any] = [
            "grant_type" : "password",
            "client_id" : "2",
            "client_secret" : "LW2EwO9OTKq4ErMRXYbjAUu0g8kbuI8cd9MV4OVF",
            "username" : username,
            "password" : password,
            "scope" : ""
        ]
        APIModel.apiTokenCall(method: .post, api: "http://localhost/blueprint/app/public/oauth/token", params: params, header: [:]).subscribe(onNext: { data in
            self.TokenArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    if let array = dataIn as? [String:Any] {
                        for item in [array] {
                            self.TokenArray.append(Token.init(data: item))
                            self.saveData(self.TokenArray[0])
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.Login)
            }
            
        }).disposed(by: disposeBag)
    }
    
    func refreshToken(_ token:String) {
        let params : [String:Any] = [
            "grant_type" : "refresh_token",
            "refresh_token" : token,
            "client_id" : "2",
            "client_secret" : "LW2EwO9OTKq4ErMRXYbjAUu0g8kbuI8cd9MV4OVF",
            "scope" : ""
        ]
        APIModel.apiTokenCall(method: .post, api: "http://localhost/blueprint/app/public/oauth/token", params: params, header: [:]).subscribe(onNext: { data in
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    if let array = dataIn as? [String:Any] {
                        for item in [array] {
                            UserDefaults.standard.setValue(item["refresh_token"] ?? "", forKey: StringConstants.refreshToken())
                            UserDefaults.standard.setValue(item["access_token"] ?? "", forKey: StringConstants.accessToken())
                            self.setExpiry(secs: item["expires_in"] as! Double)
                            
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.Refresh)
            }
            
        }).disposed(by: disposeBag)
    }
    
    func setExpiry(secs: Double) {
        //set expiry time
        let millisecs = secs
        let secs = Helper.converter(milliSeconds: millisecs)
        UserDefaults.standard.setValue(Helper.extendExpiryTime(secs: secs), forKey: StringConstants.Expiry())
    }
    
    func saveData(_ data : Token) {
        UserDefaults.standard.setValue(data.refreshToken, forKey: StringConstants.refreshToken())
        UserDefaults.standard.setValue(data.accessToken, forKey: StringConstants.accessToken())
    }
    
}

