//
//  LoginUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 06/11/20.
//

import Foundation
import UIKit
import AuthenticationServices

extension LoginViewController
{
    func setUi() {
        //sicial network button ui
        appleButtonContainer.layer.cornerRadius = 5.0
        Helper.roundBorderButton(view: googleButton, radius: 5.0, borderWidth: 0.7, color: #colorLiteral(red: 0.8039215686, green: 0.8039215686, blue: 0.8039215686, alpha: 1))
        appleButtonContainer.layer.cornerRadius = 5.0
        appleButtonContainer.layer.borderWidth = 0.7
        appleButtonContainer.layer.borderColor = #colorLiteral(red: 0.8039215686, green: 0.8039215686, blue: 0.8039215686, alpha: 1)
        facebookButton.layer.cornerRadius = 5.0
        twitterButton.layer.cornerRadius = 5.0
        
        //other ui settings
        navigationController?.navigationBar.tintColor = UIColor.darkGray
        navigationController?.navigationBar.barTintColor = UIColor.white
        Helper.roundBorder(view: userDataview, radius: 5.0, borderWidth: 0.7, color: #colorLiteral(red: 0.737254902, green: 0.7725490196, blue: 0.8901960784, alpha: 1))
        emailTextfield.attributedPlaceholder = NSAttributedString(text: StringConstants.Your() + StringConstants.Email(), aligment: .left, color:#colorLiteral(red: 0.4235294118, green: 0.5411764706, blue: 0.6823529412, alpha: 1))
        passwordTextField.attributedPlaceholder = NSAttributedString(text: StringConstants.Password(), aligment: .left, color:#colorLiteral(red: 0.4235294118, green: 0.5411764706, blue: 0.6823529412, alpha: 1))
        Helper.roundBorderButton(view: singinButton, radius: 5.0, borderWidth: 0, color: UIColor.clear.cgColor)
        self.navigationController?.navigationBar.isHidden = true
            
        //add apple button
        let appleButton = ASAuthorizationAppleIDButton()
        appleButton.frame = appleButtonContainer.frame
        self.appleButtonContainer.addSubview(appleButton)
        appleButton.addTarget(self, action: #selector(signinWithAppleID), for: .touchUpInside)
        
        
    }
    //open apple login page
    @objc func signinWithAppleID(sender: ASAuthorizationAppleIDButton)
    {
        AppleProvider.handleAppleIdRequest(block: {fullName, email, token in
            
        })
    }
    
}


