//
//  LoginViewController.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 03/11/20.
//

import UIKit
import AuthenticationServices
import FBSDKLoginKit
import GoogleSignIn
import TwitterKit
class LoginViewController: UIViewController, AppleIdSignInProtocol,LoginButtonDelegate, GIDSignInDelegate {
   
    @IBOutlet var facebookButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var appleButtonContainer: UIView!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var loginScrollView: UIScrollView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var loginImageView: UIImageView!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userDataview: UIView!
    @IBOutlet weak var userDataViewHeight: NSLayoutConstraint!
    @IBOutlet weak var singinButton: UIButton!
    
    var signinDelegate : AppleIdSignInProtocol?
    let AppleProvider = AppleSignInClient()
    let loginVM = LoginVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        //fb token
        if let token = AccessToken.current,
           !token.isExpired {
            //expired
        }else{
            //active
        }
        //google login
        GIDSignIn.sharedInstance().delegate = self
        setVM()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUi()
    }
    
    //for apple login
    func setLoginData(data: NSDictionary) {
        //add
    }
    
    //keyboard handling
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    //fb login
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        print("fb login error..........")
    }
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        print("fb logout......")
    }
    //google
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil{
           print("google signin error")
        }
    }
    
    //sing in action
    @IBAction func singInButtonClick(_ sender: Any) {
        if Helper.isValidEmail(text: emailTextfield.text ?? "")
        {
            if passwordTextField.text == ""{
                Helper.showAlert(message: StringConstants.EnterYourPassword(), head: StringConstants.Note())
            }else
            {
                loginVM.getAccessToken(emailTextfield.text!, passwordTextField.text!)
            }} else{
                Helper.showAlert(message: StringConstants.EnteraValidEmail(), head: StringConstants.Note())
        }
    }
    @IBAction func SignupButtonClick(_ sender: Any) {
        performSegue(withIdentifier: String(describing: LoginViewController.self) + String(describing: SignUpVC.self), sender: self)
    }
    @IBAction func fbButtonClick(_ sender: Any) {
        getFacebookUserInfo()
    }
    @IBAction func googleLogin(_ sender: Any) {
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    func getFacebookUserInfo(){
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email ], viewController: self) { (result) in
            switch result{
            case .cancelled:
                print("Cancel button click")
            case .success:
                let params = ["fields" : "id, name, first_name, last_name, picture.type(large), email "]
                let graphRequest = GraphRequest.init(graphPath: "/me", parameters: params)
                let Connection = GraphRequestConnection()
                Connection.add(graphRequest) { (Connection, result, error) in
                    let info = result as! [String : AnyObject]
                    print(info["name"] as! String)
                }
                Connection.start()
            default:
                print("??")
            }
        }
    }
    @IBAction func twitterButtonClick(_ sender: UIButton) {
        TWTRTwitter.sharedInstance().logIn(with: self, completion: { (session, error) in
                    if let sess = session {
                        let client = TWTRAPIClient()
                        client.loadUser(withID: sess.userID) { (user, err) in
                            print("items")
                        }
                    }
                    else {
                        print("error: \(String(describing: error?.localizedDescription))");
                    }
            })
    }
    //setVM
    func setVM() {
        loginVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Login:
                //set expiry time
                let millisecs = Double(self?.loginVM.TokenArray[0].expires ?? 60.0)
                let secs = Helper.converter(milliSeconds: millisecs)
                UserDefaults.standard.setValue(Helper.extendExpiryTime(secs: secs), forKey: StringConstants.Expiry())
                Utility.setLogin(value: true)
                
                // present HomeVC
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let tabBar = storyboard.instantiateViewController(withIdentifier: "TabBarController")
                tabBar.modalPresentationStyle = .fullScreen
                self?.present(tabBar, animated: true, completion: nil)
                // self?.loginVM.refreshToken(self?.loginVM.TokenArray[0].refreshToken ?? "")
                break
            case .Refresh:
                //set expiry time
                let millisecs = Double(self?.loginVM.TokenArray[0].expires ?? 60.0)
                let secs = Helper.converter(milliSeconds: millisecs)
                UserDefaults.standard.setValue(Helper.extendExpiryTime(secs: secs), forKey: StringConstants.Expiry())
                Utility.setLogin(value: true)
                break
            default:
                break
            }
        }).disposed(by: loginVM.disposeBag)
    }
}
