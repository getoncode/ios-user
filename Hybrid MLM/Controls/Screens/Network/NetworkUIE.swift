//
//  NetworkUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 06/02/21.
//

import Foundation
import UIKit

extension NetworkVC{
    func setUi() {
        selectionView.isHidden = true
        Helper.roundBorder(view: typeView, radius: 5.0, borderWidth: 0.5, color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        webView.isHidden = true
    }
    //add and remove observers
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == typeTF{
            selectionView.isHidden = false
            textField.isEnabled = false
        }
    }
}
