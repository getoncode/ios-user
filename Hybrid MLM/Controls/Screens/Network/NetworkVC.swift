//
//  NetworkVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 06/02/21.
//

import UIKit
import WebKit

class NetworkVC: UIViewController, UIWebViewDelegate, WKNavigationDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var selectionView: UIStackView!
    @IBOutlet weak var typeView: UIView!
    @IBOutlet weak var sponsorButton: UIButton!
    @IBOutlet weak var typeTF: UITextField!
    @IBOutlet weak var placementButton: UIButton!
    @IBOutlet weak var webView: WKWebView!
    var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setActivityIndicator()
        setUi()
        if Helper.callRefreshToken(){
            loadWebView(type: typeTF.text!.lowercased())
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
    }
   func setActivityIndicator(){
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.isHidden = true
        activityIndicator.tintColor = Colors.AppBaseColor
        
        view.addSubview(activityIndicator)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
           Helper.hidePI()
           activityIndicator.isHidden = false
           activityIndicator.startAnimating()
       }
    
    //refresh webview on each appearance of viewcontroller
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Helper.showPI(string: "")
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: true)
        if Helper.callRefreshToken(){
            loadWebView(type: typeTF.text!.lowercased())
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        if Helper.callRefreshToken(){
            loadWebView(type: typeTF.text!.lowercased())
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
    }
    
    //url call for webview
    func loadWebView(type:String)
    {
        Helper.hidePI()
        let url = URL(string: APIModel.APIkeys.baseURL)!
        var request = URLRequest(url: url)
        request.addValue(StringConstants.applicationurlencoded(), forHTTPHeaderField: StringConstants.ContentType())
        request.httpMethod = StringConstants.POST()
        request.addValue(StringConstants.Bearer() + String(describing: UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String), forHTTPHeaderField: StringConstants.Authorization())
        let bodyData = StringConstants.getGenealogyViewparams() + "\(type)"
        request.httpBody = bodyData.data(using: .utf8)!
        webView.navigationDelegate = self
        webView.load(request)
        webView.allowsBackForwardNavigationGestures = true
    }
    //unhide webview after loading content
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.isHidden = false
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    //handle selectionType of GenealogyView
    @IBAction func selectType(_ sender: UIButton) {
        if sender == placementButton{
            typeTF.text = StringConstants.Placement()
        }
        else{
            typeTF.text = StringConstants.Sponsor()
        }
        webView.isHidden = true
        if Helper.callRefreshToken(){
            loadWebView(type: typeTF.text!.lowercased())
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        typeTF.isEnabled = true
        selectionView.isHidden = true
    }
    
    
    //handle redirection from webview to registration screen
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
        guard let urlAsString = navigationAction.request.url?.absoluteString.lowercased() else {
            return
        }
        if urlAsString.contains(UrlStrings.register()){
            let vc = storyboard?.instantiateViewController(identifier: String(describing: SignUpVC.self)) as! SignUpVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func showNotification(_ sender: Any) {
        let notify = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
}
