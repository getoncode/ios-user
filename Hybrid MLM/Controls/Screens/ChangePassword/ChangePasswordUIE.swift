//
//  ChangePasswordUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 17/11/20.
//

import Foundation
import UIKit

extension ChangePasswordVC
{
    func setUi() {
        //show navigationBar
        self.navigationController?.navigationBar.isHidden = false
        
        //set corner radius for textfields
        changePasswordButton.layer.cornerRadius = 5.0
        pointOneView.layer.cornerRadius = 5.0
        pointTwoView.layer.cornerRadius = 5.0
        pointThreeView.layer.cornerRadius = 5.0
        pointFourView.layer.cornerRadius = 5.0
        
        validationView.isHidden = true
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 15.0)
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
        
    }
   
    func setActive(view:UIView) {
        view.backgroundColor = #colorLiteral(red: 0, green: 0.5607843137, blue: 0, alpha: 1)
    }
    func setInactive(view:UIView) {
        view.backgroundColor = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8, alpha: 1)
    }
}
