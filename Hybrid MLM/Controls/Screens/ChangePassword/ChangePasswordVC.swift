//
//  ChangePasswordVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 17/11/20.
//

import UIKit

class ChangePasswordVC: UIViewController {
    @IBOutlet weak var currentPasswordView: UIView!
    @IBOutlet weak var currentPasswordTF: UITextField!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var pointOneView: UIView!
    @IBOutlet weak var pointTwoView: UIView!
    @IBOutlet weak var pointFourView: UIView!
    @IBOutlet weak var validationView: UIView!
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var pointThreeView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    @IBAction func showValidationView(_ sender: Any) {
        validationView.isHidden = false
    }
    @IBAction func startValidatingPassword(_ sender: Any)  {
        if newPasswordTF.text != ""
        {
            let pass = newPasswordTF.text!
            if Helper.validateUppercase(password: pass){
                setActive(view: pointOneView)
            }else{
                setInactive(view: pointOneView)
            }
            if Helper.validateNumeric(password: pass){
                setActive(view: pointTwoView)
            }else{
                setInactive(view: pointTwoView)
            }
            if Helper.validateSpecialCharacters(password: pass){
                setActive(view: pointThreeView)
            }else{
                setInactive(view: pointThreeView)
            }
            if pass.count >= 6{
                setActive(view: pointFourView)
            }else{
                setInactive(view: pointFourView)
            }
            
        }
        else
        {
            setInactive(view: pointOneView)
        }
        
    }
    @IBAction func showNotification(_ sender: Any) {
        let notify = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    
   
    
    
}
/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


