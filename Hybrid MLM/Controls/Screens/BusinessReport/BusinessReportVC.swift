//
//  BusinessReportVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 28/12/20.
//

import UIKit

class BusinessReportVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var dataCollectionView: UIView!
    @IBOutlet weak var dataCollection: UICollectionView!
    @IBOutlet weak var buttton1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var buble1: UIButton!
    @IBOutlet weak var bubble2: UIButton!
    @IBOutlet weak var bubble3: UIButton!
    @IBOutlet weak var finalButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
