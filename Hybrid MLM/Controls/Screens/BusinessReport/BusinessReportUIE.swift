//
//  BusinessReportUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 28/12/20.
//

import Foundation
import UIKit

extension BusinessReportVC
{
    func setUi()  {
        dataCollectionView.layer.cornerRadius = 10.0
        dataCollection.layer.cornerRadius = 10.0
        filterButton.layer.cornerRadius = 2.0
        filterButton.layer.borderWidth = 1
        filterButton.layer.borderColor = UIColor.lightGray.cgColor
        Helper.setShadow(view: dataCollectionView, color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.7), radius: 20.0)
        Helper.roundBorder(view: buttton1, radius: 16.5, borderWidth: 1.2, color: UIColor.lightGray.cgColor)
        Helper.roundBorder(view: button2, radius: 16.5, borderWidth: 1.2, color: UIColor.darkGray.cgColor)
        Helper.roundBorder(view: button3, radius: 16.5, borderWidth: 1.2, color: UIColor.darkGray.cgColor)
        Helper.roundBorder(view: finalButton, radius: 16.5, borderWidth: 1.2, color: UIColor.darkGray.cgColor)
        Helper.setGradient(view: buble1,color1:UIColor.lightGray, color2:#colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1) ,corner: 11)
        Helper.setGradient(view: bubble2,color1:UIColor.lightGray, color2:#colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1) ,corner: 11)
        Helper.setGradient(view: bubble3,color1:UIColor.lightGray, color2:#colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1) ,corner: 11)
        
    }
}
