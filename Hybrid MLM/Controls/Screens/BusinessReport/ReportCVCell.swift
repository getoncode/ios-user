//
//  ReportCVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 29/12/20.
//

import UIKit

class ReportCVCell: UICollectionViewCell {
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 0.5
        self.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
    
}
