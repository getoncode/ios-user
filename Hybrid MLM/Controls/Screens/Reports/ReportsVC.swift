//
//  ReportsVC.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 22/04/21.
//

import UIKit

class ReportsVC: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var ReportTableView: UITableView!
    @IBOutlet weak var lblReport: UILabel!
    var imageArray = [UIImage(named: "payout"),UIImage(named: "cornerLeft"),UIImage(named: "Activities")]
    var titleArray = ["Commission Report","PayOut","MyActivities"]
    var SubTitleArray = ["Report That Shows Earnings","Withdrawl Statement","MyActivities"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func showNotifications(_ sender: UIBarButtonItem) {
        let notify = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
