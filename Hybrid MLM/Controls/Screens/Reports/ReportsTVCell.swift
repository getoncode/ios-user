//
//  ReportsTVCell.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 22/04/21.
//

import UIKit

class ReportsTVCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var ContainerView: UIView!
    @IBOutlet weak var ReportOuterView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubDetails: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        ReportOuterView.layer.cornerRadius = 10.0
        Helper.setShadow(view: ReportOuterView, color: Colors.ShadowColor, radius: 2.5)
    }

   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
