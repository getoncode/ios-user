//
//  ReportsTVE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 22/04/21.
//

import Foundation
import UIKit
extension ReportsVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    @objc func btnCheck(_ sender: UIButton) {
        let tag = sender.tag
        let indexPath = IndexPath(row: tag, section: 0)
        switch tag {
        case 0:
            let notify = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: String(describing: EarningReportVC.self))
            navigationController?.pushViewController(notify, animated: true)
        case 1:
            let report = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: String(describing: PayoutHistoryVC.self))
            navigationController?.pushViewController(report, animated: true)
        default:
            let activity = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: String(describing: MyActivityVC.self))
            navigationController?.pushViewController(activity, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ReportTableView.dequeueReusableCell(withIdentifier: "ReportsCell", for: indexPath) as! ReportsTVCell
        cell.lblTitle.text = titleArray[indexPath.row]
        cell.lblSubDetails.text = SubTitleArray[indexPath.row]
        cell.imgView.image = imageArray[indexPath.row]
        cell.btnCheck.tag = indexPath.row
        cell.btnCheck.addTarget(self, action: #selector(self.btnCheck(_:)), for: .touchUpInside)
     
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    

}
