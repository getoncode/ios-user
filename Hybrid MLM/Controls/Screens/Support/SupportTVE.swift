//
//  SupportTVE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 29/09/21.
//

import Foundation
import UIKit
extension SupportVC{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
                //Return the amount of items
                return 8
            } else if section == 1 {
                //Return the Loading cell
                return 1
            } else {
                //Return nothing
                return 0
            }    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = (supportTable.dequeueReusableCell(withIdentifier: String(describing: SupportTVCell.self))) as! SupportTVCell
            return cell
            } else {
            let cell = (supportTable.dequeueReusableCell(withIdentifier: String(describing: SupportLoadingTVCell.self))) as! SupportLoadingTVCell
            cell.activityIndicator.startAnimating()
            return cell
                }
 
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
                return 150 //Item Cell height
            } else {
                return 160//Loading Cell height
            }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            let offsetY = scrollView.contentOffset.y
            let contentHeight = scrollView.contentSize.height

            if (offsetY > contentHeight - scrollView.frame.height * 4) && !isLoading {
                loadMoreData()
            }
        }
    func loadMoreData() {
        if !self.isLoading {
            self.isLoading = true
            DispatchQueue.global().async {
                // Fake background loading task for 2 seconds
                sleep(2)
                // Download more data here
                DispatchQueue.main.async {
                    self.supportTable.reloadData()
                    self.isLoading = false
                }
            }
        }
    }
    func setTableHeight() {
        var height : CGFloat = 0.0
        height = CGFloat(80.0 * Double(8))
        if  height ?? 0 > self.view.frame.height - 100
        {
            if notchDevice.contains(modelName)
            {
                outerViewConstraint.constant = self.view.frame.height - 100
            }
            else
            {
                outerViewConstraint.constant = self.view.frame.height - 50
            }
        }
        else
        {
            outerViewConstraint.constant = height + 20
        }
    }
}
