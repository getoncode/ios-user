//
//  SupportTVCell.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 29/09/21.
//

import UIKit

class SupportTVCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priorityBtn: UIButton!
    @IBOutlet weak var usrdescriptionLbl: UILabel!
    @IBOutlet weak var departmentLbl: UIButton!
    @IBOutlet weak var timestampLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        //btn add
        priorityBtn.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
      // Configure the view for the selected state
    }

}
