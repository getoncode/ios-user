//
//  SupportUIE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 29/09/21.
//

import Foundation
extension SupportVC{
    func setUi() {
    Helper.setShadow(view: supportOuterView, color: Colors.ShadowColor, radius: 15.0)
    supportOuterView.layer.cornerRadius = 8.0
    supportTable.layer.cornerRadius = 8.0
    addTicketBtn.layer.cornerRadius = 4.0
    }
}
