//
//  SupportLoadingTVCell.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 05/10/21.
//

import UIKit

class SupportLoadingTVCell: UITableViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
