//
//  TwoFactorAuthenticationVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 17/11/20.
//

import UIKit

class TwoFactorAuthenticationVC: UIViewController {

    @IBOutlet weak var outerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
       setUi()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    @IBAction func showNotification(_ sender: Any) {
        let notify = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
