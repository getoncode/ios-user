//
//  TwoFactorAuthUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 17/11/20.
//

import Foundation
import UIKit

extension TwoFactorAuthenticationVC
{
    func setUi()  {
        //shw navigation bar
        self.navigationController?.navigationBar.isHidden = false
       // Helper.roundBorder(view: outerView, radius: 5.0, borderWidth: 1.0, color: Colors.AppBorderColor.cgColor)
        
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 15.0)
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
    }
}
