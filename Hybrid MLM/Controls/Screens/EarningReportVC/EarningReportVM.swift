//
//  EarningReportVM.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 10/08/21.
//

import Foundation
import RxCocoa
import RxSwift

class EarningReportVM : NSObject{
    enum typeHere:Int   {
        case Default = 0
        case Earnings = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var earningArray : [Earning] = []
    func getEarningReport() {
        let params : [String:Any] = [
            "purpose" : "earningReportData"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: "accessToken") as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.earningArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in [dataIn] {
                        for itm in item{
                            self.earningArray.append(Earning.init(data: itm))
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.Earnings)
            }
        }).disposed(by: disposeBag)
    }
    
    func getEarningFilterReport(filterArray:[String:Any]) {
         let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: "accessToken") as! String)"]
         APIModel.apiCall(method: .post, api: "", params: filterArray, header: headers).subscribe(onNext: { data in
            self.earningArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in [dataIn] {
                        for itm in item{
                            self.earningArray.append(Earning.init(data: itm))
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.Earnings)
            }
        }).disposed(by: disposeBag)
    }
    func sortingFilterReport(sortBy:[String:Any]) {
         let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: "accessToken") as! String)"]
         APIModel.apiCall(method: .post, api: "", params: sortBy, header: headers).subscribe(onNext: { data in
            self.earningArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in [dataIn] {
                        for itm in item{
                            self.earningArray.append(Earning.init(data: itm))
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.Earnings)
            }
        }).disposed(by: disposeBag)
    }
}
