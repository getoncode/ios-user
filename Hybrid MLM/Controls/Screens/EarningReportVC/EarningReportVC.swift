//
//  EarningReportVC.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 10/08/21.
//

//
//  EarningReportVC.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 10/08/21.
//
import UIKit

class EarningReportVC: UIViewController,UITableViewDelegate,UITableViewDataSource, UIAdaptivePresentationControllerDelegate {
    
    @IBOutlet weak var earningReportTable: UITableView!
    @IBOutlet weak var outerViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var outerView: UIView!
    var reportArray : [Earning] = []
    let modelName = UIDevice.modelName
    var notchDevice : [String] = []
    let earningReportVM = EarningReportVM()
    let reportfiltrVM = ReportFilterVM()
    var passIndex : Int?
    var filtr:[String:Any] = [:]
    var sort:[String:Any] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        if Helper.callRefreshToken(){
            self.earningReportVM.getEarningReport()
            
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        setVM()
        earningReportTable.estimatedRowHeight = 140
        setUI()
    }
    
    func setVM() {
        earningReportVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Earnings:
                self?.earningReportTable.reloadData()
                self?.setTableHeight()
                break
            default:
                break
            }
        }).disposed(by: earningReportVM.disposeBag)
    }
    
    @IBAction func btnCommission(_ sender: Any) {
        performSegue(withIdentifier: String(describing: EarningReportVC.self)+String(describing: ReportFilterVC.self), sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //refresh after choosing filter values from Filter vc
    if segue.identifier == String(describing: EarningReportVC.self)+String(describing: ReportFilterVC.self){
    if let controller = segue.destination as? ReportFilterVC{
           controller.responseBack.subscribe(onNext: { [weak self]data in
           controller.filterArray = data as? [String : Any] ?? [:]
             self?.filtr = controller.filterArray
                print(self?.filtr as Any)
              self?.earningReportVM.getEarningFilterReport(filterArray:self?.filtr ?? [:])
              self?.earningReportTable.reloadData()
    }).disposed(by: controller.disposebag)
        }
        if let controller = segue.destination as? ReportFilterVC{
               controller.responseBack2.subscribe(onNext: { [weak self]data in
                controller.sortBy = data as? [String : Any] ?? [:]
            self?.sort = controller.sortBy
                 print(self?.sort as Any)
            self?.earningReportVM.sortingFilterReport(sortBy: self?.sort ?? [:])
         self?.earningReportTable.reloadData()
         }).disposed(by: controller.disposebag)
         }
        
        }
    }
}
