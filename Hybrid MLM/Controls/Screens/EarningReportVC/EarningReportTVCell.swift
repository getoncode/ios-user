//
//  EarningReportTVCell.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 10/08/21.
//

import UIKit

class EarningReportTVCell: UITableViewCell {
    
    @IBOutlet weak var commissionName: UILabel!
    @IBOutlet weak var walletName: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var transactionId: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setData(_ data : Earning) {
        let wallet = data.operation["to_wallet"] as? String
        walletName.text = wallet
        date.text = Helper.dateToString(date: data.created_at, format: "dd MMM yyyy")
        commissionName.text = data.commissionType
        transactionId.text   = "#\(data.transaction_id)"
        let amountValues = data.transaction["amount"] as? Double
        let round = Double(round(100*amountValues!)/100)
        amount.text = "$ \(round)"
        
        
    }
}
