//
//  EarningReportUIE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 10/08/21.
//

import Foundation
import UIKit
extension EarningReportVC{
    
    func setUI(){
        Helper.setShadow(view: outerView, color:#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.7), radius: 20.0)
        outerView.layer.cornerRadius = 20.0
        earningReportTable.layer.cornerRadius = 8.0
        setTableHeight()
    }
}
