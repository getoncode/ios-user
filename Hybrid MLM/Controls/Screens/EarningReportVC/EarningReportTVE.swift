//
//  EarningReportTVE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 10/08/21.
//

import Foundation
import UIKit
extension EarningReportVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return earningReportVM.earningArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = earningReportTable.dequeueReusableCell(withIdentifier: "EarningReportVC", for: indexPath) as! EarningReportTVCell
        cell.setData(earningReportVM.earningArray[indexPath.row])
        if (indexPath.row == self.earningReportVM.earningArray.count-1) {
            cell.seperatorView.isHidden = true
        }else{
            cell.seperatorView.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func setTableHeight() {
        var height : CGFloat = 0.0
        height = CGFloat(120.0 * Double(earningReportVM.earningArray.count))
        if  height > self.view.frame.height - 100
        {
            if notchDevice.contains(modelName)
            {
                outerViewConstraint.constant = self.view.frame.height - 100
            }
            else
            {
                outerViewConstraint.constant = self.view.frame.height - 50
            }
        }
        else
        {
            outerViewConstraint.constant = height + 20
        }
    }
}
