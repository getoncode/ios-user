//
//  Terms&ConditionsVC.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 23/09/21.
//

import UIKit

class TermsConditionsVC: UIViewController {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var termsTxtView: UIView!
    @IBOutlet weak var termsConditionslbl: UILabel!
    @IBOutlet weak var termsTxtViewlbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
    }

}
