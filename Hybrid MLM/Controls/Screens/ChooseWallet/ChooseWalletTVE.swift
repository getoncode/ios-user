//
//  ChooseWalletTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 27/03/21.
//

import Foundation
import UIKit

extension ChooseWalletVC{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: ChooseWalletTVCell.self))) as! ChooseWalletTVCell
        cell.layer.cornerRadius = 8.0
        tableView.rowHeight = 70
        cell.setData(dataArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        responseBack.onNext(dataArray[indexPath.row].name)
        responseBack1.onNext(String(describing:dataArray[indexPath.row].id))
        self.dismiss(animated: true, completion: nil)
    }
}
