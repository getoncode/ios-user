//
//  ChooseWalletTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 27/03/21.
//

import UIKit

class ChooseWalletTVCell: UITableViewCell {
    @IBOutlet weak var walletNameLabel: UILabel!
    @IBOutlet weak var cardImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //set data after api call
    func setData(_ data: Transfer) {
        walletNameLabel.text = data.name
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
