//
//  ChooseWalletVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 27/03/21.
//

import UIKit
import RxCocoa
import RxSwift

class ChooseWalletVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var walletTable: UITableView!
    let sendMoneyVM = SendMoneyVM()
    var dataArray : [Transfer] = []
    let searchRecipientVM = SearchRecipientVM()
    let responseBack = PublishSubject<String>()
    let responseBack1 = PublishSubject<String>()
    let disposebag = DisposeBag()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
