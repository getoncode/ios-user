//
//  SettingsVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 29/03/21.
//

import Foundation
import RxCocoa
import RxSwift

class SettingsVM: NSObject{
    enum typeHere:Int {
        case Default = 0
        case Password = 1
        case ChangeSettings = 2
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var SettingsArray: [Settings] = []
    
    func eWalletSettings() {
        let params : [String:Any] = [
            "purpose" : "ewalletSettings"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    for item in [dataIn] {
                        self.SettingsArray.append(Settings.init(data: item))
                    }
                }
                self.ResposeBack.onNext(typeHere.Password)
            }
            
        }).disposed(by: disposeBag)
    }
    func ChangeSetting(ip:String,old:String,new:String,confirm:String) {
        let params : [String:Any] = [
            "purpose" : "saveEwalletSettings",
            "params[password]" : new,
            "params[password_confirmation]" : confirm,
            "params[old_password]" : old,
            "params[ip]" : ip
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                self.ResposeBack.onNext(typeHere.ChangeSettings)
            }
            
        }).disposed(by: disposeBag)
    }
}

