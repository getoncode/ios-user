//
//  SettingsVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 29/03/21.
//

import UIKit

class SettingsVC: UIViewController, UITextViewDelegate {
    @IBOutlet weak var currentPasswordView: UIView!
    @IBOutlet weak var currentPasswordTF: UITextField!
    @IBOutlet weak var ipTextView: UITextView!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var confirmPasswordTf: UITextField!
    @IBOutlet weak var addIpButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    var settingsVM = SettingsVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        if Helper.callRefreshToken(){
            settingsVM.eWalletSettings()
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        setVM()
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if Helper.callRefreshToken(){
            settingsVM.ChangeSetting(ip: ipTextView.text, old: currentPasswordTF.text ?? "", new: newPasswordTF.text ?? "", confirm: confirmPasswordTf.text ?? "")
        }else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
    }
    @IBAction func addIpAction(_ sender: Any) {
        ipTextView.isUserInteractionEnabled = true
        addIpButton.isHidden = true
        ipTextView.becomeFirstResponder()
    }
    @IBAction func showNotification(_ sender: Any) {
        let notify = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    func setVM() {
        settingsVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Password:
                if let ip = self?.settingsVM.SettingsArray[0].ip{
                    if ip.count > 0{
                        self?.ipTextView.text = ip
                    }
                    else{
                        self?.ipTextView.isUserInteractionEnabled = false
                        self?.addIpButton.isHidden = false
                    }
                }
                if self?.settingsVM.SettingsArray[0].transaction_password != nil{
                    self?.currentPasswordView.isHidden = false
                }else{
                    self?.currentPasswordView.isHidden = true
                }
                break
            case .ChangeSettings:
                self?.navigationController?.popViewController(animated: true)
                Helper.showAlert(message: StringConstants.Settingsaresaved(), head: StringConstants.Success())
                break
            default:
                break
            }
        }).disposed(by: settingsVM.disposeBag)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
