//
//  SettingUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 29/03/21.
//

import Foundation
import UIKit

extension SettingsVC{
    func setUi(){
        outerView.layer.cornerRadius = 8.0
        submitButton.layer.cornerRadius = 8.0
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
        Helper.roundBorder(view: ipTextView, radius: 5.0, borderWidth: 0.7, color: #colorLiteral(red: 0.737254902, green: 0.7725490196, blue: 0.8901960784, alpha: 1))
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 10.0)
    }
    //add and remove observers
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
}
