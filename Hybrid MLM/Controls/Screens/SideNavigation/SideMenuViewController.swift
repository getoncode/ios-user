//
//  SideMenuViewController.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 31/12/20.
//

import UIKit
import SideMenu

class SideMenuViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var sideMenuTable: UITableView!
    var data = [DataModel(headername: StringConstants.Payout(),image :#imageLiteral(resourceName: "payout") ,subtype: [], inexpansion: false),DataModel(headername: StringConstants.Mail(), image :#imageLiteral(resourceName: "Mail") ,subtype: [], inexpansion: false),DataModel(headername: StringConstants.Reports(),image :#imageLiteral(resourceName: "Reports") , subtype: [], inexpansion: false),DataModel(headername: StringConstants.Support(), image :#imageLiteral(resourceName: "Support") ,subtype: [],inexpansion: false),DataModel(headername: StringConstants.PrivacyPolicy(),image :#imageLiteral(resourceName: "Privacy&Policy") , subtype: [], inexpansion: false),DataModel(headername: StringConstants.TermsConditions(), image :#imageLiteral(resourceName: "Terms&Conditions") ,subtype: [],inexpansion: false)]
    var selectedIndex : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImage.layer.cornerRadius = 42.5
    }

    override func viewWillAppear(_ animated: Bool) {
        nameLabel.text = Utility.UserData.username
        Helper.setImage(image:  APIKeys.baseURLFirst + Utility.UserData.profilePic, on: profileImage)
        emailLabel.text = Utility.UserData.email
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    //set logout
    @IBAction func logout(_ sender: Any) {
        Helper.logout()
    }
}

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .default
    }
}
