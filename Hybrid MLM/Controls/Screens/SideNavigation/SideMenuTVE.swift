//
//  SideMenuTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 14/01/21.
//

import Foundation
import UIKit

extension SideMenuViewController{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SideMenuTViewCell.self), for: indexPath) as! SideMenuTViewCell
        cell.menuLabel.text = data[indexPath.row].HeaderName
        cell.iconView.image = data[indexPath.row].image
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            //push to payout
            performSegue(withIdentifier: String(describing: SideMenuViewController.self)+String(describing: PayOutVC.self), sender: nil)
        }
        if indexPath.row == 1{
            //push to mail
            let storyboard: UIStoryboard = UIStoryboard(name: StringConstants.Mail(), bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: String(describing: InboxVC.self)) as! InboxVC
            self.show(vc, sender: nil)
        }
        if indexPath.row == 2{
            //push to reports
            performSegue(withIdentifier: String(describing: SideMenuViewController.self)+String(describing: ReportsVC.self), sender: nil)
        }
        if indexPath.row == 3{
            performSegue(withIdentifier: String(describing: SideMenuViewController.self)+String(describing: SupportHomeVC.self), sender: nil)
            
        }
        if indexPath.row == 4{
            // push to privacy policy
            performSegue(withIdentifier: String(describing: SideMenuViewController.self) + String(describing: PrivacyPolicyVC.self), sender: nil)
        }
        if indexPath.row == 5{
            //push to terms&conditions
            performSegue(withIdentifier: String(describing: SideMenuViewController.self) + String(describing:TermsConditionsVC.self), sender: nil)
        }
         
    }
}
