//
//  DataModel.swift
//  dropdownmenu
//
//  Created by Shahna Nabeel on 21/11/20.
//

import Foundation
import UIKit
class DataModel {
    var HeaderName : String?
    var image : UIImage?
    var SubType = [String]()
    var inExpansion : Bool = false
    init(headername:String,image:UIImage,subtype:[String],inexpansion:Bool) {
        self.HeaderName = headername
        self.image = image
        self.SubType = subtype
        self.inExpansion = inexpansion
    }
}
