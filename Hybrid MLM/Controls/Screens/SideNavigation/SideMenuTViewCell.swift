//
//  SideMenuTViewCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 02/01/21.
//

import UIKit
import Charts

class SideMenuTViewCell: UITableViewCell {
    @IBOutlet weak var menuLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
