//
//  HeaderView.swift
//  dropdownmenu
//
//  Created by Shahna Nabeel on 21/11/20.
//

import UIKit
protocol HeaderDelegate {
    func callHeader(idx:Int)
    }


class HeaderView: UIView {
    var secIndex:Int?
    var delegate:HeaderDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(btn)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var btn: UIButton  = {
        let btn = UIButton(frame: CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.width, height: 60))
        btn.titleLabel?.textColor = UIColor.white
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 19.0)
        btn.contentHorizontalAlignment = .left
        btn.titleEdgeInsets.left = 46
        btn.addTarget(self, action: #selector(onClickView), for: .touchUpInside)
        return btn
        
    }()
    lazy var img : UIImageView = {
        let img = UIImageView(frame: CGRect(x: self.frame.width - 30, y: btn.frame.height/2, width: 15, height: 15))
        img.image = #imageLiteral(resourceName: "downArrow")
        return img
    }()

    @objc func onClickView(){
        if let idx = secIndex {
            delegate?.callHeader(idx: idx)
        }
    }
}
