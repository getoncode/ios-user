//
//  SearchRecipientTVE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 24/03/21.
//

import Foundation
import UIKit

extension SearchRecipientVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchRecipientVM.RecipientArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: String(describing: SearchRecipientTVCell.self))) as! SearchRecipientTVCell
        tableView.rowHeight = 70
        cell.setData(searchRecipientVM.RecipientArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ary = [searchRecipientVM.RecipientArray[indexPath.row].username]
        let idAry = [searchRecipientVM.RecipientArray[indexPath.row].id]
        Utility.recipientsData(value: idAry, names: ary)
        responseBack.onNext(ary)
        self.dismiss(animated: true, completion: nil)
    }
    
}
