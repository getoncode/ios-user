//
//  SearchRecipientUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 20/03/21.
//

import Foundation
import UIKit

extension SearchRecipientVC{
    
    //UI additionals
    func setUi() {
        recipientTable.layer.cornerRadius = 8.0
        searchResultLabelHeight.constant = 0
        searchResultLabel.isHidden = true
        searchBar.searchBarStyle = .minimal
    }
    //add and remove observers
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    //dismiss viewcontroller on tapping outside search view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.view {
            if let data = UserDefaults.standard.value(forKey: UserDefaultConstants.Recipients){
                responseBack.onNext(data)
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //search for recipients
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0{
            searchResultLabelHeight.constant = 0
            searchResultLabel.isHidden = true
            searchRecipientVM.getRecipients(search: "")
        }
        else{
            searchResultLabelHeight.constant = 20
            searchResultLabel.isHidden = false
            searchRecipientVM.getRecipients(search: searchText)
        }
    }
}
