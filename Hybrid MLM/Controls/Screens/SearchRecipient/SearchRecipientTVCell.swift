//
//  SearchRecipientTVCell.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 20/03/21.
//

import UIKit

class SearchRecipientTVCell: UITableViewCell {
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImage.layer.cornerRadius = 20.0
    }
    
    //set data after api call
    func setData(_ data: Recipients) {
        usernameLabel.text = data.username
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
