//
//  SearchRecipientVM.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 22/03/21.
//

import Foundation
import RxCocoa
import RxSwift

class SearchRecipientVM{
    enum typeHere: Int{
        case Default = 0
        case Recipient = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var RecipientArray: [Recipients] = []
    
    
    //get recipients api call
    func getRecipients(search: String) {
        let params : [String:Any] = [
            "purpose" : "getUsers",
            "params[keyword]" : search
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.RecipientArray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [String:Any] {
                    for item in [dataIn] {
                        let vals = item["original"] as! [Any]
                        for itm in vals{
                            self.RecipientArray.append(Recipients.init(data: itm as! [String : Any]))
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.Recipient)
            }
            
        }).disposed(by: disposeBag)
    }
}
