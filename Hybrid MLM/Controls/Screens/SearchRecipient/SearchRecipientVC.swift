//
//  SearchRecipientVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 20/03/21.
//

import UIKit
import RxCocoa
import RxSwift

class SearchRecipientVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchResultLabel: UILabel!
    @IBOutlet weak var searchResultLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var recipientTable: UITableView!
    let searchRecipientVM = SearchRecipientVM()
    let responseBack = PublishSubject<Any>()
    let disposebag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        searchBar.delegate = self
        if Helper.callRefreshToken(){
        searchRecipientVM.getRecipients(search: "")
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        setVM()
    }
    
    //setvm
    func setVM() {
        searchRecipientVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Recipient:
                self?.recipientTable.reloadData()
                break
            default:
                break
            }
        }).disposed(by: searchRecipientVM.disposeBag)
    }
    
   
    
}
