//
//  MyActivityUIE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 07/04/21.
//

import Foundation
extension MyActivityVC {
    func setUi() {
        notchDevice  = [StringConstants.iPhoneX(),StringConstants.iPhoneXS(),StringConstants.iPhoneXSMax(),StringConstants.iPhoneXR(),StringConstants.iPhone11(),StringConstants.iPhone11Pro(),StringConstants.iPhone11ProMax(),StringConstants.iPhone12mini(),StringConstants.iPhone12(),StringConstants.iPhone12Pro(),StringConstants.iPhone12ProMax(),StringConstants.iPhone13mini(),StringConstants.iPhone13(),StringConstants.iPhone13Pro(),StringConstants.iPhone13ProMax()]
        outerView.layer.cornerRadius = 8.0
        
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 15.0)
        setTableHeight()
    }
}
