//
//  MyActivityVC.swift
//  Hybrid MLM
//
//  Created by AiswaryaO on 07/04/21.
//

import UIKit

class MyActivityVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var outerViewConstraint: NSLayoutConstraint!
    let modelName = UIDevice.modelName
    var notchDevice : [String] = []
    var myActivityVM = MyActivityVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUi()
        if Helper.callRefreshToken(){
            myActivityVM.getActivity()
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        setVM()
        tableView.estimatedRowHeight = 100
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    func setVM() {
        myActivityVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .Activity:
                self?.tableView.reloadData()
                self?.setTableHeight()
                break
            default:
                break
            }
        }).disposed(by: myActivityVM.disposeBag)
    }
    
    @IBAction func showNotifications(_ sender: UIBarButtonItem) {
        let notify = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    
    
}
