//
//  MyActivityTVE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 07/04/21.
//

import Foundation
import UIKit
extension MyActivityVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myActivityVM.activityArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MyActivityTVCell.self), for: indexPath) as! MyActivityTVCell
        cell.setData(myActivityVM.activityArray[indexPath.row])
        if (indexPath.row == self.myActivityVM.activityArray.count-1) {
            cell.seperatorView.isHidden = true
        }else{
            cell.seperatorView.isHidden = false
        }
        return cell
    }
    
    func setTableHeight() {
        var height : CGFloat = 0.0
        height = CGFloat(80.0 * Double(myActivityVM.activityArray.count))
        if  height ?? 0 > self.view.frame.height - 100
        {
            if notchDevice.contains(modelName)
            {
                outerViewConstraint.constant = self.view.frame.height - 100
            }
            else
            {
                outerViewConstraint.constant = self.view.frame.height - 50
            }
        }
        else
        {
            outerViewConstraint.constant = height + 20
        }
    }
}
