//
//  MyActivityTVCell.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 07/04/21.
//

import UIKit

class MyActivityTVCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundView?.layer.cornerRadius = 10
    }
    
    func setData(_ data: Activity) {
        lblDate.text = data.timestamp
        lblTitle.text = data.description["en"] as? String
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
