//
//  MyActivityVM.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 07/04/21.
//

import Foundation
import RxCocoa
import RxSwift

class MyActivityVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case Activity = 1
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var activityArray : [Activity] = []
    func getActivity() {
        let params : [String:Any] = [
            "purpose" : "getActivities"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in [dataIn] {
                        for itm in item{
                            self.activityArray.append(Activity.init(data: itm))
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.Activity)
            }
            
        }).disposed(by: disposeBag)
    }
    
}
