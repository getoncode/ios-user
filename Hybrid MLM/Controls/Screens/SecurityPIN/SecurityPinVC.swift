//
//  SecurityPinVC.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 17/11/20.
//

import UIKit

class SecurityPinVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var submitOtpButton: UIButton!
    @IBOutlet weak var reTypeTFView: UIView!
    @IBOutlet weak var otpOneTF: UITextField!
    @IBOutlet weak var otpTwoTF: UITextField!
    @IBOutlet weak var otpThreeView: UIView!
    @IBOutlet weak var otpThreeTF: UITextField!
    @IBOutlet weak var otpFourTF: UITextField!
    @IBOutlet weak var otpFourView: UIView!
    @IBOutlet weak var otpTwoView: UIView!
    @IBOutlet weak var otpOneView: UIView!
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var newTFview: UIView!
    @IBOutlet weak var newPinTF: UITextField!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var submitButtonRef: UIButton!
    @IBOutlet weak var reTypePinTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        otpOneTF.delegate = self
        otpTwoTF.delegate = self
        otpThreeTF.delegate = self
        otpFourTF.delegate = self
        setUi()
        setOtpTFs()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    
    @IBAction func sumitButtonClick(_ sender: Any) {
        if newPinTF.text != "" && reTypePinTF.text != ""{
            if newPinTF.text == reTypePinTF.text
            {
                mainView.isHidden = true
                otpView.isHidden = false
                submitOtpButton.isHidden = false
                otpOneTF.becomeFirstResponder()
                
            }
        }
    }
    @IBAction func showNotification(_ sender: Any) {
        let notify = UIStoryboard(name: StringConstants.Main(), bundle: nil).instantiateViewController(identifier: String(describing: NotificationVC.self))
        navigationController?.pushViewController(notify, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
