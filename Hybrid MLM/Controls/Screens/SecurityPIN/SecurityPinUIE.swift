//
//  SecurityPinUIE.swift
//  Hybrid MLM
//
//  Created by Shahna Nabeel on 17/11/20.
//

import Foundation
import UIKit

extension SecurityPinVC
{
    func setUi() {
      //show navigation bar
        self.navigationController?.navigationBar.isHidden = false
        Helper.roundBorder(view: submitButtonRef, radius: 5.0, borderWidth: 0.7, color: Colors.AppBaseColor.cgColor)
        Helper.roundBorder(view: submitOtpButton, radius: 5.0, borderWidth: 0.7, color: Colors.AppBaseColor.cgColor)
        
        //set corner radius for textfields
        Helper.addBottomBorderWithColor(TF: otpOneTF, color: Colors.AppBaseColor, width: 1.0)
        Helper.addBottomBorderWithColor(TF: otpTwoTF, color: Colors.AppBaseColor, width: 1.0)
        Helper.addBottomBorderWithColor(TF: otpThreeTF, color: Colors.AppBaseColor, width: 1.0)
        Helper.addBottomBorderWithColor(TF: otpFourTF, color: Colors.AppBaseColor, width: 1.0)
        
        //hide OTP view initially
        otpView.isHidden = true
        submitOtpButton.isHidden = true
        
        outerView.layer.cornerRadius = 8.0
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 15.0)
        Helper.removeNavigationSeparator(controller: self.navigationController!, image: false)
        
    }
    
    func setOtpTFs()  {
        otpOneTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpTwoTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpThreeTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpFourTF.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpOneTF.becomeFirstResponder()
    }
    //setting OTP textfield count minimum 1
    @objc func textFieldDidChange(textField: UITextField){
        
        let text = textField.text
        
        if text?.utf16.count==1{
            switch textField{
            case otpOneTF:
                otpTwoTF.becomeFirstResponder()
            case otpTwoTF:
                otpThreeTF.becomeFirstResponder()
            case otpThreeTF:
                otpFourTF.becomeFirstResponder()
            case otpFourTF:
                otpFourTF.resignFirstResponder()
            default:
                break
            }
        }else{
            
        }
    }
    //set maximum length of otp textfields
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        switch textField {
        case otpOneTF:
            if ((textField.text?.length)! + (string.length - range.length)) > 1 {
                return false
            }

        case otpTwoTF:
            if ((textField.text?.length)! + (string.length - range.length)) > 1 {
                return false
            }
        case otpThreeTF:
            if ((textField.text?.length)! + (string.length - range.length)) > 1 {
                return false
            }
        case otpFourTF:
            if ((textField.text?.length)! + (string.length - range.length)) > 1 {
                return false
            }
        default:
            return true
        }
        return true
    }
}
