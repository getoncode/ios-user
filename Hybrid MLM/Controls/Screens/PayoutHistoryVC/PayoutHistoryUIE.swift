//
//  PayoutHistoryUIE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 09/07/21.
//

import Foundation
import UIKit
extension PayoutHistoryVC
{
    func setUi(){
        noDataLabel.isHidden = true
        sgTextOnlyBar.selectorType = .bottomBar
        sgTextOnlyBar.selectorColor = .systemBlue
        sgTextOnlyBar.SelectedFont = UIFont.boldSystemFont(ofSize: 16)
        sgTextOnlyBar.normalFont = UIFont.systemFont(ofSize: 16)
        sgTextOnlyBar.buttonTitles =
            "\(StringConstants.PayoutRequest()),\(StringConstants.PayoutRelease())"
        sgTextOnlyBar.clipsToBounds = true
        Helper.setShadow(view: outerView, color: Colors.ShadowColor, radius: 10.0)
        sgTextOnlyBar.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        outerView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        outerView.layer.cornerRadius = 15
        sgTextOnlyBar.layer.cornerRadius = 15
        Helper.setShadow(view: sgTextOnlyBar, color: Colors.ShadowColor, radius: 8.0)
        tablePayoutHistory.register(UINib(nibName: String(describing: PayoutRequestHistoryTVCell.self), bundle: nil), forCellReuseIdentifier: String(describing: PayoutRequestHistoryTVCell.self))
        tablePayoutHistory.layer.cornerRadius = 10
        outerView.layer.cornerRadius = 10
        tablePayoutHistory.register(UINib(nibName: String(describing: PayoutReleaseHistoryTVCell.self), bundle: nil), forCellReuseIdentifier: String(describing: PayoutReleaseHistoryTVCell.self))
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
}

