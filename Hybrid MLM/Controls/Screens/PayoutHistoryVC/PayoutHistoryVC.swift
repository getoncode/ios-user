//
//  PayoutHistoryTVC.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 09/07/21.
//

import UIKit
import WMSegmentControl
class PayoutHistoryVC:UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let modelName = UIDevice.modelName
    var notchDevice : [String] = []
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var outerViewConstraint: NSLayoutConstraint!
    @IBOutlet var tablePayoutHistory: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var sgTextOnlyBar: WMSegment!
    let payoutHistoryVM = PayoutHistoryVM()
    var passIndex : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        if Helper.callRefreshToken(){
            payoutHistoryVM.getPayoutRequestHistory()
            payoutHistoryVM.getPayoutReleaseHistory()
        }
        else{
            Helper.showAlertReturn(message: StringConstants.tokenExpired(), head: StringConstants.Sorry(), type: StringConstants.Logout(), Hide: true, completion: {_ in
                Helper.logout()
            })
        }
        setVM()
        setUi()
    }
    func setVM() {
        payoutHistoryVM.ResposeBack.subscribe(onNext: { [weak self]data in
            switch data {
            case .ReqHistory:
                if self?.payoutHistoryVM.payoutRequestHarray.count == 0{
                    self?.noDataLabel.isHidden = false
                }else{
                    self?.noDataLabel.isHidden = true
                }
                self?.tablePayoutHistory.reloadData()
                break
            case .RelHistory:
                if self?.payoutHistoryVM.payoutReleaseHarray.count == 0{
                    self?.noDataLabel.isHidden = false
                }else{
                    self?.noDataLabel.isHidden = true
                }
                self?.tablePayoutHistory.reloadData()
                break
            default:
                break
            }
        }).disposed(by: payoutHistoryVM.disposeBag)
    }
    
    @IBAction func onValueChanged(_ sender: WMSegment) {
        if sgTextOnlyBar.selectedSegmentIndex == 0{
            payoutHistoryVM.getPayoutRequestHistory()
        }else{
            payoutHistoryVM.getPayoutReleaseHistory()
        }
        tablePayoutHistory.reloadData()
    }
}
