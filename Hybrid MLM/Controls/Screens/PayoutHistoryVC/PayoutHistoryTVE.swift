//
//  PayoutHistoryTVE.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 09/07/21.
//

import Foundation
import UIKit
extension PayoutHistoryVC{
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let selectedIndex = sgTextOnlyBar.selectedSegmentIndex
        switch selectedIndex
        {
        case 0:
            return payoutHistoryVM.payoutRequestHarray.count
        case 1:
            return payoutHistoryVM.payoutReleaseHarray.count
        //Add other cases here
        default:
            return 0
        }
        //the datasource of the dynamic section
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tablePayoutHistory.rowHeight = UITableView.automaticDimension
        tablePayoutHistory.estimatedRowHeight = 100
        let selectedIndex = sgTextOnlyBar.selectedSegmentIndex
        switch selectedIndex
        {
        case 0:
            let cell =
                tablePayoutHistory.dequeueReusableCell(withIdentifier: String(describing: PayoutRequestHistoryTVCell.self), for: indexPath) as! PayoutRequestHistoryTVCell
            cell.setData(payoutHistoryVM.payoutRequestHarray[indexPath.row])
            setTableHeight()

         
            return cell
        case 1:
            let cell1 =
                tablePayoutHistory.dequeueReusableCell(withIdentifier: String(describing: PayoutReleaseHistoryTVCell.self), for: indexPath) as! PayoutReleaseHistoryTVCell
            cell1.setData(payoutHistoryVM.payoutReleaseHarray[indexPath.row])

            setTableHeight()
                     return cell1
            
        //Add other cases here
        default:
            return UITableViewCell()
        }
    }
    func setTableHeight() {
        var height : CGFloat = 0.0
        let selectedIndex = sgTextOnlyBar.selectedSegmentIndex
        if selectedIndex == 0{
            height = CGFloat(84.0 * Double(payoutHistoryVM.payoutRequestHarray.count))
        }else{
            height = CGFloat(84.0 * Double(payoutHistoryVM.payoutReleaseHarray.count))
        }
            if  height ?? 0 > self.view.frame.height - 126.0
            {
                if notchDevice.contains(modelName)
                {
                    outerViewConstraint.constant = self.view.frame.height - 100.0
                }
                else
                {
                    outerViewConstraint.constant = self.view.frame.height - 50
                }
            }
            else
            {
                outerViewConstraint.constant = height + 20
            }
        }


    }

