//
//  PayoutRequestHistoryTVCell.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 09/07/21.
//

import UIKit

class PayoutRequestHistoryTVCell: UITableViewCell {

    @IBOutlet weak var ewalletOuterView: UIView!
    @IBOutlet weak var ewalletLbl: UILabel!
    @IBOutlet weak var strtArrowImage: UIImageView!
    @IBOutlet weak var amountLbl: UILabel!
    
    @IBOutlet weak var transIdLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    
    @IBOutlet weak var outerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
//        outerView.layer.cornerRadius = 10
        
    }
    //set data after api call
        func setData(_ data: PayoutReqHistory) {
            let amountValue = data.dataAry["request_amount"] as? Double
            let round = Double(round(100*amountValue!)/100)
            amountLbl.text = "$ \(round)"
            let transaction = data.dataAry["transaction_id"] as? Int
            transIdLbl.text = "#\(String(describing: transaction!))"
            dateLbl.text = data.dataAry["created_at"] as? String
            ewalletLbl.text = data.walletName
        }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
