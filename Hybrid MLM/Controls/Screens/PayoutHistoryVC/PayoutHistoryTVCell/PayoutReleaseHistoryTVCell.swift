//
//  PayoutReleaseHistoryTVCell.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 09/07/21.
//

import UIKit

class PayoutReleaseHistoryTVCell: UITableViewCell {

    @IBOutlet weak var ewalletLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var walletTransferLabel: UILabel!
    @IBOutlet weak var walletAmountLbl: UILabel!
    @IBOutlet weak var transcIdLbl: UILabel!
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var dateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    //set data after api call
        func setData(_ data: PayoutRelHistory) {
            let round = Double(round(100*data.amount)/100)
            walletAmountLbl.text = "$\(round)"
            transcIdLbl.text = "#\(String(describing: data.dataAry["transaction_id"]!))"
         
        }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
