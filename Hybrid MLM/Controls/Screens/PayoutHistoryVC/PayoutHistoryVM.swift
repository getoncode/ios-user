//
//  PayoutHistoryVM.swift
//  Hybrid MLM
//
//  Created by acemero technologies on 09/07/21.
//

import Foundation
import RxCocoa
import RxSwift

class PayoutHistoryVM : NSObject{
    enum typeHere:Int {
        case Default = 0
        case ReqHistory = 1
        case RelHistory = 2
    }
    let disposeBag = DisposeBag()
    let ResposeBack = PublishSubject<typeHere>()
    var payoutRequestHarray : [PayoutReqHistory] = []
    var payoutReleaseHarray : [PayoutRelHistory] = []
   
    func getPayoutRequestHistory() {
        let params : [String:Any] = [
            "purpose" : "payoutRequestHistory"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.payoutRequestHarray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in [dataIn] {
                        for itm in item{
                         self.payoutRequestHarray.append(PayoutReqHistory.init(data: itm))
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.ReqHistory)
            }
            
        }).disposed(by: disposeBag)
    }
    func getPayoutReleaseHistory() {
        let params : [String:Any] = [
            "purpose" : "payoutReleaseHistory"
        ]
        let headers = ["Authorization" : "Bearer \(UserDefaults.standard.value(forKey: StringConstants.accessToken()) as! String)"]
        APIModel.apiCall(method: .post, api: "", params: params, header: headers).subscribe(onNext: { data in
            self.payoutReleaseHarray.removeAll()
            if data.0 {
                if let dataIn = data.1 as? [[String:Any]] {
                    for item in [dataIn] {
                        for itm in item{
                        self.payoutReleaseHarray.append(PayoutRelHistory.init(data: itm))
                        }
                    }
                }
                self.ResposeBack.onNext(typeHere.RelHistory)
            }
            
        }).disposed(by: disposeBag)
    }
   
}


